# -*- mode: python -*-
a = Analysis(['IntegratorTSV2DanteRTSV.py'],
             pathex=['F:\\LymPHOS Tools'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='IntegratorTSV2DanteRTSV.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True , icon='IntegratorTSV2DanteRTSV.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='IntegratorTSV2DanteRTSV')
