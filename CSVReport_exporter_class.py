#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from __future__ import division

"""
:synopsis: `CSVReport_exporter_class.py` defines the classes needed to exports
a LymPHOS2 DataBase as a CSV file (:class:`CSVReportGenerator`) and additional
PNG information files (:class:`GraphReportGenerator`). If quantitative data is
exported some assessment parameters (T Student tests, Fold-change, 3*SDofMeans,
...) are calculated, and graphical histograms are saved as PNG files.
See file ``CSVReport_config.py`` to define ``SEPARATOR`` and ``QASCORE_THRESHOLD``.

It defines :class:`ReportGenerator`, :class:`CSVReportGenerator` and
:class:`GraphReportGenerator`:

              ReportGenerator
               ^          ^
               |          |
  CSVReportGenerator   GraphReportGenerator
        |                    |
        V                    V
     CSV file             PNG files

:created:    2013/10/08

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013-2017 LP-CSIC/UAB. All rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '1.7'
__UPDATED__ = '2017-07-05'


#===============================================================================
# Imports
#===============================================================================
from tools_commons import PepProtFuncs, MathFuncs, ObjectDoesNotExist

import numpy as np
from scipy import stats
import matplotlib.pyplot as plot
import os
from datetime import datetime
from collections import defaultdict


#===============================================================================
# Global variables
#===============================================================================

import CSVReport_config as config


#===============================================================================
# Class definitions
#===============================================================================
class ReportGenerator(object):
    """
    Class to generate Report files from LymPHOS data
    """
    def __init__(self, filen, data_records=None, nop_stats=None, sub_db=None):
        self._iofile = None
        self._data_records = None
        #
        self.filen = filen
        self.sub_db = sub_db
        if not data_records and sub_db:
            data_records = sub_db.get_data()
        self.data_records = data_records
        self.nop_stats = nop_stats

    @staticmethod
    def _min_or_h(minutes):
        """
        Transform minutes in minutes or hours, returning also the chosen units.
        """
        if minutes >= 60:
            time = minutes / 60
            units = 'h.'
        else:
            time = minutes
            units = 'min.'
        return time, units

    @staticmethod
    def _get_experiments(data_records):
        """
        Return a set containing all the experiments witch generated the supplied
        data records (data_records).
        """
        return set(data_record.experiment for data_record in data_records)

    @staticmethod
    def _get_ascore_data(data_records, max_psites):
        """
        Return a dictionary (`fmt_ok_ascores`) containing the number data records
        (`data_records`) with ascore values > config.QASCORE_THRESHOLD for each p-site
        order position of a peptide.
        """
        # Get Ascore values for all data_records:
        ok_ascores = [0] * max_psites #Number of ascores > config.QASCORE_THRESHOLD for the p-site number equivalent to the index of the list +1
        for data_record in data_records:
            for n_psite, ascore in enumerate(data_record.ascores):
                if ascore > config.QASCORE_THRESHOLD:
                    ok_ascores[n_psite] += 1
        # Format and return Ascore columns:
        fmt_ok_ascores = dict()
        ascore_header_tmplt = '#Ascore>{0} psite-{1}'.format(config.QASCORE_THRESHOLD,
                                                             '{0}')
        for n_psite, ascores in enumerate(ok_ascores):
            fmt_ok_ascores[ ascore_header_tmplt.format(n_psite + 1) ] = ascores
        return fmt_ok_ascores

    @staticmethod
    def _collect_time_quantvals(data_records):
        """
        Return a dictionary (`alltimes_vals`) containing quantitative data
        collected from the specified data records (`data_records`) and grouped by
        time.
        """
        # Structure of the dictionary used for data collection for the current
        # peptide data_records in a given time:
        alltimes_vals = defaultdict( lambda: {'data_records': list(),
                                              '#spectra': 0, #Number of spectra
                                              'reg_vals': list(),   #reg_vs, ratios and stdvs values for the current peptide in a given time
                                              'ratio_vals': list(), #
                                              'stdv_vals': list(),  #
                                              } )
        for data_record in data_records:
            # Ascore information and quantitative values for each time condition
            # of the current data record:
            try:
                record_time_values = data_record.dataquant.time_values()
            except ObjectDoesNotExist: #data_record has not dataquant related records (is not quantitative)
                continue
            for time, (reg_vs, ratio, stdv) in record_time_values.items():
                time_vals = alltimes_vals[time]
                time_vals['data_records'].append(data_record)
                time_vals['#spectra'] += 1
                time_vals['reg_vals'].extend(reg_vs)
                time_vals['ratio_vals'].append(ratio)
                time_vals['stdv_vals'].append(stdv)
        #
        return alltimes_vals


class CSVReportGenerator(ReportGenerator):
    """
    Class to generate CSV Report files from LymPHOS DB data.
    """

    INFO_HEADER = """DATA EXPORTED FROM LymPHOS2 DataBase; https://www.lymphos.org; https://dx.doi.org/10.1093/database/bav115
Exporter version:; {3} (build {4});Date&Time:; {0}
Original File Name:; {1}
DataBase filter:; {2}

3*SD Filter:; 1: Caution! The peptide SD > 3*(mean of SDs)
; 0: No problem (The peptide SD <= 3*(mean of SDs)
t-test:; 1: change with the confidence indicated by %
; 0: no confidence of change

Caution:; You can find duplicated sequences (SEQ.) because they were found in different Supar-Experiments (look also at Supra-Exp. column)


""".replace(';', config.SEPARATOR)
    _ascore_header_base = '#Ascore>{0} psite-'.format(config.QASCORE_THRESHOLD)
    HEADERS = ( 'SEQ.', 'Supra-Exp.', '#spectra', '#experiments',
                _ascore_header_base+'1', _ascore_header_base+'2', _ascore_header_base+'3',
                ('Mean ratio', 'SD', 'Fold-Change', 'SD>3*MEAN-SD',
                 't-test 95%', 't-test 99%', '#exps', '#spectra',
                 _ascore_header_base+'1', _ascore_header_base+'2',
                 _ascore_header_base+'3', '#scans down', '#scans no-change',
                 '#scans up'),
               '#prots.', 'Prots. ACs'
               )

    def __init__(self, filen, data_records=None, proteins=None, nop_stats=None,
                 quantitative=None, quant_times=None, sub_db=None):
        self.peptides = dict() #Grouped data records.
        self._proteins = None
        self.proteins_by_ac = dict()
        #
        self.quantitative = quantitative #True: Only quantitative data, False: only non-quantitative data, None: both
        if not proteins and sub_db:
            proteins = sub_db.get_proteins()
        if not quant_times and sub_db:
            quant_times = sub_db.get_quant_times()
        self.proteins = proteins
        self.quant_times = quant_times
        #
        super(CSVReportGenerator, self).__init__(filen, data_records, nop_stats,
                                                 sub_db)

    @property
    def proteins(self):
        return self._proteins
    @proteins.setter
    def proteins(self, proteins):
        self._proteins = proteins
        if proteins:
            self.proteins_by_ac = PepProtFuncs.proteins_by_ac(proteins)
        else:
            self.proteins_by_ac = dict()

    @property
    def data_records(self):
        return self._data_records
    @data_records.setter
    def data_records(self, data_records):
        self._data_records = data_records
        self.peptides = dict() #Grouped data records
        self.sd_means3 = dict() #3*(mean of stdv) from all data records for each time
        if data_records:
            # Group data records in ``self.peptides`` dictionary:
            attrs = ('peptide',) #Group data records by peptide sequence.
            if self.quantitative is not False: #Quantitative data MAY be present:
                attrs = attrs + ('experiment.expgroup',) #Also group by experiment group (supra-experiment)
            self.peptides = PepProtFuncs.peptides_by_attr(data_records, *attrs)
            # Calculates 3*(mean of stdv) from all data records, for each time
            # condition, and stores them as self.sd_means3 dictionary: #X-NOTE: It's correct to do it with all data_records of all supra-experiments? Should it be separated per supra-experiment? Does it has some sense when later we compare it with a stdv of a mean of means?
            if self.quantitative is not False: # Quantitative data MAY be present:
                time_sd_means = PepProtFuncs.mean_of_peps_sds(data_records)
                for time, stdv in time_sd_means.items():
                    self.sd_means3[time] = 3 * stdv
            else: # No quantitative records:
                self.sd_means3 = dict()

    def open(self):
        self._iofile = open(self.filen, 'wb')
        return self._iofile

    def write_head(self, *args, **kwargs):
        """
        Write information about the file, and the columns headers.
        """
        # Write information about the file:
        self._iofile.write( self.INFO_HEADER.format(datetime.now(), self.filen,
                                                    self.sub_db._filters,
                                                    __VERSION__, __UPDATED__) )
        #
        # Generate the 2 header rows:
        header_rows = ( [], [] )
        for header in self.HEADERS:
            if not isinstance(header, (tuple, list)): #General headers (in 2nd header row):
                header_rows[0].append('')
                header_rows[1].append(header)
            else: #Quantitative headers:
                for minutes in sorted(self.quant_times):
                    # Put Control vs. times in 1st header row:
                    time, units = self._min_or_h(minutes)
                    header_rows[0].append( 'C. vs {0} {1}'.format(time, units) )
                    header_rows[0].extend( [''] * (len(header)-1) )
                    # Put quantitative headers in 2nd header row:
                    for subheader in header:
                        header_rows[1].append(subheader)
        # Write the 2 header rows:
        for header_row in header_rows:
            self._iofile.write( config.SEPARATOR.join(header_row) )
            self._iofile.write('\n')
        #
        return self._iofile

    def _get_quant_cols(self, peptide, exp_group_id):
        """
        Get quantitative analyzed data about one peptide from its peptide
        grouping dictionary (`peptide`).

        Returns a dictionary containing the quantitative analyzed data (`cols`).
        """
        cols = dict()
        # Collect quantitative data by time for the statistics:
        time_quantvals = self._collect_time_quantvals( peptide['data_records'] )
        # Get statistic data for corresponding no-phosphorylated peptides of
        # the same supra-experiment:
        nop_stats = self.nop_stats[exp_group_id]
        # Summarizes and apply statistics to quantitative data for the current
        # peptide in each time:
        for time, quantvals in time_quantvals.items():
            sub_cols = dict()
            reg_vals = quantvals['reg_vals']
            ratios = quantvals['ratio_vals']
            stdvs = quantvals['stdv_vals']
            n_spectra = quantvals['#spectra']
            if n_spectra > 1: # There is the minimum data to do statistics:
                ratio = np.mean(ratios)
                stdv = np.std(ratios)
                # Student's t-test for mean log2(ratio) of 2 populations (no
                # phosphorylated, and current peptide in current condition in
                # current time) with unequal sample sizes, unequal variance:
                df = MathFuncs.degrees_freedom(nop_stats['mu'],    ratio,
                                               nop_stats['sigma'], stdv,
                                               nop_stats['n'],     n_spectra)
                tstat = MathFuncs.ttest(nop_stats['mu'],    ratio,
                                        nop_stats['sigma'], stdv,
                                        nop_stats['n'],     n_spectra)
                thyp95 = 0 if tstat < MathFuncs.tstudent(0.05, df) else 1
                thyp99 = 0 if tstat < MathFuncs.tstudent(0.01, df) else 1
            else: # Not enough data to do statistics:
                ratio = ratios[0]
                stdv = stdvs[0]
                thyp95 = thyp99 = '-'
            # Alerts (value 1) if the stdv is greater than 3x mean stdv of the
            # same time: #X-NOTE: Does it has some sense to compare a 3x mean of all records stdv with a stdv of a mean of means?
            sd_filter = 0 if stdv < self.sd_means3[time] else 1
            # Add the data to condition/time sub-row:
            sub_cols['Mean ratio'] = ratio
            sub_cols['SD'] = stdv
            sub_cols['SD>3*MEAN-SD'] = sd_filter
            sub_cols['Fold-Change'] = 2**ratio
            sub_cols['#spectra'] = n_spectra
            sub_cols['#exps'] = len(self._get_experiments( quantvals['data_records'] ))
            sub_cols.update(self._get_ascore_data( quantvals['data_records'],
                                                   len( peptide['psites'] ) )
                            )
            sub_cols['t-test 95%'] = thyp95
            sub_cols['t-test 99%'] = thyp99
            sub_cols['#scans down'] = reg_vals.count(-1)
            sub_cols['#scans no-change'] = reg_vals.count(0)
            sub_cols['#scans up'] = reg_vals.count(1)
            #
            cols[time] = sub_cols
        #
        return cols

    def _get_rows(self, *args, **kwargs):
        """
        Generator method of each row (corresponding to a peptide in one
        condition) of the exported data.
        """
        for gid, peptide in sorted( self.peptides.items() ):
            row = dict()
            if self.quantitative is not False:
                pid, exp_group = gid
            else:
                pid = gid
                exp_group = None
            # Add the peptide sequence:
            row['SEQ.'] = pid
            # Add the experimental group (supra-experiment):
            row['Supra-Exp.'] = exp_group.id if exp_group else '-'
            # Add general ascore data:
            row.update(self._get_ascore_data( peptide['data_records'],
                                               len(peptide['psites']) ))
            # Add total number of scans and experiments:
            row['#spectra'] = peptide['n_scans']
            row['#experiments'] = len(self._get_experiments(peptide['data_records']))
            # Add quantitative data for each time:
            if self.quantitative is not False and peptide['quantitative']:
                quant_cols = self._get_quant_cols(peptide, exp_group.id)
                row.update(quant_cols)
            # Add proteins data:
            row['Prots. ACs'] = sorted(peptide['l_prots_w_decoys'])
            row['#prots.'] = len(row['Prots. ACs'])
            yield row

    def write_rows(self, *args, **kwargs):
        """
        Formats row_data dictionary returned by _get_rows() generator into tab
        separated values, in the order indicated by HEADERS labels
        (headersorted_row list), to write it to file.
        """
        for row_data in self._get_rows():
            headersorted_row = list()
            for header in self.HEADERS:
                if not isinstance(header, (tuple, list)):
                    row_value = row_data.get(header, '-')
                    if not isinstance(row_value, (tuple, list)):
                        headersorted_row.append(row_value)
                    else:
                        headersorted_row.extend(row_value)
                else:
                    for time in sorted(self.quant_times):
                        for subheader in header:
                            quant_data = row_data.get(time, {}).get(subheader, '-')
                            headersorted_row.append(quant_data)
            #
            self._iofile.write( config.SEPARATOR.join(map(str, headersorted_row)) )
            self._iofile.write('\n')

    def write_all(self, *args, **kwargs):
        if not self._iofile:
            self.open()
        #
        self.write_head(*args, **kwargs)
        self.write_rows(*args, **kwargs)
        #
        return self._iofile

    def close(self):
        self._iofile.close()
        return self._iofile


class GraphReportGenerator(ReportGenerator):
    """
    Class to generate Graph Report files from LymPHOS data.
    """

    @property
    def data_records(self):
        return self._data_records
    @data_records.setter
    def data_records(self, data_records):
        self._data_records = data_records
        self.quantvals = dict() #Quantitative values of all data records for each time
        if data_records:
            # Get quantitative values of all data records for each time:
            self.quantvals = self._collect_time_quantvals(data_records)

    def write_ratio_histograms(self, times=None):
        """
        Write ratio histograms of all the experiments with data for each time.
        """
        if not times:
            times = self.quantvals.keys()
        for time in times:
            quantvals = self.quantvals[time]
            time, units = self._min_or_h(time)
            plot_name = '{0} {1}'.format(time, units)
            exp_group = [exp.id for exp in
                         self._get_experiments(quantvals['data_records'])]
            title = '-'.join( map(str, sorted(exp_group)) )
            title = 'Experiments {0} {1}'.format(title, plot_name)
            legend_tmplt = '{0} -> mu: {1:.4f}, sigma: {2:.4f}'
            # Phospho Ratios distribution:
            dist = MathFuncs.calc_normal(quantvals['ratio_vals'])
            A, mu, sigma, (xmin, xmax), nbins = dist
            legend = legend_tmplt.format(plot_name, mu, sigma)
            data_vals = (legend, quantvals['ratio_vals'], dist)
            # Non-Phospho Ratios distribution:
            dist_nop = (dist[0], self.nop_stats['mu'], self.nop_stats['sigma'],
                        (None, None), 600)
            legend = legend_tmplt.format('Non-Phospho', self.nop_stats['mu'],
                                         self.nop_stats['sigma'])
            nop_vals = (legend, None, dist_nop)
            #
            self.make_figures(title, data_vals, nop_vals, save=True)

    def _get_reporter_ions_int(self):
        intensity_by_time = defaultdict(list)
        for data_record in self.data_records:
            times = data_record.experiment.get_quant_cond()
            intensityes = data_record.qdata_l
            for intensity, time in zip(intensityes, times):
                intensity_by_time[time].append(intensity)
        #
        return intensity_by_time

    def write_reporterions_histograms(self, times=None):
        """
        Write reporter ions intensity histograms of all the experiments with
        data for each time.
        """
        intensity_by_time = self._get_reporter_ions_int()

        if not times:
            times = intensity_by_time.keys()

        for time in times:
            int_vals = intensity_by_time[time]
            exp_group = [exp.id for exp in
                         self._get_experiments(self.data_records)]
            title = '-'.join( map(str, sorted(exp_group)) )
            time, units = self._min_or_h(time)
            plot_name = '{0} {1}'.format(time, units)
            title = 'Experiments {0} Reporter Ions at {1}'.format(title,
                                                                  plot_name)
            legend_tmplt = '{0}:\n mean+-stdv: {1:.2f}+-{2:.2f}\n median: {3:.2f}\n mode: {4:.5f}'
            legend = legend_tmplt.format(plot_name,
                                         np.mean(int_vals),
                                         np.std(int_vals),
                                         np.median(int_vals),
                                         stats.mode(int_vals)[0][0]
                                         )
            dist = (None, None, None, ( 0, 500000 ), 200)
            data_vals = (legend, int_vals, dist)
            self.make_figures(title, data_vals, save=True)

    # Functions make_figures() and plot_gauss() copied and adapted from LymPHOS
    # PQuantifier pq_analyze.py at revision 123:
    def make_figures(self, title, *args, **kwargs):
        """
        Draw and save matplotlib plots.

        title: figure title
        args: plots to save/draw. Each plot as a tuple of type (name, values,
              dist), where dist is a tuple or list of the type (A, mu, sigma,
              (xmin, xmax), nbins)
        kwargs: draw: plot in screen. True/False
                save: save as png file. True/False. If True, 3 more key
                      arguments can be used: dir_save, file_save, and dpi_save
        """
        legends = []
        for legend, values, dist in args:
            A, mu, sigma, (xmin, xmax), nbins = dist
            if values:
                values = np.reshape(values, (1,-1))
                bins = np.linspace(xmin, xmax, nbins)
                plot.hist(values[0], bins, alpha=0.5, lw=0.4)
            if mu is not None and sigma is not None and A is not None:
                self.plot_gauss(A, mu, sigma)
            legends.append(legend)
        plot.title(title)
        plot.figtext(0.5,0.8, '\n'.join(legends))

        if kwargs.get('save', False):
            directory = kwargs.get('dir_save', None)
            filen = kwargs.get('file_save', None)
            dpi = kwargs.get('dpi_save', 600)
            if not filen:
                timestamp = datetime.now().strftime('%Y-%m-%d_%H%M%S')
                filen = self.filen.format(title=title, timestamp=timestamp)
            if directory:
                filen = os.path.join(directory, filen)
            plot.savefig(filen, dpi=dpi)

        if kwargs.get('draw', False):
            plot.show()
        else:
            plot.close()
    #
    @staticmethod
    def plot_gauss(A=2, mu=0, sigma=0.5, limits=None, resolution=100):
        """
        Plot gaussian distribution with parameters A, mu, sigma

        limits: tuple (xmin, xmax).
                if not given the curve covering 20 times sigma is drawn
        resolution: int. determines number of points
        """
        resolution = float(resolution)
        if not limits:
            lower = int(resolution * (mu - 20 * sigma))
            upper = int(resolution * (mu + 20 * sigma))
        else:
            lower = int(resolution * limits[0])
            upper = int(resolution * limits[1])

        x = [v / resolution for v in range(lower, upper)]

        data = MathFuncs.gauss(x, A, mu, sigma)
        plot.plot(x, data, linewidth=2)

