#!/usr/local/bin/python2.7
# encoding: utf-8
"""
update_prots_with_chr -- Updates the proteins in LymPHOS DB with the chromosome they are encoded in

:synopsis:   DanteRDB2TSV -- Updates the proteins in LymPHOS DB with the
chromosome they are encoded in.
See the Global variables section to define Configuration constants
(``CLI_PARAMS``), DanteR TSV file constants (``SQL_TABLES``), Data processing
constants (``PVAL_THRESHOLD`` and ``FC_THRESHOLD``), and Output TSV file
constants (``OUTPUT_METADATA_HEADERS``).

It defines the following classes and functions:
    :class:`I2DConfiguration`
    :func:`db_dict_factory`
    :func:`load_dante_data`
    :func:`process_data`
    :func:`csv_dict_writer`

:created:    2014-04-03

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1
:updated:    2014-04-03
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import division
from __future__ import print_function

from tools_commons import DB_SubSet
from LymPHOS_v1_5.lymphos.filters import UniProtXmlConverter, settings

import sys


#===============================================================================
# Global variables
#===============================================================================
__version__ = 0.1
__created__ = '2014-04-03'
__updated__ = '2014-04-10'


#===============================================================================
# Class definitions
#===============================================================================
class ProgramCancelled(Exception):
    """
    Generic sub-class of :class:``Exception`` to raise when program must be 
    cancelled by an user action.
    """
    def __init__(self, msg=None):
        super(ProgramCancelled).__init__(type(self))
        self._msg = 'Program Cancelled by user'
        if msg:
            self._msg = '{0}: {1}'.format(self._msg, msg)
    
    @property
    def msg(self):
        return self._msg
    
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


#===============================================================================
# Function definitions
#===============================================================================
def add_chr2protein(**more_filters):
    filters = {'chr': ''}
    filters.update(more_filters)
    sub_db = DB_SubSet()
    protac2chrm = UniProtXmlConverter(db_file=settings.UP_ZIP_DB).protac2chrm
    print('NextProt data Loaded')
    for protein in sub_db.get_proteins(**filters):
        prot_ac_no_isoform = protein.ac.partition('-')[0]
        if 'decoy' not in prot_ac_no_isoform:
            protein.chr = protac2chrm.get(prot_ac_no_isoform, '')
            protein.save()
            print(protein.ac + ' -> ' + protein.chr)
    
def main(*args):
    try:
        print('Starting work...')
        add_chr2protein()
        print('Work finished!')
        return 0
    except (KeyboardInterrupt, ProgramCancelled):
        ### Handle keyboard interrupt, and program cancellation
        print('Program Cancelled by user!')
        return 1
    except SystemExit:
        ### Handle system exit
        return 0        


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    #
    exit_code = main()
    #
    sys.exit(exit_code)