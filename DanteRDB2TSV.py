#!/usr/local/bin/python2.7
# encoding: utf-8
"""
DanteRDB2TSV -- Converts a DanteR SQlite DB output file to a TSV file

:synopsis:   DanteRDB2TSV -- Converts a DanteR SQlite DB output file to a TSV
file.

See the Global variables section to define:
- Configuration constants: ``CLI_PARAMS``
- DanteR TSV file constants: ``SQL_TABLES`` and ``SQL_ROWS``
- Data processing constants: ``PVAL_THRESHOLD`` and ``FC_THRESHOLD``
- Output TSV file constants: ``OUTPUT_METADATA_HEADERS``

It defines the following Classes and Functions:
    :class:`DanteDB`
    :func:`process_data`
    :func:`mix_qdata_metadata`
    :func:`csv_dict_writer`

:created:    2014-02-18

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""
from __future__ import division
from __future__ import print_function

__VERSION__ = '0.99'
__CREATED__ = '2014-02-18'
__UPDATED__ = '2015-10-29'


#===============================================================================
# Imports
#===============================================================================
import sys
import traceback
import os
import csv
import sqlite3
from collections import defaultdict, OrderedDict
from datetime import datetime

from tools.EasyMUI import Configuration, ProgramCancelled


#===============================================================================
# Global variables
#===============================================================================
# Configuration constants:
CLI_PARAMS = OrderedDict( (
                 ('verbose', {'arg': ('-v', '--verbose'),
                              'arg_opts': dict(action='count',
                                               help='set verbosity level')
                              } ),
                 ('interactive', {'arg': ('-i', '--interactive'),
                                  'arg_opts': dict(action='store_true',
                                                   help='ask for missing parameters')
                                  } ),
                 ('file_name', {'arg': tuple(),
                                      'arg_opts': dict(help='DanteR SQlite DB file to process',
                                                       metavar='input file',
                                                       nargs='?')
                                 } ),
                 ('output_file', {'arg': tuple(),
                                  'arg_opts': dict(help='output TSV file',
                                                   metavar='output file',
                                                   nargs='?')
                                  } ),
                              ) )

# DanteR DB file constants:
#    SQL_TABLES = {'id_name': 'sqlite3_table_name'}
SQL_TABLES = {'p_adjustment_ANOVA_TABLE': 'PAdj_ANOVA_LogT_T_Data',
              'ANOVA_TABLE': 'ANOVA_LogT_T_Data',
              }
#    SQL_ROWS = {'id_name': 'sqlite3_row_name'}
SQL_ROWS = {'Metadata_TABLE': 'T_Row_Metadata',
            'peptide': 'FINALpeptide',
            'proteins': 'consensus_proteins',
            '1st_protn': 'consensus_first_name',
            '1st_protac': 'consensus_first_accession',
            'phospho': 'phosphorylated',
            'row_id': 'row_names',
            }

# Data processing constants:
PVAL_THRESHOLD = 0.05 #p-Value threshold
FC_THRESHOLD = 1.5 #Fold Change threshold

# Output TSV file constants:
OUTPUT_METADATA_HEADERS = ['peptide', 'phospho', 'proteins', '1st_protn',
                           '1st_protac', 'max_ascore1', 'max_ascore2',
                           'max_ascore3']

# Execution constants:
DEBUG = False
PROFILE = False


#===============================================================================
# Class definitions
#===============================================================================
class DanteDB(object):
    """
    Class representing a SQLite3 DanteR export database.
    """

    _SQL_TABLES = {'p_adjustment_ANOVA_TABLE': 'PAdj_ANOVA_LogT_T_Data',
                   'ANOVA_TABLE': 'ANOVA_LogT_T_Data',
                   }
    _SQL_ROWS = {'Metadata_TABLE': 'T_Row_Metadata',
                 'peptide': 'FINALpeptide',
                 'proteins': 'proteins',
                 '1st_protn': 'first_name',
                 '1st_protac': 'first_accession',
                 'phospho': 'Phosphorylated',
                 'row_id': 'row_names',
                 }

    def __init__(self, file_name, id2sqltable = None, id2sqlrow = None,
                 separator='_'):
        """
        :param string file_name: the name of the SQLite3 database file to open.
        :param dictionary id2sqltable: internal table ID to real SQLite table
        name. Defaults to `DanteDB._SQL_TABLES`.
        :param dictionary id2sqlrow: internal row ID to real SQLite row name.
        Defaults to `DanteDB._SQL_ROWS`
        :param str separator: character to separate different analysis
        conditions. Defaults to '_'.
        """
        self._filen = None
        self._results_base_keys = None
        self.connection = None
        self._id2sqltable = None
        self._id2sqlrow = None
        self._id2tablerow = None
        self._quant_data = None
        self._metadata = None
        self._metadata_summary = None

        if id2sqltable:
            self.id2sqltable = id2sqltable
        else:
            self.id2sqltable = self._SQL_TABLES
        if id2sqlrow:
            self.id2sqlrow = id2sqlrow
        else:
            self.id2sqlrow = self._SQL_ROWS
        self.file_name = file_name
        self.separator = separator

    def __del__(self):
        self.close() #Try to ensure database close.

    @property
    def file_name(self):
        return self._filen
    @file_name.setter
    def file_name(self, file_name):
        if file_name != self._filen:
            self.close()
            self._filen = file_name
            self.open()
            self._results_base_keys = None

    @property
    def id2sqltable(self):
        return self._id2sqltable
    @id2sqltable.setter
    def id2sqltable(self, id2sqltable):
        if id2sqltable != self._id2sqltable:
            self._id2sqltable = id2sqltable
            self._id2tablerow = None

    @property
    def id2sqlrow(self):
        return self._id2sqlrow
    @id2sqlrow.setter
    def id2sqlrow(self, id2sqlrow):
        if id2sqlrow != self._id2sqlrow:
            self._id2sqlrow = id2sqlrow
            self._id2tablerow = None

    @property
    def id2tablerow(self):
        if self._id2tablerow is None:
            tables_rows = self.id2sqltable.copy()
            tables_rows.update(self.id2sqlrow)
            self._id2tablerow = tables_rows
        return self._id2tablerow

    @property
    def tables(self):
        """
        Names of all the tables in a SQLite database.
        """
        if self._tables is None and self.connection:
            cursor = self.connection.cursor()
            sql = "SELECT name FROM sqlite_master WHERE type='table';"
            cursor.execute( sql )
            self._tables = tuple( row['name'] for row in cursor.fetchall() )
        return self._tables

    def fields_in(self, table_name):
        """
        Names of all the fields in a SQLite database table.
        """
        cursor = self.connection.cursor()
        sql = "PRAGMA table_info('{0}');"
        cursor.execute( sql.format(table_name) )
        return tuple( row['name'] for row in cursor.fetchall() )

    @property
    def results_base_keys(self):
        if self._results_base_keys is None and self.connection:
            sep = self.separator
            cursor = self.connection.cursor()
            sql = "SELECT * FROM {ANOVA_TABLE};"
            cursor.execute( sql.format(**self.id2sqltable) )
            cols = {col[0].rpartition(sep)[0] for col in cursor.description}
            cols.discard( '' )
            cols.discard( self.id2sqlrow['row_id'].rpartition(sep)[0] )
            self._results_base_keys = tuple( sorted(cols) )
        return self._results_base_keys

    @staticmethod
    def _db_dict_factory(cursor, original_row):
        return { col[0]: original_row[idx] for idx, col in enumerate(cursor.description) }

    def open(self):
        """
        Open the connection to the database, if there is no opened connection
        """
        if not self.connection:
            self.connection = sqlite3.connect(self.file_name)
            self.connection.row_factory = self._db_dict_factory #Make the cursor return row dictionaries.
        return self

    def close(self):
        """
        Close the connection to the database, if there is an opened connection
        """
        if self.connection:
            self.connection.close()
            return True
        else:
            return False

    def iter_quant_data(self):
        """
        Generator function for loading quantitative data from the DanteR SQlite
        database.

        :return: yields a tuple containing a row dictionary for the p-adjustment
        data + some metadata, and the corresponding row dictionary for the
        anova data.
        """
        sql_p_adj = "SELECT DISTINCT {p_adjustment_ANOVA_TABLE}.* FROM {p_adjustment_ANOVA_TABLE}, {Metadata_TABLE} WHERE {Metadata_TABLE}.{peptide} = {p_adjustment_ANOVA_TABLE}.{row_id};"
        sql_anova = "SELECT DISTINCT {ANOVA_TABLE}.* FROM {ANOVA_TABLE}, {Metadata_TABLE} WHERE {Metadata_TABLE}.{peptide} = {ANOVA_TABLE}.{row_id};"
        cursors = list()
        for sql in (sql_p_adj, sql_anova):
            cursor = self.connection.cursor()
            cursor.execute( sql.format(**self.id2tablerow) )
            cursors.append(cursor)

        row_id = self.id2sqlrow['row_id']
        for row_p_adj, row_anova in zip(*cursors):
            assert row_p_adj[row_id] == row_anova[row_id]
            #
            yield row_p_adj, row_anova

        for cursor in cursors:
            cursor.close()

    def get_quant_data(self):
        """
        Method for loading all ANOVA results from the DanteR SQlite database.

        :return: a list containing a row dictionary for each record in the
        database anova table + each corresponding record in the database
        p-adjustment anova table.
        """
        quant_data = list()
        for row_p_adj, row_anova in self.iter_quant_data():
            formated_row = dict()
            pval_tag = self.separator + 'pval'
            for key_ending, row_type in ( ('anova', row_anova),
                                          ('padj',  row_p_adj) ):
                for key, value in row_type.items():
                    if key.endswith(pval_tag):
                        key = key + self.separator + key_ending
                    formated_row[key] = value
            quant_data.append(formated_row)
        return quant_data

    @property
    def quant_data(self):
        """
        Lazy and memoyzed property for ANOVA results from the DanteR SQlite
        database

        :return: a list containing a row dictionary for each record in the
        database anova table + each correspodig record in the database
        p-adjustment anova table
        """
        if self._quant_data is None and self.connection:
            self._quant_data = self.get_quant_data()
        return self._quant_data

    def get_metadata(self):
        """
        Function for loading all metadata from the DanteR SQlite database

        :return: a list containing a row dictionary for each record in the
        database metadata table
        """
        sql_metadata = "SELECT DISTINCT {Metadata_TABLE}.* FROM {ANOVA_TABLE}, {Metadata_TABLE} WHERE {Metadata_TABLE}.{peptide} = {ANOVA_TABLE}.{row_id};"
        cursor = self.connection.cursor()
        cursor.execute( sql_metadata.format(**self.id2tablerow) )
        meta_data = cursor.fetchall()
        cursor.close()
        return meta_data

    @property
    def metadata(self):
        """
        Lazy and memoyzed property for metadata from the DanteR SQlite database

        :return: a list containing a row dictionary for each record in the
        database metadata table
        """
        if self._metadata is None and self.connection:
            self._metadata = self.get_metadata()
        return self._metadata

    def get_metadata_summary(self, field='peptide', *fields):
        """
        Group ``self.metadata`` (list of metadata row dictionaries) by one
        (`field`) or more (`fields`) of its row key's values and summarize some
        information about them

        :returns: a dictionary of grouped and summarized metadata row dictionaries
        """
        # Skeleton of the dictionary used for grouping:
        sumamry = defaultdict(lambda: {'n_scans': 0,
#                                       'metadata_records': list()
                                       }
                              )
        if fields:
            fields = (field,) + fields
        # Iterate over metadata rows in ``self.metadata``, and group by `field`
        # or `fields` into a ``summary`` dict:
        for row in self.metadata:
            if not fields: #One field (row key) specified
                gid = row[ self.id2sqlrow[field] ]
            else: #More fields (row keys) specified
                multiple_gid = list()
                for field in fields:
                    multiple_gid.append( row[ self.id2sqlrow[field] ] )
                gid = tuple(multiple_gid)
            # Row is from a new metadata:
            if gid not in sumamry:
                sumamry[gid]['id'] = gid
                for summary_field in ('peptide', 'proteins', '1st_protn',
                                      '1st_protac', 'phospho'):
                    sumamry[gid][summary_field] = row[ self.id2sqlrow[summary_field] ]
                for idx in range(1, 4):
                    row_ascore = row[ 'ascore{0}'.format(idx) ]
                    try:
                        row_ascore = float(row_ascore)
                    except ValueError as _:
                        row_ascore = 0
                    sumamry[gid][ 'max_ascore{0}'.format(idx) ] = row_ascore
            # Row is from a previously collected metadata:
            else:
                if sumamry[gid]['phospho'] == '1':
                    for idx in range(1, 4):
                        max_key = 'max_ascore{0}'.format(idx)
                        max_ascore = sumamry[gid][max_key]
                        row_ascore = row[ 'ascore{0}'.format(idx) ]
                        try:
                            row_ascore = float(row_ascore)
                        except ValueError as _:
                            row_ascore = 0
                        sumamry[gid][max_key] = max(max_ascore, row_ascore)
            sumamry[gid]['n_scans'] += 1
#            sumamry[gid]['metadata_records'].append(row)
        #
        return sumamry

    @property
    def metadata_summary(self):
        """
        Lazy and memoyzed property for peptide-grouped and summarized metadata
        from the DanteR SQlite database

        :returns: a dictionary of grouped and summarized metadata row dictionaries
        """
        if self._metadata_summary is None and self.connection:
            self._metadata_summary = self.get_metadata_summary()
        return self._metadata_summary


#===============================================================================
# Function definitions
#===============================================================================
def process_data(row_p_adj, row_anova, base_keys):
    processed_data = dict()
    # Get and rename ANOVA result rows:
    for key_ending, row_type in (('_anova', row_anova), ('_padj', row_p_adj)):
        for key, value in row_type.items():
            if key.endswith('_pval'):
                key = key + key_ending
            processed_data[key] = value
    # Calculate and Add FoldCahnge row:
    for key, value in processed_data.items():
        if key.endswith('_est') and value:
            key_fc = key.rstrip('est') + 'FoldChange'
            processed_data[key_fc] = 2**value
    # Calculate and Add Regulation-Description rows:
    for key, value in processed_data.items():
        if key.endswith('_FoldChange') and value:
            base_key = key.rstrip('FoldChange')
            for key_ending in ('_anova', '_padj'):
                regulation = ''
                key_pval = base_key + 'pval' + key_ending
                pval = processed_data[key_pval]
                if pval and pval < PVAL_THRESHOLD:
                    if value > FC_THRESHOLD:
                        regulation = 'Up'
                    elif value < 1/FC_THRESHOLD:
                        regulation = 'Down'
                key_regulation = base_key + 'regulation' + key_ending
                processed_data[key_regulation] = regulation

    return processed_data


def mix_qdata_metadata(processed_qdata, metadata_summary):
    mixed_data = list()
    qfield_id = SQL_ROWS['row_id']
    for row in processed_qdata:
        mixed_row = row.copy()
        metadata_row = metadata_summary[ row[qfield_id] ]
        mixed_row.update(metadata_row)
        mixed_data.append(mixed_row)
    return mixed_data


def write_meta_info(io_file, config=None):
    # Prepare meta-information:
    if config:
        prg_name = config.prg_name
        version = config.version
        updated = config.updated
        input_filen = config.file_name
    else:
        prg_name = os.path.splitext( os.path.basename( sys.argv[0] ) )[0]
        version = __VERSION__
        updated = __UPDATED__
        input_filen = None
    meta_info = [ 'Generated with program {0} version {1} (build {2}).'.format(prg_name, version, updated),
                  'Execution Date & Time: {0}'.format( datetime.now() ),
                  'p-value threshold: {0} \t Fold Change thershold: {1}'.format(PVAL_THRESHOLD, FC_THRESHOLD) ]
    if input_filen:
        size = os.path.getsize(input_filen)
        meta_info.append( 'DanteR DB file name: {0} ({1} bytes)'.format(input_filen, size) )
    meta_info.append('') #A blank line.
    meta_info.extend( ['_est columns: \t log2(ratio)',
                       'Regulation: \t Up: if p-value < p-value threshold, and Fold Change > Fold Change threshold.',
                       '\t Down: if p-value < p-value threshold, and Fold Change < (1 / Fold Change threshold).'] )
    meta_info.extend( ['', ''] ) #Two blank lines.
    # Write meta-information:
    io_file.write( '\n'.join(meta_info) )
    io_file.flush()


def csv_dict_writer(filen, dictrows, fieldnames=None, config=None):
    if not fieldnames:
        fieldnames = sorted( dictrows[0].keys() )
    with open(filen, 'wb') as io_file:
        write_meta_info(io_file, config)
        csvdw = csv.DictWriter(io_file, fieldnames=fieldnames, restval='',
                               extrasaction='ignore', delimiter='\t')
        csvdw.writeheader()
        csvdw.writerows(dictrows)


def main(*args):
    config = Configuration(CLI_PARAMS, version=__VERSION__, created=__CREATED__,
                           updated=__UPDATED__)

    try:
        wellcome_msg = ['Welcome to {0} program.'.format(config.prg_name),
                        'Version {0} (build {1})'.format(config.version, config.updated),
                        '']
        if config.verbose > 0:
            wellcome_msg.append(' Verbose mode On')
            if config.interactive:
                wellcome_msg.append(' Interactive mode: ON')
        config.ui.msgbox( '\n'.join(wellcome_msg), config.prg_name )

        can_ask_the_user = config.interactive or config.ui.gui

        if not config.file_name and can_ask_the_user:
            config.file_name = config.ui.fileopenbox( 'Name of the DanteR SQlite file to Open',
                                                       config.prg_name + ' Open...',
                                                       filetypes=[['*.db', 'DanteR SQlite output files']] )
        if not config.output_file:
            if config.file_name:
                config.output_file = os.path.splitext(config.file_name)[0] + '.xls'
            if can_ask_the_user:
                output_file = config.ui.filesavebox( 'Name of the file to Save the result',
                                                     config.prg_name + ' Save as...',
#                                                            default=config.output_file,
                                                     filetypes=[['*.xls', '*.tsv', 'Spreadsheet-like TSV file']] )
            if output_file is None:
                raise ProgramCancelled()
            if not os.path.splitext(output_file)[1]: #No extension given to file:
                output_file += '.tsv'
            config.output_file = output_file

        if not config.file_name or not config.output_file:
            if config.verbose > 0:
                config.ui.msgbox('Not enought data supplied for processing!!\n',
                                 config.prg_name + ' Error!')
            config.parser.print_usage()
            return 1

        if can_ask_the_user and not config.ui.ynbox():
            return 1

        # Load and Process data from DanteR database:
        config.ui.nonblockingmsgbox('Loading data from DanteR database...',
                                    config.prg_name + ' Working, please wait ...')
        db = DanteDB(config.file_name, SQL_TABLES, SQL_ROWS)
        processed_data = list()
        for row_p_adj, row_anova in db.iter_quant_data():
            processed_data.append( process_data(row_p_adj, row_anova,
                                                db.results_base_keys) )
        config.ui.nonblockingmsgbox('Processing data from DanteR database...',
                                    config.prg_name + ' Working, please wait ...')
        output_data = mix_qdata_metadata(processed_data, db.metadata_summary)
        db.close()
        # Save data to TSV file:
        config.ui.nonblockingmsgbox('Saving data to TSV file...',
                                    config.prg_name + ' Working, please wait ...')
        datafields = sorted( [field for field in output_data[0].keys()
                              if field not in OUTPUT_METADATA_HEADERS] )
        fieldnames = OUTPUT_METADATA_HEADERS + datafields
        fieldnames.remove( SQL_ROWS['row_id'] )
        fieldnames.remove( 'id' )
        csv_dict_writer(config.output_file, output_data, fieldnames, config)
        config.ui.cli_module.nonblockingmsgbox().close()
        #
        config.ui.msgbox('Ok. Job finished', config.prg_name + ' Finished!')
        return 0

    except (KeyboardInterrupt, ProgramCancelled):
        ### Handle keyboard interrupt, and program cancellation
        config.ui.msgbox('Program Cancelled by user!',
                         config.prg_name + ' Cancelled!')
        return 1

    except SystemExit:
        ### Handle system exit (for example when -h)
        return 0

    # Un-comment when in production mode:
    except Exception as e:
        if DEBUG:
            raise(e)

        indent = (len(config.prg_name) + 2) * ' '
        err_title = config.prg_name + ': Internal Error'
        err_msg = e.message
        err_trace = traceback.format_exc()
        sys.stderr.write(err_title + '\n')
        sys.stderr.write(indent + err_msg + '\n')
        sys.stderr.write(err_trace)
        config.ui.textbox(msg='ERROR!: '+err_msg, title=err_title,
                          text=err_trace)
        return 2



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    if DEBUG:
        sys.argv.append('-v')
        sys.argv.append('-i')
#        sys.argv.append('--force-cli') #DEBUG cli
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'DanteRDB2TSV_profile.txt'
        cProfile.run('main()', profile_filename)
        with open('DanteRDB2TSV_profile_stats.txt', 'wb') as statsfile:
            p = pstats.Stats(profile_filename, stream=statsfile)
            stats = p.strip_dirs().sort_stats('cumulative')
            stats.print_stats()
        sys.exit(0)
    #
    exit_code = main()
    #
    sys.exit(exit_code)