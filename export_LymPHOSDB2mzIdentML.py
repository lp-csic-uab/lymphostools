#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
'''
Created on 2013/09/19

@authors: Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
@version: 0.8
'''

#===============================================================================
# Imports
#===============================================================================
from __future__ import division

from tools_commons import LymPHOS_settings, DB_SubSet, PepProtFuncs
from mzIdentML_exporter_class import MZIDGenerator

import os
from datetime import datetime
from collections import namedtuple

#===============================================================================
# Global variables
#===============================================================================
from mzIdentML_globals import PROVIDER, EXP_SEARCHPROTOCOLS, FASTA_FULLFILEN

EXPERIMENTS = [(35,)] #List with tuples of experiment IDs to process in packs. Empty list or None for all.

QUANTITATIVE = None #True: Only quantitative data, False: only non-quantitative data, None: both
PHOSPHO = None #True: Only phosphorilated data, False: only non-phosphorilated data, None: both

OUT_FILEN = 'LymPHOS_Export_Exp_{0}'


#===============================================================================
# Class definitions
#===============================================================================
MGFInfo = namedtuple('MGFInfo', ['name', 'fullname', 'scans2index'])
FASTAInfo = namedtuple('FASTAInfo', ['name', 'fullname', 'num_seqs'])


#===============================================================================
# Function definitions
#===============================================================================
def save_iterable(file_name, iterable):
    with open(file_name, 'wb') as iofile:
        iofile.write( '\n'.join(sorted( iterable )) )


def _get_scans_string(data_record):
    if data_record.scan_i == data_record.scan_f:
        scans = str(data_record.scan_i)
    else:
        scans = '{0}-{1}'.format(data_record.scan_i, 
                                 data_record.scan_f)
    return scans

def _get_charge_string(data_record):
    if data_record.charge < 0:
        charge = str(abs(data_record.charge)) + '-'
    else:
        charge = str(data_record.charge) + '+'
    return charge

def generate_mgf(spectra, filename):
    '''
    Generates a MGF file (name = filename) with all the spectrum within spectra,
    ussing specifications in http://www.matrixscience.com/help/data_file_help.html
    
    Returns a dictionary (scans2indexinmgf) containing {scans -> index in the
    mgf} references
    '''
    scans2indexinmgf = dict()
    with open(filename, 'wb') as io_file:
        # Information header:
        io_file.write('# Generated from LymPHOS v{0} on {1}\n'
                      '#  http://www.lymphos.org\n\n'\
                      '# {2}\n'\
                      '# (cc) BY-NC-SA '\
                      '( http://creativecommons.org/licenses/by-nc-sa/3.0/'\
                      ' )\n\n'\
                      'MASS=Monoisotopic\n'.format(
                             LymPHOS_settings.__VERSION__, 
                             datetime.now().ctime(),
                             LymPHOS_settings.__AUTHORS__)
                      )
        # Spectrum data:
        for index, spectrum in enumerate(spectra):
            # Spectrum begining and meta-data header:
            charge = _get_charge_string(spectrum.data)
            scans = _get_scans_string(spectrum.data)
            scans2indexinmgf[scans] = index
            io_file.write('BEGIN IONS\n'\
                          'TITLE=Spectrum MS{0} Id {1} scans: {5} index: {6}\n'\
                          'PEPMASS={2:.3f}\n'\
                          'CHARGE={3}\n'\
                          'RTINSECONDS={4}\n'\
                          'SCANS={5}\n'.format( 
                             spectrum.ms_n, spectrum.pk, spectrum.data.mass,
                             charge, spectrum.data.rt, scans, index)
                          )
            # Spectrum mz/intensity ion pairs:
            for mz, intensity in zip(spectrum.mz_as_strings, spectrum.intensity_as_strings):
                io_file.write('{0} {1}\n'.format(mz, intensity))
            # Spectrum ending:
            io_file.write('END IONS\n\n')
    return scans2indexinmgf


def main(onefile4fraction=True):
    '''
    Main program control
    '''
    for exp_subset in EXPERIMENTS:
        sub_db = DB_SubSet(exp_subset, PHOSPHO, QUANTITATIVE)
        searchprotocols = EXP_SEARCHPROTOCOLS['EXP_COMMON']
        searchprotocols.update( EXP_SEARCHPROTOCOLS.get(exp_subset, dict()) )
        if onefile4fraction:
            raw_files = sub_db.get_raw_files()[0:1] #TEST: only 1 file to write
        else:
            raw_files = [( None, 
                           OUT_FILEN.format('-'.join(map(str, exp_subset))) )]
        for raw_file, filename in raw_files:
            mzid_filen = filename + '.mzid'
            raw_file_sub_db = DB_SubSet(exp_subset, PHOSPHO, QUANTITATIVE, raw_file)
            data_records = raw_file_sub_db.get_data()
            proteins = raw_file_sub_db.get_proteins(seq__gt='') #X-NOTE: We cannot process deleted proteins -> Errors in MZID
            spectra = raw_file_sub_db.get_spectra()
            mgf_filen = filename + '.mgf'
            spectra_info = MGFInfo(name=mgf_filen, 
                                   fullname=os.path.join(os.getcwd(), mgf_filen), 
                                   scans2index=generate_mgf(spectra, mgf_filen)
                                   )
            fasta_info = FASTAInfo(name=os.path.split(FASTA_FULLFILEN)[1], 
                                   fullname=FASTA_FULLFILEN, 
                                   num_seqs=len(PepProtFuncs.load_fasta_data(FASTA_FULLFILEN))
                                   )                       
            engines = raw_file_sub_db.get_search_engines()
            mzid_generator = MZIDGenerator(mzid_filen, data_records, proteins, 
                                           spectra_info, engines, 
                                           raw_file_sub_db, fasta_info)
            common_unimods = set(searchprotocols['ENG_COMMON']['search_unimods'])
            label2unimod = {'iTRAQ': 214, 'TMT': 737}
            for label in raw_file_sub_db.get_labelings():
                common_unimods.add(label2unimod[label])
            searchprotocols['ENG_COMMON']['search_mods'] = common_unimods
            mzid_generator.write_all(provider = PROVIDER,
                                     searchprotocols = searchprotocols)
            mzid_generator.close()


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    time0 = datetime.now()
    #
    main()
    #        
    dtime = datetime.now() - time0
    print('\nData exported in {0} seconds'.format(dtime.seconds))
    print('\nEnd of program execution.')
