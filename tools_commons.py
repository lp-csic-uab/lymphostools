#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
:synopsis: Phospho-proteomic classes and functions (arranged in classes) used in
different LymPHOS Tools.

:created:    2013/09/19

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    1.1
:updated:    2015-08-18
"""

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
from __future__ import division

import sys
import os

# LymPHOS Configuration Paths:
if os.name == 'posix':
    LYMPHOS_BASE_PATH = r'/mnt/Dades/Laboratorio Proteomica CSIC-UAB/Programas/LymPHOS Web/src'
elif os.name == 'nt':
    LYMPHOS_BASE_PATH = r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src'
sys.path.append(LYMPHOS_BASE_PATH)
LYMPHOS_SRC_PATH = os.path.join(LYMPHOS_BASE_PATH, 'LymPHOS_v1_5')
sys.path.append(LYMPHOS_SRC_PATH)

# LymPHOS Environment:
import django
if django.get_version() < '1.5':
    from django.core import management
    import LymPHOS_v1_5.settings as LymPHOS_settings
    management.setup_environ(LymPHOS_settings)
    from LymPHOS_v1_5.lymphos import models
else: #Changes from django version 1.5 :
    os.environ['DJANGO_SETTINGS_MODULE'] = 'LymPHOS_v1_5.settings'
    from django.conf import settings as LymPHOS_settings
    if django.get_version() >= '1.7': #Changes from django version 1.7 :
        from django.core.wsgi import get_wsgi_application
        application = get_wsgi_application()
        from lymphos import models
    else: #django versions >=1.5 and < 1.7 :
        from LymPHOS_v1_5.lymphos import models

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum

from commons import mass

import re
import math
import numpy as np
from scipy import histogram
from scipy.optimize import curve_fit
from scipy import stats
from collections import defaultdict, namedtuple


#===============================================================================
# Global variables
#===============================================================================
from mzIdentML_globals import DB2CV_ENGINES


#===============================================================================
# Class definitions
#===============================================================================
PSiteSeqData = namedtuple('PSiteData', ['proteins', 'peptides'])


ProteinPSiteData = namedtuple('ProteinPSiteData', ['protein', 'ac', 'position', 
                                                   'aa'])


PeptidePSiteData = namedtuple('PeptidePSiteData', ['peptide', 'seq', 'ascore'])


class PSite(object):
    def __init__(self, prot_ac=None, position=0, p_aa='', max_ascore=0, 
                 time_regulation=None, supporting_peps=None):
        self.prot_ac = prot_ac
        self.position = position
        self.p_aa = p_aa
        self.max_ascore = max_ascore
        if not time_regulation:
            self.time2regulations = dict() # {time: regulation} ; and regulation can be -1 (down), 0 (no-change), or 1 (up)
        if not supporting_peps:
            self.supporting_peps = set()
    
    def __repr__(self):
        return ('PSite({prot_ac}, {position}, {p_aa}, {max_ascore}, '
                '{time_regulation}, {supporting_peps})').format(**self.__dict__)


class DB_SubSet(object):
    """
    Class to act only over a filtered sub-set of the LymPHOS DataBase
    """
    def __init__(self, experiments=None, phospho=None, quant=None, 
                 raw_file=None, **extra_filters):
        self._filters = extra_filters
        self.experiments = experiments
        self.phospho = phospho
        self.quant = quant
        self.raw_file = raw_file
        self._db_updated = False
    
    @property
    def experiments(self):
        return self._filters.get('experiment__id__in', None)
    @experiments.setter
    def experiments(self, value):
        self._db_updated = True
        if value:
            self._filters['experiment__id__in'] = value
        else:
            self._filters.pop('experiment__id__in', None)
    
    @property
    def phospho(self):
        return self._filters.get('phospho', None)
    @phospho.setter
    def phospho(self, value):
        self._db_updated = True
        if value is not None:
            self._filters['phospho'] = value
        else:
            self._filters.pop('phospho', None)
    
    @property
    def quant(self):
        value = self._filters.get('dataquant__isnull', None)
        if value is not None:
            value = not value
        return value
    @quant.setter
    def quant(self, value):
        self._db_updated = True
        if value is not None:
            self._filters['dataquant__isnull'] = not value
        else:
            self._filters.pop('dataquant__isnull', None)
    
    @property
    def raw_file(self):
        return self._filters.get('raw_file', None)
    @raw_file.setter
    def raw_file(self, value):
        self._db_updated = True
        if value:
            self._filters['raw_file'] = value
        else:
            self._filters.pop('raw_file', None)
    
    def get_spectra(self, **extra_filters):
        spectra_filters = dict()
        for key, value in self._filters.items():
            spectra_filters['data__' + key] = value
        spectra_filters.update(extra_filters)
        #
        query = models.Spectra.objects.filter(**spectra_filters).order_by('data__scan_i').distinct()
        return query  
    
    def get_proteins(self, pep_names=None, with_decoys=False, **extra_filters):
        prot_filters = dict()
        for key, value in self._filters.items():
            prot_filters['data__' + key] = value
        prot_filters.update(extra_filters)
        if pep_names:
            prot_filters['data__peptide__in'] = pep_names
        if not with_decoys:
            prot_filters['is_decoy'] = False
        #
        query = models.Proteins.objects.filter(**prot_filters).distinct()
        return query
    
    def get_data(self, with_decoys=False, **extra_filters):
        data_filters = self._filters
#         if not with_decoys:
#             extra_filters['_proteins__is_decoy']=False #X-NOTE: This doesn't filter data records with mixed decoys and non-decoys proteins.
        data_filters.update(extra_filters)
        #
        query = models.Data.objects.filter(**data_filters).distinct()
        if not with_decoys:
            query = query.exclude(_proteins__is_decoy=True)
        return query  
    
    def get_peptidecache(self, with_decoys=False, **extra_filters):
        peptidecache_filters = dict()
        # Adapt DB_SubSet properties to peptidecache filters:
        for peptidecache_filter, db_property in ( ('phospho', 'phospho'), 
                                                  ('quantitative', 'quant') ):
            property_value = getattr(self, db_property)
            if property_value is not None:
                peptidecache_filters[peptidecache_filter] = property_value
        peptidecache_filters.update(extra_filters)
        #
        query = models.PeptideCache.objects.filter(**peptidecache_filters)
        if not with_decoys:
            query = query.exclude(dbproteins__contains='decoy')
        return query  
    
    def get_peptides(self, **extra_filters):
        """
        Returns a dict of pep_orig dictionaries (grouped by original peptide
        (pep_orig)) with some summarized information about them.
        """
        data_records = self.get_data(**extra_filters).order_by('pep_orig')
        return PepProtFuncs.peptides_by_pep_orig(data_records)
    
    def get_dataquants(self, pep_names=None, exp_groups=None, **extra_filters):
        dq_filters = dict()
        for key, value in self._filters.items():
            dq_filters['data__' + key] = value
        dq_filters.update(extra_filters)
        if pep_names:
            dq_filters['unique_pep__in'] = pep_names
        if exp_groups:
            dq_filters['quantpepexpcond__expgroup__in'] = exp_groups
        #
        query = models.DataQuant.objects.filter(**dq_filters).distinct()
        return query

    def get_quantpepexpconds(self, pep_names=None, exp_groups=None, 
                             **extra_filters):
        qpec_filters = dict()
        for key, value in self._filters.items():
            qpec_filters['dataquant__data__' + key] = value
        qpec_filters.update(extra_filters)
        if pep_names:
            qpec_filters['unique_pep__in'] = pep_names
        if exp_groups:
            qpec_filters['expgroup__in'] = exp_groups
        #
        query = models.QuantPepExpCond.objects.filter(**qpec_filters).distinct()
        return query
        
    def get_peps_for_prot(self, protein_ac, **extra_filters):
        """
        Returns a dict of pep_orig dictionaries (grouped by original peptide
        (pep_orig)) for a protein AC, and with some summarized information about
        them.
        """
        extra_filters['_proteins__ac'] = protein_ac
        return self.get_peptides(**extra_filters)
    
    def get_search_engines(self, **extra_filters):
        """
        Returns a dict of db2cv_engine dictionaries corresponding to the search
        engines used in the current sub- database set
        """
        engines = dict()
        data_records = self.get_data(**extra_filters)
        for engine, db2cv_engine in DB2CV_ENGINES.items():
            if data_records.aggregate(sum=Sum(db2cv_engine['field']))['sum']:
                engines[engine] = db2cv_engine
        return engines
    
    def get_raw_files(self, **extra_filters):
        data_records = self.get_data(**extra_filters)
#        raw_files = data_records.values('raw_file').annotate(Count('raw_file')).values_list('raw_file', flat=True)
        raw_files = set(data_records.values_list('raw_file', flat=True))
        filenames = [raw_file.rpartition('.')[0] for raw_file in raw_files]
        return zip(raw_files, filenames)
    
    def get_labelings(self, **extra_filters):
        data_records = self.get_data(**extra_filters)
        labels = set(data_records.values_list('experiment__label', flat=True))
        labels.discard('')
        return labels
    
    def get_quant_times(self, **extra_filters):
        quantpepexpconds = self.get_quantpepexpconds(**extra_filters)
        dbquant_times = set(quantpepexpconds.values_list('dballquanttimes', 
                                                         flat=True)
                            )
        dbquant_times.discard(u'')
        quant_times = set()
        for dbtimes in dbquant_times:
            for time in dbtimes.split('|'):
                quant_times.add(int(time))
        return quant_times


class PepProtFuncs(object):
    """
    Class grouping different functions to manipulate peptide and protein 
    sequences and or collections 
    """
    UNIMOD = {4:  {'unimod_id': 4,
                   'delta_mass': 57.021464,
                   'unimod_name': 'Carbamidomethyl',
                   'residues': 'C',
                   'fixed': 'true',
                   },
              21: {'unimod_id': 21,
                   'delta_mass': 79.966331,
                   'unimod_name': 'Phospho',
                   'residues': 'S T Y',
                   'fixed': 'false',
                   },
              23: {'unimod_id': 23,
                   'delta_mass': -18.010565,
                   'unimod_name': 'Dehydrated',
                   'residues': 'S T Y',
                   'fixed': 'false',
                   },
              35: {'unimod_id': 35,
                   'delta_mass': 15.994915,
                   'unimod_name': 'Oxidation',
                   'residues': 'M',
                   'fixed': 'false',
                   },
              214:{'unimod_id': 214,
                   'delta_mass': 144.102063,
                   'unimod_name': 'iTRAQ4plex',
                   'residues': '.',
                   'fixed': 'false',
                   },
              737:{'unimod_id': 737,
                   'delta_mass': 229.162932,
                   'unimod_name': 'TMT6plex',
                   'residues': '.',
                   'fixed': 'false',
                   },
             }
   
    @staticmethod
    def new_protein_from_fasta(seq_header, seq_buffer):
        new_prot = models.Proteins()
        new_prot.ac = seq_header.split('|')[1]
        new_prot.name = seq_header
        new_prot.seq = ''.join(seq_buffer)
        return new_prot
    
    @classmethod
    def load_fasta_data(cls, fasta_filen):
        fasta_prots = dict()
        #
        with open(fasta_filen, 'r') as io_file:
            seq_header = ''
            seq_buffer = list()
            curr_prot = None
            for row in io_file:
                row = row.strip()
                if row[0] == '>':
                    if seq_header and seq_buffer:
                        curr_prot = cls.new_protein_from_fasta(seq_header, 
                                                               seq_buffer)
                        fasta_prots[curr_prot.ac] = curr_prot
                    seq_buffer = list()
                    seq_header = row
                else:
                    seq_buffer.append(row)
            if seq_header and seq_buffer:
                curr_prot = cls.new_protein_from_fasta(seq_header, seq_buffer)
                fasta_prots[curr_prot.ac] = curr_prot
        #
        return fasta_prots
    
    @staticmethod
    def proteins_by_ac(proteins):
        return {prot.ac: prot for prot in proteins}
    
    @staticmethod
    def proteins_by_pep(proteins):
        proteins_pep = defaultdict(list)
        for protein in proteins:
            for data_record in proteins.data.all():
                proteins_pep[data_record.peptide].append(protein)
        return proteins_pep
    
    @staticmethod
    def peptides_by_attr(data_records, attr, *attrs):
        """
        Group data_records (iterable of Data objects) by one (attr) or more
        (attrs) of its attributes/properties values and summarize some
        information about them.
        """
        # Skeleton of the dictionary used for grouping:
        peptides = defaultdict(lambda: {'n_scans': 0, 
                                        'quantitative': False,
                                        'data_records': list()})
        if attrs:
            attrs = (attr,) + attrs
        # Iterate over record Data instances in data_records (a QuerySet with
        # experimental peptide data), and group by pep_orig in peptides dict:
        for record in data_records:
            if not attrs:
                gid = GeneralFuncs.deep_getattr(record, attr)
            else:
                multiple_gid = list()
                for an_attrr in attrs:
                    multiple_gid.append(GeneralFuncs.deep_getattr(record, 
                                                                  an_attrr)
                                        )
                gid = tuple(multiple_gid)
            # Record is from a new peptide:
            if gid not in peptides:
                peptides[gid]['pid'] = gid
                peptides[gid]['pep_orig'] = record.pep_orig
                peptides[gid]['peptide'] = record.peptide.upper()
                peptides[gid]['calc_mass'] = PepProtFuncs.pep_mod_mass(peptides[gid])
                l_prots = record.proteins_w_decoys.all().values_list('ac',      #TODO: get proteins from FASTA_FILEN
                                                                     flat=True) #
                peptides[gid]['l_prots_w_decoys'] = set(l_prots)                #
                peptides[gid]['psites'] = record.get_psites()
                peptides[gid]['phospho'] = record.phospho
                peptides[gid]['max_ascores'] = record.get_ascores()
            # Record is from a previously collected phospho-peptide:
            else:
                l_prots = record.proteins_w_decoys.all().values_list('ac',      #TODO: get proteins from FASTA_FILEN
                                                                     flat=True) #
                peptides[gid]['l_prots_w_decoys'].update(l_prots)               #
                if peptides[gid]['phospho']:
                    grouped_ascores = zip(peptides[gid]['max_ascores'], 
                                          record.get_ascores())
                    peptides[gid]['max_ascores'] = map(max, grouped_ascores)
            # Always, afterwards/finally, do:
            try:
                if not peptides[gid]['quantitative'] and record.qdata \
                   and record.dataquant:
                    peptides[gid]['quantitative'] = True
            except ObjectDoesNotExist:
                pass
            peptides[gid]['n_scans'] += 1
            peptides[gid]['data_records'].append(record)
        #
        return peptides

    @classmethod
    def peptides_by_pep_orig(cls, data_records):
        """
        Group data_records (iterable of Data objects) by original peptide
        attribute ('pep_orig') and summarize some information about them.
        """
        return cls.peptides_by_attr(data_records, 'pep_orig')
    
    @staticmethod
    def peptides_by_ac(peptides_dict):
        """
        Group peptides_dict (dict of peptide dicts) by proteins AC (ac).
        """
        # Skeleton of the dictionary used for grouping:
        ac2peps = defaultdict(dict)
        # Iterate over peptide dicts in peptides_dict, and group by
        # peptide['l_prots_w_decoys'] ac into ac2peps dict:
        for pid, peptide in peptides_dict.items():
            for ac in peptide['l_prots_w_decoys']:
                ac2peps[ac][pid] = peptide
        return ac2peps
    
    @classmethod
    def find_mods(cls, pep_orig, unimods=None, fixed_mods=None):
        """
        Locates UNIMOD modifications, and returns a list of dictionaries
        containing location and UNIMOD information
        """
        if not unimods:
            unimods = cls.UNIMOD
        if not fixed_mods: #TODO: make it more general not so case-specific
            # Find carbamidomethylations X-NOTE: default static modification:
            fixed_mods = {'C': cls.UNIMOD[4]}
        pos = 0
        mods = list()
        aas_and_mods = re.split('[\\)\\(, ]', pep_orig)
        for aas_or_mods in aas_and_mods: #Mods numbers (unimods)
            if aas_or_mods.isdigit():
                mod = unimods[int(aas_or_mods)].copy()
                mod['pos'] = pos
                mods.append(mod)
            else: #AAs letters (fixed_mods)
                for residue, unimod in fixed_mods.items():
                    sub_pos = 0
                    c_pos = aas_or_mods.find(residue, sub_pos)
                    while c_pos > -1:
                        sub_pos = c_pos + 1
                        mod = unimod.copy()
                        mod['pos'] = pos + sub_pos
                        mods.append(mod)
                        c_pos = aas_or_mods.find(residue, sub_pos)
                pos += len(aas_or_mods)
        return mods
    
    @staticmethod
    def old_regex_format(rs_search, regex_replace):
        """
        #DEPRECATED: See new method regex_format()
        Formating of rs_search string according to regex_replace dictionary
        """
        rl_search = list()
        rkeys = regex_replace.keys()
        for each in list(rs_search):
            if each in rkeys:
                each = regex_replace[each]
            rl_search.append(each)
        return ''.join(rl_search)
    
    @staticmethod
    def regex_format(original, replace_tbl=None):
        """
        Formating of original string according to a replace_tbl dictionary 
        ( {each: regex} ).
        Ex.: AAABAAXAAI -> AAA[BND]AA.AA[IJ]
        """
        if not replace_tbl: #Default replace table for amino-acids
            replace_tbl = {'B': '[BND]',
                           'Z': '[ZEQ]',
                           'J': '[JIL]',
                           'X': '[.]',
                           'I': '[IJ]',
                           'L': '[LJ]',
                           'Other': '{0}',
                           #'Global': '{0}', #Return the resulting regex, without using a lookahead assertion to accelerate searches
                           'Global': '(?={0})', #Add a lookahead assertion (allows overlaping matches, but those matches are void: X-NOTE: http://stackoverflow.com/a/11430936) and return the regex
                           }
        regex_search = list()
        rkeys = replace_tbl.keys()
        for each in list(original):
            if each in rkeys:
                each = replace_tbl[each]
            else:
                each = replace_tbl['Other'].format(each)
            regex_search.append(each)
        return replace_tbl['Global'].format(''.join(regex_search))
    
    @classmethod
    def re_search_pep_in_prots(cls, pep, prots):
        prots_w_peppos = list()
        pep = cls.regex_format(pep.upper())
        #
        for prot in prots:
            pep_pos = list()
            for match in re.finditer(pep, prot.seq):
                pep_pos.append((match.start()+1, match.end()))
            prots_w_peppos.append( (prot, pep_pos) )
        #
        return prots_w_peppos
    
    @classmethod
    def protein_coverage(cls, protein, peptides):
        covered_aa_pos = set() #Amino-acids positions covered by peptides
        for peptide in peptides.values():
            pep_prots = [protein]
            prots_w_peppos = cls.re_search_pep_in_prots(peptide['peptide'], 
                                                                pep_prots)
            for _, pep_poss in prots_w_peppos:
                for pep_pos in pep_poss:
                    start, end = pep_pos
                    covered_aa_pos.add(range(start, end+1))
        # Coverage calculus:
        return len(covered_aa_pos) / len(protein.seq) * 100
    
    @staticmethod
    def mean_of_peps_sds(data_redords):
        stdvs = defaultdict(list)
        for data_redord in data_redords:
            try:
                time_values = data_redord.dataquant.time_values()
            except ObjectDoesNotExist: #data_record has not dataquant related records (is not quantitative)
                continue
            for time, values in time_values.items():
                stdvs[time].append( values[2] ) #values[2] == stdv (see :method:`time_values` of :class:`DataQuant` in lymphos/models.py)
        means = dict()
        for time in stdvs.keys():
            means[time] = np.mean( stdvs[time] )
        return means
    
    @classmethod
    def pep_mod_mass(cls, peptide, unimods=None, fixed_mods=None):
        pep_seq = peptide['peptide']
        pep_mass = mass.get_mass(pep_seq)
        #
        pep_orig = peptide['pep_orig']
        mods = cls.find_mods(pep_orig)
        for mod in mods:
            pep_mass += mod['delta_mass']
        return pep_mass
    
    @classmethod
    def get_Psite_seqs_from_Ppeptides(cls, peptides, protein, prot_seq=None, 
                                      extend=7, phospho_sites=None):
        """
        Extend by <extend> amino acids the C- and N-terms of each phospho site
        found in the protein from the peptide sequence in peptides iterable
    
        Returns a phospho_sites defaultdict: 
          {phospo_site_seq: ( set(ProteinPSiteData), set(PeptidePSiteData) ), ...}
        Allows direct modification of a supplied phospho_sites defaultdict (acts like a ByRef parameter):
          defaultdict( lambda: PSiteSeqData( set(), set() ) )
    
        Highly Modified from enlarge_sequences() in tools.phoscripts.kinase_motifs
        """
        if not prot_seq:
            prot_seq = protein.seq
        extreme = '-' * extend
        prot_seq = extreme + prot_seq + extreme
        #
        if not isinstance(phospho_sites, defaultdict): #Allows direct modification of a supplied phospho_sites defaultdict
            phospho_sites = defaultdict( lambda: PSiteSeqData( set(), set() ) )
        #
        for pep in peptides:
            pep_seq = pep.peptide
            reges_sequence = cls.regex_format(pep_seq)
            found = False
            for match in re.finditer(reges_sequence, prot_seq):
                found = True
                for pep_psite, pep_ascore in zip(pep.psites, pep.ascores):
                    start = match.start() + pep_psite
                    tail = prot_seq[ start-extend : start ]
                    head = prot_seq[ start+1 : start+1+extend ]
                    sec = tail + pep_seq[pep_psite] + head
                    phospho_sites[sec].proteins.add( ProteinPSiteData(protein, 
                                                                      protein.ac, 
                                                                      start, 
                                                                      prot_seq[pep_psite]) )
                    phospho_sites[sec].peptides.add( PeptidePSiteData(pep, pep_seq, 
                                                                      pep_ascore) )
            #
            if not found:
                print 'not found {0}'.format(pep_seq)
                cls.get_Psite_seqs_from_Ppeptides([pep], protein, pep_seq.upper(), #Mock recursion where prot_seq is subtituted with pep_seq.upper()
                                                  extend, phospho_sites)
        #
        return phospho_sites
    
    @classmethod
    def get_Psites4prot(cls, protein, phospho_sites=None):
        """
        :params protein: a :class:`lymphos.models.Proteins' instance.
        :params phospho_sites: an instance of :class:`defaultdict`
        (``defaultdict( PSite )``) to be modified in place (acts like a ByRef
        parameter).
        
        :returns: a defaultdict of sequence positions to :class:`PSite`
        objects::
          {position_in_seq: PSite(prot_ac, position, p_aa, max_ascore, supporting_peps), ...}
        """
        if not isinstance(phospho_sites, defaultdict): #Allows direct modification of a supplied phospho_sites defaultdict
            phospho_sites = defaultdict(PSite)
        #
        for pep in protein.phosphopeptides:
            pep_seq_regex = cls.regex_format(pep.peptide.upper())
            found = False
            for match in re.finditer(pep_seq_regex, protein.seq):
                found = True
                for pep_psite, pep_ascore in zip(pep.psites, pep.ascores):
                    pos = match.start() + pep_psite + 1 #Biological position in protein (start at 1).
                    p_site = phospho_sites[pos] #Get the PSite object for in-situ modification:
                    if not p_site.position: #New protein p-site
                        p_site.position = pos
                        p_site.prot_ac = protein.ac
                        p_site.p_aa = protein.seq[pos-1]
                    p_site.max_ascore = max(p_site.max_ascore, pep_ascore)
                    p_site.supporting_peps.add(pep)
            #
            if not found:
                print 'not found {0} in protein {1}'.format(pep.peptide, protein.ac)
#                 phospho_sites[0].supporting_peps.add(pep)
        #
        return phospho_sites


class GeneralFuncs(object):
    """
    Class grouping different general functions
    """
    @staticmethod
    def deep_getattr(obj, deep_attr, *args):
        not_set = object()
        default = not_set
        if args and len(args) == 1:
            default = args[0]
        elif len(args) > 1:
            error = TypeError('deep_getattr expected at most 3 arguments')
            raise error
        chained_attrs = deep_attr.split('.')
        value = obj
        for attr in chained_attrs:
            if default is not not_set:
                value = getattr(value, attr, default)
            else:
                value = getattr(value, attr)
        return value
    
    @classmethod
    def group_by_attrs(cls, objects, attr, *attrs):
        """
        Group objects (iterable of objects) by one (attr) or more (attrs) of its
        attributes/properties values
        """
        grouped = defaultdict(set)
        if attrs:
            attrs = (attr,) + attrs
        # Iterate over objects and group by gid in a dictionary of sets:
        for obj in objects:
            if not attrs:
                gid = cls.deep_getattr(obj, attr)
            else:
                multiple_gid = list()
                for an_attr in attrs:
                    multiple_gid.append(cls.deep_getattr(obj, an_attr))
                gid = tuple(multiple_gid)
            grouped[gid].add(obj)
        #
        return grouped
    
    @staticmethod
    def save_iterable(file_name, str_iterable):
        """
        Save a string iterable (str_iterable) to file (file_name)
        """
        with open(file_name, 'wb') as iofile:
            iofile.write( '\n'.join(str_iterable) )
    

class MathFuncs(object):
    """
    Class grouping different math functions
    """
    # Functions gauss() and calc_normal() copied and adapted from LymPHOS
    # PQuantifier pq_analyze.py at revision 123:
    @staticmethod
    def gauss(x, A, mu, sigma):
        """
        Returns f(x) for a gaussian function.
        X-NOTE: https://en.wikipedia.org/wiki/Normal_distribution
        In the canonical equation A = 1 / (sigma * (2 * pi)**0.5)
        """
        x = np.array(x)
        return A * np.exp(-(x-mu)**2 / (2*sigma**2))
    #
    @classmethod
    def calc_normal(cls, x, limits=(-10,10)):
        """
        Calculates mu, sigma from a gaussian fit of the x distribution.
        Curve fitting produces wrong results for distributions with few values:
        bins with 0 frequency affects calculation. This must be addressed.
        """
        nbins = 600
        xmin, xmax = limits
        bins = np.linspace(xmin, xmax, nbins)
    
        n, _ = histogram(x, bins)
        diff = (bins[1] - bins[0]) / 2
        bin_centers = [bin+diff for bin in bins[:-1]]
        A, mu, sigma = curve_fit(cls.gauss, bin_centers, n)[0]
    
        #gauss curve_fit sometimes produces negative sigmas
        sigma = abs(sigma)
    
        return A, mu, sigma, limits, nbins

    @staticmethod
    def mix_normals(exp_normals, exp_pack=None):
        """
        X-NOTE: https://en.wikipedia.org/wiki/Mixture_density#Moments
        """
        if exp_pack:
            exps = list( set(exp_pack).intersection( exp_normals.keys() ) )
        else:
            exps = exp_normals.keys()
        
        mixed_normal = exp_normals[exps[0]]
        for exp in exps[1:]:
            n = mixed_normal['n'] + exp_normals[exp]['n']
            summed_p = mixed_normal['n']/n
            otther_p = exp_normals[exp]['n']/n
            mu = (summed_p * mixed_normal['mu'] + 
                  otther_p * exp_normals[exp]['mu'])
            sigma = math.sqrt(summed_p * ( (mixed_normal['mu'] - mu)**2 + 
                                           mixed_normal['sigma']**2) + 
                              otther_p * ( (exp_normals[exp]['mu'] - mu)**2 + 
                                           exp_normals[exp]['sigma']**2)
                              )
            mixed_normal = {'mu': mu, 'sigma': sigma, 'n': n}
        return mixed_normal

    @staticmethod
    def tstudent(alpha, df):
        """
        Return T values from a real t-student distribution, not a table of pre-
        calculated values
        """
        T = stats.t.interval(1-alpha, df)[1]
        return T
    
    @staticmethod
    def ttest(mean1, mean2, sd1, sd2, n1, n2):
        """
        This test also known as Welch's t-test is used only when the two population
        variances are assumed to be different (the two sample sizes may or may not
        be equal) and hence must be estimated separately.
        
        - http://en.wikipedia.org/wiki/Student%27s_t-test#Unequal_sample_sizes.2C_unequal_variance
        - Welch, B. L. (1947), "The generalization of "student's" problem when 
          several different population variances are involved.", Biometrika 34: 28-35
        """
        mean_dif_variance = (sd1 ** 2) / n1 + (sd2 ** 2) / n2
        mean_dif_sd = math.sqrt(mean_dif_variance)
        t = (mean1 - mean2) / mean_dif_sd
        return abs(t)
    
    @staticmethod
    def degrees_freedom(mean1, mean2, sd1, sd2, n1, n2):
        """
        This is the Welch-Satterthwaite equation.
        
        - Satterthwaite, F. E. (1946), "An Approximate Distribution of Estimates of 
          Variance Components.", Biometrics Bulletin 2: 110-114, doi:10.2307/3002019
        """
        sd1n1 = (sd1 ** 2) / n1
        sd2n2 = (sd2 ** 2) / n2
        df = ((sd1n1 + sd2n2) ** 2) / ((sd1n1 ** 2) / (n1 - 1) + 
                                       (sd2n2 ** 2) / (n2 - 1))
        return df
    
    @staticmethod
    def list_template(original_list, *places):
        def list_template_func(*iterables):
            positions = places
            len_iterables = len(iterables)
            len_positions = len(positions)
            if len_iterables > len_positions:
                positions += (len(original_list),)*(len_iterables-len_positions)
            elif len_iterables < len_positions:
                iterables += ([],)*(len_positions-len_iterables)
            last_position = 0
            new_list = list()
            for position, iterable in zip(positions, iterables):
                new_list += original_list[last_position:position] + list(iterable)
                last_position = position
            return new_list
        return list_template_func

