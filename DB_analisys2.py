#!/usr/bin/env python
# encoding: utf-8
"""
:synopsis: Calculate different parameters from a LymPHOS DataBase and write them
to a TSV file.

:created:    2013/11/29

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.4
:updated:    2015-08-04
"""

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
from __future__ import division

from collections import defaultdict
from tools_commons import DB_SubSet, PepProtFuncs


#===============================================================================
# Global variables
#===============================================================================
OUT_FILEN = 'DB_analisys.xls'
ASCORE_THRESHOLD = 19


#===============================================================================
# Class definitions
#===============================================================================
class DB_PInfo(DB_SubSet):
    def __init__(self, *args, **kwargs):
        kwargs['phospho'] = True
        super(DB_PInfo, self).__init__(*args, **kwargs)
        
        # Variables for caching purposes:
        self._prot2psites = dict()
        self._prot2hcpsites = dict()
        self._prot2qpsites = dict()
    
    def _calc_time2regulation(self, psite):
        """
        Calculates a regulation index ( -1 (down), 0 (no_change), or 1(up) ) at
        each analyzed time for the given ``psite`` object.
        
        :param pstie: a :class:``PSite`` to calculate the time -> regulation
        information associated to its ``supporting_pep`` in a LymPHOS DataBase.
        
        :returns: a ``{time: regulaton}`` dictionary, where regulation can be 
        -1 (down), 0 (no-change), or 1 (up).
        """
        #Fetch regulation raw data from all the supporting peptides of the psite:
        time2downsnochangesups = defaultdict(lambda: [0,0,0])
        supporting_peps = map(lambda pep: pep.peptide, psite.supporting_peps)
        quantpepexpconds = self.get_quantpepexpconds(pep_names=supporting_peps)
        for supra_exp in quantpepexpconds:
            for time, values in supra_exp.time_values().items():
                for index, spectra in enumerate(values[1]): #X-NOTE: values[1] = [downs, no_changes, ups]
                    time2downsnochangesups[time][index] += spectra
        #Calculate the final regulation index value from previously fetched data:
        time2regulation = dict()
        for time, downsnochangesups in time2downsnochangesups.items():
            up_or_down = downsnochangesups[2] - downsnochangesups[0] #upregulated - downregulated
            if up_or_down < 0:
                regulation = -1 #Downregulation
            elif up_or_down > 0:
                regulation = 1  #Upregulation
            else:
                regulation = 0  #No-Change
            time2regulation[time] = regulation
        return time2regulation
    
    def get_all_Psites(self):
        """
        :returns: dictionary with All phospho-sites for each protein::
            {protein_ac: {position_in_seq: PSite(prot_ac, position, p_aa, max_ascore, supporting_peps), ...} , ...} 
        """
        if not self._prot2psites or self._db_updated:
            prot2psites = dict()
            for prot in self.get_proteins(is_decoy=False):
                prot_psites = PepProtFuncs.get_Psites4prot(prot)
                # Add time -> regulation information for each protein p-site: 
                for psite in prot_psites.values():
                    psite.time2regulation = self._calc_time2regulation(psite)
                prot2psites[prot.ac] = prot_psites
            self._prot2psites = prot2psites
        return self._prot2psites
    
    def _filter_Psites(self, filter_func, prot2psites=None):
        """
        :param filter_func: the function or lambda used to filter. It must
        accept two parameters (a ``psite_pos`` and a ``psite`` object).
        :param prot2psites: `dictionary` with protein associated phospho-sites
        to be filtered. If ``prot2psites`` is not provided, all phospho-sites 
        from current :class:`DB_PInfo` instance will be used.
       
        :returns: dictionary with Filtered phospho-sites for each protein,
        according to ``filter_func``::
            {protein_ac: {position_in_seq: PSite(prot_ac, position, p_aa, 
                                                 max_ascore, supporting_peps), 
                          ...},
            ...} 
        """
        if not prot2psites:
            prot2psites = self.get_all_Psites()
        #
        prot2filteredpsites = dict()
        for prot_ac, psites in prot2psites.items():
            filteredpsites = dict()
            for psite_pos, psite in psites.items():
                if filter_func(psite_pos, psite):
                    filteredpsites[psite_pos] = psite
            if filteredpsites:
                prot2filteredpsites[prot_ac] = filteredpsites
        #
        return prot2filteredpsites
    
    def get_hc_Psites(self):
        """
        :returns: dictionary with High Confidence phospho-sites for each protein::
            {protein_ac: {position_in_seq: PSite(prot_ac, position, p_aa, 
                                                 max_ascore, supporting_peps), 
                          ...},
            ...} 
        """
        if not self._prot2hcpsites or self._db_updated:
            filter_func = lambda psite_pos, psite: psite.max_ascore >= ASCORE_THRESHOLD
            self._prot2hcpsites = self._filter_Psites(filter_func)
        return self._prot2hcpsites
    
    def get_quantitative_Psites(self):
        """
        :returns: dictionary with Quantitative phospho-sites for each protein::
            {protein_ac: {position_in_seq: PSite(prot_ac, position, p_aa, 
                                                 max_ascore, supporting_peps), 
                          ...}, 
            ...} 
        """
        if not self._prot2qpsites or self._db_updated:
            filter_func = lambda psite_pos, psite: filter(lambda pepdata: pepdata.quantitative, psite.supporting_peps)
            self._prot2qpsites = self._filter_Psites(filter_func)
        return self._prot2qpsites
    
    @staticmethod
    def _count_Psites(prot2psites):
        count = 0
        for psites in prot2psites.values():
            count += len(psites)
        return count
    
    @property
    def all_Psites(self):
        return self._count_Psites(self.get_all_Psites())
    
    @property
    def hc_Psites(self):
        return self._count_Psites(self.get_hc_Psites())
    
    @staticmethod
    def _phospho_aa(prot2psites):
        """
        :returns: a dictionary containing the number of pSer, pThr and pTyr
        (both, the total number, and only the High Confidence ones)::
            {aa: [number of times this aa is in a phospho-site,
                  number of times this aa is in a High Confidence phospho-site], 
            ...}
        """
        p_aas = {'S': [0, 0],  # [0] -> Total ; [1] -> High Confidence (ascore>19)
                 'T': [0, 0],  #
                 'Y': [0, 0]}  #
        for psites in prot2psites.values():
            for psite in psites.values():
                if psite.p_aa: #X-NOTE: this only takes into account psites within a protein sequence
                    p_aas[psite.p_aa][0] += 1
                    if psite.max_ascore >= ASCORE_THRESHOLD:
                        p_aas[psite.p_aa][1] +=1
        return p_aas
    
    @property
    def phospho_aa(self):
        """
        :returns: a dictionary containing the number of pSer, pThr and pTyr
        (both, the total number, and only the High Confidence ones)::
            {aa: [number of times this aa is in a phospho-site,
                  number of times this aa is in a High Confidence phospho-site],
            ...}
        """
        return self._phospho_aa(self.get_all_Psites())
    
    @property
    def quant_phospho_aa(self):
        """
        :returns: a dictionary containing the number of pSer, pThr and pTyr
        (both, the total number, and only the High Confidence ones) with some
        quantified peptide::
            {aa: [number of times this aa is in a phospho-site,
                  number of times this aa is in a High Confidence phospho-site],
            ...}
        """
        return self._phospho_aa(self.get_quantitative_Psites())
    
    @property
    def time2regulation2phospho_aas(self):
        """
        :returns: a hierarchical three-level-deep dictionary (time -> regulation
        -> phospho-AAs) containing in the 1st level the experimental time, in
        the 2nd level the regulation type (up, down, or no-change), and in the
        3th level the number of pSer, pThr and pTyr (both, the total number, and
        only the High Confidence ones)::
           {time: {regulation: {aa: [number of times this aa is in a phospho-site,
                                     number of times this aa is in a High Confidence phospho-site],
                                ...}
        """
        #Create the t2reg2p_aas (time -> regulation -> phospho-AAs) data
        #structure:
        t2reg2p_aas  = dict()
        for time in self.get_quant_times():
            t2reg2p_aas[time] = dict()
            for regulation in (-1, 0, 1):
                t2reg2p_aas[time][regulation] = {'S': [0, 0],  # [0] -> Total ; [1] -> High Confidence (ascore>19)
                                                 'T': [0, 0],  #
                                                 'Y': [0, 0]}  #
        #Fill t2reg2p_aas with regulation data from quantitative p-sites:
        for psites in self.get_quantitative_Psites().values():
            for psite in psites.values():
                if psite.p_aa: #X-NOTE: this only takes into account p-sites within a protein sequence
                    for time, regulation in psite.time2regulation.items():
                        t2reg2p_aas[time][regulation][psite.p_aa][0] += 1
                        if psite.max_ascore >= ASCORE_THRESHOLD:
                            t2reg2p_aas[time][regulation][psite.p_aa][1] +=1
        return t2reg2p_aas
    
    def spectra_with_Ppeptides(self):
        """"
        :returns: number of spectra identified as phosphopeptides
        """
        return self.get_data().count()
    
    def quant_spectra_with_Ppeptides(self):
        """"
        :returns: number of Quantified spectra identified as phosphopeptides
        """
        return self.get_data(dataquant__isnull=False).count()
    
    def unique_Ppeptides(self):
        """"
        :returns: number of non-redundant phosphorilated peptides
        """
        return self.get_peptidecache().count()
    
    def quant_unique_Ppeptides(self):
        """"
        :returns: number of non-redundant Quantified phosphorilated peptides
        """
        return self.get_peptidecache(quantitative=True).count()
    
    def Pproteins(self):
        """"
        :returns: number of phospho-proteins
        """
        return self.get_proteins().count()
    
    def quant_Pproteins(self):
        """"
        :returns: number of phospho-proteins with some Quantified peptide
        """
        return self.get_proteins(data__dataquant__isnull=False).count()


#===============================================================================
# Function definitions
#===============================================================================
def main(filename):
    """
    Dirty code for a fast file reporting of phospho-information from a LymPHOS
    DataBase as a TSV
    """
    print('\nWorking... Please wait...\n')
    
    sub_db_info = DB_PInfo()
    
    with open(filename, 'w') as io_file:
        io_file.write('All p-sites:\t{0}\n'.format(sub_db_info.all_Psites))
        io_file.write('High Confidence p-sites:\t{0}\n'.format(sub_db_info.hc_Psites))
        io_file.write('\n')
        
        io_file.write('Total:\n')
        io_file.write('- Spectra matching pospho-peptides:\t{0}\n'.format(sub_db_info.spectra_with_Ppeptides()))
        io_file.write('- Unique phospho-peptides:\t{0}\n'.format(sub_db_info.unique_Ppeptides()))
        io_file.write('- Phospho-proteins:\t{0}\n'.format(sub_db_info.Pproteins()))
        io_file.write('\n')
        
        io_file.write('Quantitative:\n')
        io_file.write('- Spectra matching pospho-peptides:\t{0}\n'.format(sub_db_info.quant_spectra_with_Ppeptides()))
        io_file.write('- Unique phospho-peptides:\t{0}\n'.format(sub_db_info.quant_unique_Ppeptides()))
        io_file.write('- Phospho-proteins:\t{0}\n'.format(sub_db_info.quant_Pproteins()))
        io_file.write('\n')
        
        confidence = {0: 'Total', 1: 'High Confidence'}
        
        io_file.write('Total phospho-amino-acids:\n')
        psites = sub_db_info.phospho_aa
        for i in range(2):
            io_file.write('- {0}:\t{1}\n'.format(confidence[i], psites['S'][i] + psites['T'][i] + psites['Y'][i]))
            io_file.write('S: {0} | T: {1} | Y: {2}\n'.format(psites['S'][i], psites['T'][i], psites['Y'][i]))
        io_file.write('\n')
        
        io_file.write('Total quantitative phospho-amino-acids:\n')
        psites = sub_db_info.quant_phospho_aa
        for i in range(2):
            io_file.write('- {0}:\t{1}\n'.format(confidence[i], psites['S'][i] + psites['T'][i] + psites['Y'][i]))
            io_file.write('S: {0} | T: {1} | Y: {2}\n'.format(psites['S'][i], psites['T'][i], psites['Y'][i]))
        io_file.write('\n')
        
        regulations = {1: 'Upregulated', -1: 'Downregulated', 0:'No-Change'}
        
        psites = sub_db_info.time2regulation2phospho_aas
        for time, reg2p_aas in sorted(psites.items()):
            io_file.write('\nTime (min.):\t{0}\n'.format(time))
            out_lines = ['\t' + '\t'.join([regulations[reg_id] for reg_id in (1, -1, 0)])]
            for i in range(2):
                out_line = ['- ' + confidence[i]]
                linesum = 0
                for regulation in (1, -1, 0): #Up/Down/NotChanging
                    p_aas = reg2p_aas[regulation]
                    out_line.append('S: {0} | T: {1} | Y: {2}'.format(p_aas['S'][i], p_aas['T'][i], p_aas['Y'][i]))
                    linesum += p_aas['S'][i] + p_aas['T'][i] + p_aas['Y'][i]
                out_line.append('Total:\t{0}'.format(linesum))
                out_lines.append('\t'.join(out_line))    
            io_file.write('\n'.join(out_lines))
            io_file.write('\n')
    
    print( '\nWork finished!\nSee file: {0}\n'.format(filename) )


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    main(OUT_FILEN)

