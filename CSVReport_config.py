#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
:synopsis: `CSVReport_config.py` defines the configuration variables needed by
``CSVReport_exporter_class.py`` and ``exportLymPHOSDB2CSVReport.py`` files.

:created:    2013/10/08

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '1.7'
__UPDATED__ = '2017-07-05'


#===============================================================================
# Global variables
#===============================================================================

# INPUTs:
EXPERIMENTS = [] #List with tuples of experiment IDs to process in packs (Ex. [(209, 210), (209,), (210,)] ). Empty list or None for all.
QUANTITATIVE = None #True: Only quantitative data, False: only non-quantitative data, None: both
PHOSPHO = True #True: Only phosphorylated data, False: only non-phosphorylated data, None: both

# OUTPUTs:
SEPARATOR = '\t'
OUT_FILENAME = 'LymPHOS2_CSV_Export_{experiments}.xls'
EXTRA_OUT_FILENAME = 'LymPHOS2_Export_{experiments}_{0}.txt'
GRAPH_OUT_FILENAME = '{title}_{timestamp}.png'

# DATA PROCESSING:
# Q-Ascore confidence threshold:
QASCORE_THRESHOLD = 19

#------------------------------------------------------------------------------
# LymPHOS2 JM experiment statistics for no-phospho-peptides from PQuantifier
# log-file for Jurkat experiments 2013-11-08:
#------------------------------------------------------------------------------
#REAL_NOP_STATS = {201: {'mu':  0.0681, 'sigma':  0.2161, 'n':  838-40 },
#                  202: {'mu':  0.5290, 'sigma': -0.4769, 'n': 1831-90 },
#                  203: {'mu':  0.3439, 'sigma':  0.4605, 'n': 1414-70 },
#                  204: {'mu':  0.0557, 'sigma':  0.1860, 'n': 1175-104},
#                  205: {'mu': -0.0009, 'sigma':  0.2544, 'n': 1954-96 },
#                  206: {'mu': -0.0500, 'sigma':  0.1866, 'n': 1772-56 },
#                  }
#NOP_STATS = {201: {'mu': 0.0, 'sigma': 0.2161, 'n':  838-40 },
#             202: {'mu': 0.0, 'sigma': 0.4769, 'n': 1831-90 },
#             203: {'mu': 0.0, 'sigma': 0.4605, 'n': 1414-70 },
#             204: {'mu': 0.0, 'sigma': 0.1860, 'n': 1175-104},
#             205: {'mu': 0.0, 'sigma': 0.2544, 'n': 1954-96 },
#             206: {'mu': 0.0, 'sigma': 0.1866, 'n': 1772-56 },
#             }
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LymPHOS2 JM experiment statistics for no-phospho-peptides from PQuantifier
# log-file for Jurkat experiments 2013-12-09:
#------------------------------------------------------------------------------
#REAL_NOP_STATS = {201: {'mu':  0.0421, 'sigma': -0.2206, 'n': 3176-158},
#                  202: {'mu':  0.4430, 'sigma': -0.5048, 'n': 5447-272},
#                  203: {'mu':  0.2472, 'sigma': -0.4716, 'n': 3899-194},
#                  205: {'mu':  0.0039, 'sigma':  0.2155, 'n': 3353-166},
#                  206: {'mu': -0.0478, 'sigma':  0.2200, 'n': 3629-180},
#                  208: {'mu': -0.0574, 'sigma':  0.2121, 'n': 4335-216},
#                  209: {'mu': -0.1833, 'sigma': -0.3169, 'n': 2841-142},
#                  210: {'mu': -0.0209, 'sigma': -0.1989, 'n': 3568-178},
#                  }
# NOP_STATS = {201: {'mu': 0.0, 'sigma': 0.2206, 'n': 3176-158},
#              202: {'mu': 0.0, 'sigma': 0.5048, 'n': 5447-272},
#              203: {'mu': 0.0, 'sigma': 0.4716, 'n': 3899-194},
#              205: {'mu': 0.0, 'sigma': 0.2155, 'n': 3353-166},
#              206: {'mu': 0.0, 'sigma': 0.2200, 'n': 3629-180},
#              208: {'mu': 0.0, 'sigma': 0.2121, 'n': 4335-216},
#              209: {'mu': 0.0, 'sigma': 0.3169, 'n': 2841-142},
#              210: {'mu': 0.0, 'sigma': 0.1989, 'n': 3568-178},
#              }
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LymPHOS2 experiment statistics for no-phospho-peptides from PQuantifier
# log-file with TMT cut-off at 250:
#------------------------------------------------------------------------------
#REAL_NOP_STATS = {  1: {'mu':  0.0242, 'sigma':  0.4109, 'n':  607-6 },
#                    2: {'mu':  0.0215, 'sigma':  0.3453, 'n':  567-4 },
#                    5: {'mu':  1.4039, 'sigma':  0.4002, 'n':  178-2 },
#                    6: {'mu':  0.2732, 'sigma':  0.1668, 'n': 1656-32},
#                    7: {'mu':  0.2914, 'sigma': -0.1483, 'n': 3215-22},
#                   29: {'mu':  0.2047, 'sigma': -0.1468, 'n': 3327-66},
#                   30: {'mu':  0.2884, 'sigma':  0.3139, 'n': 2879-56},
#                   35: {'mu': -0.4271, 'sigma': -0.2647, 'n': 1838-36},
#                  }
#NOP_STATS = {  1: {'mu': 0.0, 'sigma':  0.4109, 'n':  607-6 },
#               2: {'mu': 0.0, 'sigma':  0.3453, 'n':  567-4 },
#               5: {'mu': 0.0, 'sigma':  0.4002, 'n':  178-2 },
#               6: {'mu': 0.0, 'sigma':  0.1668, 'n': 1656-32},
#               7: {'mu': 0.0, 'sigma': -0.1483, 'n': 3215-22},
#              29: {'mu': 0.0, 'sigma': -0.1468, 'n': 3327-66},
#              30: {'mu': 0.0, 'sigma':  0.3139, 'n': 2879-56},
#              35: {'mu': 0.0, 'sigma': -0.2647, 'n': 1838-36},
#             }
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LymPHOS2 experiment statistics for no-phospho-peptides from PQuantifier
# log-file with TMT cut-off at 600:
#------------------------------------------------------------------------------
#REAL_NOP_STATS = {  1: {'mu':  0.0271, 'sigma':  0.4208, 'n':  592-4 },
#                    2: {'mu':  0.0186, 'sigma':  0.3389, 'n':  541-4 },
#                    5: {'mu':  1.9481, 'sigma':  0.7809, 'n':  173-2 },
#                    6: {'mu':  0.3035, 'sigma':  0.1625, 'n': 1655-32},
#                    7: {'mu':  0.2015, 'sigma': -0.2597, 'n': 3212-64},
#                   29: {'mu':  0.2047, 'sigma': -0.1468, 'n': 3294-64},
#                   30: {'mu':  0.1981, 'sigma': -0.1501, 'n': 2851-56},
#                   35: {'mu': -0.4307, 'sigma':  0.3322, 'n': 1824-36},
#                  }
#NOP_STATS = {  1: {'mu': 0.0, 'sigma':  0.4208, 'n':  592-4 },
#               2: {'mu': 0.0, 'sigma':  0.3389, 'n':  541-4 },
#               5: {'mu': 0.0, 'sigma':  0.7809, 'n':  173-2 },
#               6: {'mu': 0.0, 'sigma':  0.1625, 'n': 1655-32},
#               7: {'mu': 0.0, 'sigma': -0.2597, 'n': 3212-64},
#              29: {'mu': 0.0, 'sigma': -0.1468, 'n': 3294-64},
#              30: {'mu': 0.0, 'sigma': -0.1501, 'n': 2851-56},
#              35: {'mu': 0.0, 'sigma':  0.3322, 'n': 1824-36},
#             }
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LymPHOS2 new format experiment statistics for no-phospho-peptides from
# PQuantifier log-file with TMT cut-off as mean_0.1%, no experiment 5, and
# experiment 36:
#------------------------------------------------------------------------------
#REAL_NOP_STATS = { 1: {  1: {'mu':  0.0459, 'sigma':  0.5008, 'n':  582-4 },
#                         2: {'mu':  0.0222, 'sigma':  0.3458, 'n':  540-4 },
#                   4: {  6: {'mu':  0.3035, 'sigma':  0.1607, 'n': 1641-32}, },
#                         7: {'mu':  0.2011, 'sigma': -0.2589, 'n': 3192-62},
#                        29: {'mu':  0.2048, 'sigma':  0.1455, 'n': 3268-64},
#                        30: {'mu':  0.1994, 'sigma': -0.1497, 'n': 2830-56},
#                        36: {'mu':  0.2194, 'sigma':  0.1745, 'n': 1434-28}, },
#                   9: { 35: {'mu': -0.4294, 'sigma':  0.2586, 'n': 1777-34}, },
#                  }
NOP_STATS = { 1: {  1: {'mu': 0.0, 'sigma':  0.5008, 'n':  582-4 },
                    2: {'mu': 0.0, 'sigma':  0.3458, 'n':  540-4 }, },
              4: {  6: {'mu': 0.0, 'sigma':  0.1607, 'n': 1641-32},
                    7: {'mu': 0.0, 'sigma': -0.2589, 'n': 3192-62},
                   29: {'mu': 0.0, 'sigma':  0.1455, 'n': 3268-64},
                   30: {'mu': 0.0, 'sigma': -0.1497, 'n': 2830-56},
                   36: {'mu': 0.0, 'sigma':  0.1745, 'n': 1434-28}, },
              9: { 35: {'mu': 0.0, 'sigma':  0.2586, 'n': 1777-34}, },
             }
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LymPHOS2 supra-experiment statistics for no-phospho-peptides from PQuantifier
# log-file for experiment 35:
#------------------------------------------------------------------------------
#REAL_NOP_STATS = {9: {'mu': -0.4271, 'sigma': -0.2647, 'n': 1838-36}}
#
#NOP_STATS = {9: {'mu': 0.0, 'sigma': -0.2647, 'n': 1838-36}}
#------------------------------------------------------------------------------
