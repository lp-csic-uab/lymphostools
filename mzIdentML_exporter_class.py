#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
'''
Created on 2013/09/19

@authors: Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
@version: 0.8
'''

#===============================================================================
# Imports
#===============================================================================
from __future__ import division

from tools_commons import PepProtFuncs
from datetime import datetime


#===============================================================================
# Global variables
#===============================================================================
from mzIdentML_globals import DB2CV_ENGINES, CV_INTEGRATOR


#===============================================================================
# Class definitions
#===============================================================================
class MZIDGenerator(object):
    '''
    Class to generate mzIdentML files
    '''
    HEAD_AND_CVLIST = """<?xml version="1.0" encoding="UTF-8"?>
<MzIdentML xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://psidev.info/psi/pi/mzIdentML/1.1 http://www.psidev.info/files/mzIdentML1.1.0.xsd"
    xmlns="http://psidev.info/psi/pi/mzIdentML/1.1" id="LymPHOS_Export_mzIdentML" version="1.1.0"
    creationDate="{0}">
    
    <cvList>
        <cv id="PSI-MS" fullName="PSI-MS" version="2.25.0" 
            uri="http://psidev.cvs.sourceforge.net/viewvc/*checkout*/psidev/psi/psi-ms/mzML/controlledVocabulary/psi-ms.obo"/>
        <cv id="UNIMOD" fullName="UNIMOD" uri="http://www.unimod.org/obo/unimod.obo"/>
        <cv id="UO" fullName="UNIT-ONTOLOGY"
            uri="http://obo.cvs.sourceforge.net/*checkout*/obo/obo/ontology/phenotype/unit.obo"/>
    </cvList>
    """
    
    PROVIDER = """
    <Provider id="PROVIDER">
        <ContactRole contact_ref="PERSON_DOC_OWNER">
            <Role>
                <cvParam accession="{role_accession}" cvRef="PSI-MS" name="{role_name}"/>
            </Role>
        </ContactRole>
    </Provider>
    <AuditCollection>
        <Person firstName="{first_name}" lastName="{last_name}" id="PERSON_DOC_OWNER">
            <cvParam cvRef="PSI-MS" accession="MS:1000589" name="contact email"
                value="{person_email}"/>                
            <Affiliation organization_ref="LP-CSIC/UAB"/>
        </Person>
        <Organization name="{organization}" id="LP-CSIC/UAB">
            <cvParam cvRef="PSI-MS" accession="MS:1000589" name="contact email" 
                value="{organization_email}"/>
        </Organization>
    </AuditCollection>
    """
    
    # AnalysisSoftwareList subsections XML Chunk:
    ASL_AS_INTEGRATOR = """
        <AnalysisSoftware version="{software_version}" name="{software_name}" id="AS_{engine}">
            <ContactRole contact_ref="LP-CSIC/UAB">
                <Role>
                    <cvParam cvRef="PSI-MS" accession="MS:1001267" name="software vendor"/>
                </Role>
            </ContactRole>
            <SoftwareName>
                <userParam name="{software_name}" 
                    unitAccession="{unit_accession}" unitCvRef="PSI-MS" unitName="{unit_name}"/>
            </SoftwareName>
        </AnalysisSoftware>"""
    ASL_AS = """
        <AnalysisSoftware version="{software_version}" name="{software_name}" id="AS_{engine}">
            <SoftwareName>
                <cvParam accession="{software_accession}" cvRef="PSI-MS" name="{software_name}"/>
            </SoftwareName>
        </AnalysisSoftware>"""

    # SequenceCollection subsections XML Chunks:
    SC_DBS = """
        <DBSequence id="{ac}" accession="{ac}" searchDatabase_ref="{fasta}" length="{length}">
            <Seq>{seq}</Seq>
            <cvParam accession="MS:1001088" cvRef="PSI-MS" name="protein description"
                value="{description}"/>
        </DBSequence>"""
    
    SC_P_MOD = """
            <Modification monoisotopicMassDelta="{delta_mass}" location="{pos}">
                <cvParam accession="UNIMOD:{unimod_id}" cvRef="UNIMOD" name="{unimod_name}"/>
            </Modification>"""
    SC_P = """
        <Peptide id="{pid}">
            <PeptideSequence>{peptide}</PeptideSequence>{modifications}                
        </Peptide>"""
    
    SC_PE = """
        <PeptideEvidence isDecoy="{decoy}" post="{post}" pre="{pre}" end="{end}" start="{start}"
            peptide_ref="{pid}" dBSequence_ref="{ac}"
            id="PE_{pid}_{ac}_{match_id}"/>"""
    
    # AnalysisCollection subsections XML Chunks:
    AC_SIL_REFS = """
        <SpectrumIdentification spectrumIdentificationList_ref="SIL_{engine}"
            spectrumIdentificationProtocol_ref="SIP_{engine}" id="SI_{engine}">
            <InputSpectra spectraData_ref="SD_{raw_file}"/>
            <SearchDatabaseRef searchDatabase_ref="{fasta}"/>
        </SpectrumIdentification>"""
    
    AC_PDL_REFS_ISI = """
                <InputSpectrumIdentifications spectrumIdentificationList_ref="SIL_{engine}"/>"""
    AC_PDL_REFS = """
        <ProteinDetection proteinDetectionList_ref="Integrator_results" proteinDetectionProtocol_ref="PDP_Integrator"
            id="PD_Integrator">{inputspectrumidentifications}
        </ProteinDetection> """

    # AnalysisProtocolCollection subsections XML Chunks:
    APC_SIP_MOD = """
                <SearchModification residues="{residues}" massDelta="{delta_mass}" fixedMod="{fixed}">
                    <cvParam accession="UNIMOD:{unimod_id}" cvRef="UNIMOD" name="{unimod_name}"/>
                </SearchModification>"""
    APC_SIP = """
        <SpectrumIdentificationProtocol analysisSoftware_ref="AS_{engine}" id="SIP_{engine}">
            <SearchType>
                <cvParam accession="MS:1001083" cvRef="PSI-MS" name="ms-ms search"/>
            </SearchType>
            <AdditionalSearchParams>
                <cvParam accession="MS:1001211" cvRef="PSI-MS" name="parent mass type mono"/>
                <cvParam accession="MS:1001256" cvRef="PSI-MS" name="fragment mass type mono"/>
            </AdditionalSearchParams>
            <ModificationParams>{searchmodifications}
            </ModificationParams>
            <Enzymes independent="false">
                <Enzyme missedCleavages="{missed_cleavages}" semiSpecific="false" cTermGain="OH" nTermGain="H"
                    id="{engine}_{enzyme}">
                    <EnzymeName>
                        <cvParam accession="{enzyme_acc}" cvRef="PSI-MS" name="{enzyme}"/>
                    </EnzymeName>
                </Enzyme>
            </Enzymes>
            <FragmentTolerance>
                <cvParam accession="MS:1001412" cvRef="PSI-MS" unitCvRef="UO" unitName="dalton"
                    unitAccession="UO:0000221" value="{fragment_tolerance}" name="search tolerance plus value"/>
                <cvParam accession="MS:1001413" cvRef="PSI-MS" unitCvRef="UO" unitName="dalton"
                    unitAccession="UO:0000221" value="{fragment_tolerance}" name="search tolerance minus value"/>
            </FragmentTolerance>
            <ParentTolerance>
                <cvParam accession="MS:1001412" cvRef="PSI-MS" unitCvRef="UO" unitName="dalton"
                    unitAccession="UO:0000221" value="{parent_tolerance}" name="search tolerance plus value"/>
                <cvParam accession="MS:1001413" cvRef="PSI-MS" unitCvRef="UO" unitName="dalton"
                    unitAccession="UO:0000221" value="{parent_tolerance}" name="search tolerance minus value"/>
            </ParentTolerance>
            <Threshold>
                <cvParam accession="MS:1001494" cvRef="PSI-MS" name="no threshold"/>
            </Threshold>
        </SpectrumIdentificationProtocol>"""
    
    APC_PDP = """
        <ProteinDetectionProtocol analysisSoftware_ref="AS_Integrator" id="PDP_Integrator">
            <AnalysisParams>
                <userParam name="Protein inference method" value="Protein found by two or more search engines"/>
            </AnalysisParams>
            <Threshold>
                <cvParam accession="MS:1001494" cvRef="PSI-MS" name="no threshold"/>
            </Threshold>
        </ProteinDetectionProtocol>"""

    # Inputs subsections XML Chunks:
    I_SF = """    
            <SourceFile location="mysql://lymphos.org/lymphos_lymphos2allpepd" id="SF_LymPHOS">
                <FileFormat>
                    <cvParam accession="MS:1001107" cvRef="PSI-MS" name="data stored in database"/>
                </FileFormat>
            </SourceFile>"""
    I_SDB = """
            <SearchDatabase numDatabaseSequences="{fasta_seqs}"
                location="{location}"
                id="{fasta}">
                <FileFormat>
                    <cvParam accession="MS:1001348" cvRef="PSI-MS" name="FASTA format"/>
                </FileFormat>
                <DatabaseName>
                    <cvParam cvRef="PSI-MS" accession="MS:1001104" 
                        name="database UniProtKB/Swiss-Prot"/>
                </DatabaseName>
                <cvParam accession="MS:1001197" name="DB composition target+decoy" cvRef="PSI-MS"/>
            </SearchDatabase>"""
    I_SD = """
            <SpectraData location="{location}" 
                id="SD_{raw_file}">
                <FileFormat>
                    <cvParam accession="MS:1001062" cvRef="PSI-MS" name="Mascot MGF file"/>
                </FileFormat>
                <SpectrumIDFormat>
                    <cvParam accession="MS:1000774" cvRef="PSI-MS"
                        name="multiple peak list nativeID format"/>
                </SpectrumIDFormat>
            </SpectraData>"""
    
    # AnalysisData subsections XML Chunks:    
    AD_SIR_PER = """
                        <PeptideEvidenceRef peptideEvidence_ref="PE_{pid}_{ac}_{match_id}"/>"""
    AD_SIR_PARAM = """
                        <cvParam accession="{accession}" cvRef="PSI-MS" value="{value}" name="{name}"/>"""
    AD_SIR = """
                <SpectrumIdentificationResult id="SIR_{engine}_{spectrum}" spectrumID="index={spectrum}" spectraData_ref="SD_{raw_file}">
                    <SpectrumIdentificationItem passThreshold="true" rank="1"
                        peptide_ref="{pid}" calculatedMassToCharge="{calc_mz}"
                        experimentalMassToCharge="{exp_mz}" chargeState="{charge}"
                        id="SII_{engine}_{spectrum}_{pid}">{peptideevidencerefs}{params}
                    </SpectrumIdentificationItem>
                </SpectrumIdentificationResult>"""
    
    AD_PDH_SIIR = """
                            <SpectrumIdentificationItemRef spectrumIdentificationItem_ref="SII_{engine}_{spectrum}_{pid}"/>"""
    AD_PDH_PER = """
                        <PeptideHypothesis peptideEvidence_ref="PE_{pid}_{ac}_{match_id}">{spectrumidentificationitemrefs}
                        </PeptideHypothesis>"""
    AD_PDH = """
                    <ProteinDetectionHypothesis id="PDH_{pdh_id}" passThreshold="true" dBSequence_ref="{ac}">{peptideevidencerefs}
                        <cvParam cvRef="PSI-MS" accession="MS:1001093" name="sequence coverage" value="{coverage}"/>
                    </ProteinDetectionHypothesis>"""
                        
    def __init__(self, filen, data_records=None, proteins=None, 
                 spectra_info=None, engines=None, sub_db=None, 
                 fasta_info=None):
        self._iofile = None
        self._proteins = None
        self._data_records = None
        self._fasta_filen = self._fasta_fullfilen = ''
        self.proteins_by_ac = dict()
        self.peptides = dict()
        self.peptides_by_ac = dict()
        
        self.filen = filen
        self.spectra_info = spectra_info
        self.sub_db = sub_db
        if not data_records and sub_db:
            data_records = sub_db.get_data()
        if not proteins and sub_db:
            proteins = sub_db.get_proteins(seq__gt='') #X-NOTE: We cannot process deleted proteins -> Errors in MZID
        if not engines and sub_db:
            engines = sub_db.get_search_engines()
        self.data_records = data_records
        self.proteins = proteins
        self.engines = engines
        self.fasta_info = fasta_info
    
    @property
    def proteins(self):
        return self._proteins
    @proteins.setter
    def proteins(self, proteins):
        self._proteins = proteins
        if proteins:
            self.proteins_by_ac = PepProtFuncs.proteins_by_ac(proteins)
        else:
            self.proteins_by_ac = dict()
    
    @property
    def data_records(self):
        return self._data_records
    @data_records.setter
    def data_records(self, data_records):
        self._data_records = data_records
        if data_records:
            self.peptides = PepProtFuncs.peptides_by_pep_orig(data_records)
            self.peptides_by_ac = PepProtFuncs.peptides_by_ac(self.peptides)
        else:
            self.peptides = dict()
            self.peptides_by_ac = dict()
    
    def open(self):
        self._iofile = open(self.filen, 'wb')
        return self._iofile
    
    def write_head(self, *args, **kwargs):
        self._iofile.write(self.HEAD_AND_CVLIST.format(datetime.now().isoformat()))
        return self._iofile

    def analysissoftwarelist_xml(self, *args, **kwargs):
        # Write data to mzid file:
        self._iofile.write('\n    <AnalysisSoftwareList>')
        analysissoftware = self.engines.copy()
        for engine, cv_engine in analysissoftware.items():
            self._iofile.write(self.ASL_AS.format(engine=engine, **cv_engine))
        self._iofile.write(self.ASL_AS_INTEGRATOR.format(**CV_INTEGRATOR))
        self._iofile.write('\n    </AnalysisSoftwareList>\n')
        #
        return self._iofile
    
    def provider_xml(self, provider, *args, **kwargs):
        # Write Provider and AuditCollection XML sections to mzid file:
        self._iofile.write(self.PROVIDER.format(**provider))
        #
        return self._iofile
        
    def _sc_dbsequences_xml(self):
        '''
        Generator method of DBSequence XML sections from self.proteins query
        data
        '''
        for protein in self.proteins:
            prot_data = {'ac': protein.ac,            #Dict to further format DBSequence XML section
                         'fasta': self.fasta_info.name,   #
                         'length': len(protein.seq),  #
                         'seq': protein.seq,          #
                         'description': protein.name, #TODO: get header from FASTA_FILEN
                         }
            # Format the final DBSequence XML section:
            protein_xml = self.SC_DBS.format(**prot_data)
            yield protein_xml
    
    def _sc_peptides_xml(self):
        '''
        Generator method of Peptide XML sections from self.peptides grouped
        query data
        '''
        for pid, pep_data in self.peptides.items():
            modifications = list()
            for mod in PepProtFuncs.find_mods(pid):
                modification = self.SC_P_MOD.format(**mod)
                modifications.append(modification)
            pep_data['modifications'] = ''.join(modifications)
            # Format the final Peptide XML section:
            peptide_xml = self.SC_P.format(**pep_data)
            yield peptide_xml
            
    def _sc_peptideevidences_xml(self):
        '''
        Generator method of PeptideEvidence XML sections from self.peptides
        grouped query data, and self.proteins_by_ac ordered query data
        '''
        for pid, peptide in self.peptides.items():
            pep_prots = [self.proteins_by_ac[ac] for ac in peptide['l_prots_w_decoys'] 
                         if ac in self.proteins_by_ac]
            prots_w_peppos = PepProtFuncs.re_search_pep_in_prots(peptide['peptide'], 
                                                                 pep_prots)
            for prot, pep_poss in prots_w_peppos:
                pe_data = {'pid': pid,                    #Dict to further format PeptideEvidence XML section
                           'ac': prot.ac,                 #
                           'decoy': 'true' if 'decoy' in prot.ac else 'false' #
                           }                              #
                for match_id, pep_pos in enumerate(pep_poss):
                    start, end = pep_pos
                    if start-2 >= 0:
                        pre = prot.seq[start-2]
                    else:
                        pre = '-'
                    if end < len(prot.seq):
                        post = prot.seq[end]
                    else:
                        post = '-'
                    pe_data.update({'match_id': match_id,
                                    'start': start,
                                    'end': end,
                                    'pre': pre,
                                    'post': post,
                                    })
                    # Format the final PeptideEvidence XML section:
                    peptideevidence_xml = self.SC_PE.format(**pe_data)
                    yield peptideevidence_xml
    
    def sequencecollection_xml(self, peptides=None, proteins=None, 
                               *args, **kwargs):
        '''
        Write SequenceCollection XML section to mzIdentML file, collecting XML
        code for DBSequence, Peptide and PeptideEvidence sub-sections from their
        corresponding methods
        '''
        if peptides:
            self.peptides = peptides
        if proteins:
            self.proteins = proteins
        # Write opening of SequenceCollection section to mzid file:
        self._iofile.write('\n    <SequenceCollection>\n')
        # Write DBSequence XML sub-sections:
        for protein_xml in self._sc_dbsequences_xml():
            self._iofile.write(protein_xml)
        # Write Peptide XML sub-sections:
        for peptide_xml in self._sc_peptides_xml():
            self._iofile.write(peptide_xml)
        # Write PeptideEvidence XML sub-sections:
        for pepevidence_xml in self._sc_peptideevidences_xml():
            self._iofile.write(pepevidence_xml)            
        # Write ending of SequenceCollection section:
        self._iofile.write('\n    </SequenceCollection>\n')
        #
        return self._iofile
    
    def analysiscollection_xml(self, *args, **kwargs):
        '''
        Write AnalysisCollection XML section to mzIdentML file, collecting XML
        code for SpectrumIdentification and ProteinDetection sub-sections
        '''
        # Write opening of AnalysisCollection section to mzid file:
        self._iofile.write('\n    <AnalysisCollection>\n')
        pdl_refs_isis = list()
        for engine in self.engines:
            # Format and write SpectrumIdentification XML sub-section:
            sil_refs_xml = self.AC_SIL_REFS.format(engine=engine, 
                                                   raw_file=
                                                   self.spectra_info.name,
                                                   fasta=self.fasta_info.name)
            self._iofile.write(sil_refs_xml)
            # Format and collect InputSpectrumIdentifications XML sub-section of
            # ProteinDetection sub-section:
            pdl_refs_isis.append(self.AC_PDL_REFS_ISI.format(engine=engine))
        # Format and write ProteinDetection XML sub-section:
        pdl_refs_isi_xml = ''.join(pdl_refs_isis)
        pdl_refs_xml = self.AC_PDL_REFS.format(inputspectrumidentifications=
                                               pdl_refs_isi_xml)
        self._iofile.write(pdl_refs_xml)
        # Write ending of AnalysisCollection section:
        self._iofile.write('\n\n    </AnalysisCollection>\n')
        #
        return self._iofile
    
    def analysisprotocolcollection_xml(self, searchprotocols=None, 
                                       *args, **kwargs):
        '''
        Write AnalysisProtocolCollection XML section to mzIdentML file,
        collecting XML code for SpectrumIdentificationProtocol and
        ProteinDetectionProtocol sub-sections from their corresponding methods
        '''
        # Write opening of AnalysisProtocolCollection section to mzid file:
        self._iofile.write('\n    <AnalysisProtocolCollection>\n')
        # Format and write SpectrumIdentificationProtocol XML sub-sections:
        for engine in self.engines:
            searchprotocol = searchprotocols['ENG_COMMON'].copy()      #searchprotocol data is a combination of 
            searchprotocol.update( searchprotocols.get(engine, dict()) ) #comon and specific search egine data
            searchprotocol['engine'] = engine
            apc_sip_mods = list()
            for unimod_id in searchprotocol['search_mods']:
                apc_sip_mod = self.APC_SIP_MOD.format(**PepProtFuncs.UNIMOD[unimod_id])
                apc_sip_mods.append(apc_sip_mod)
            searchprotocol['searchmodifications'] = ''.join(apc_sip_mods)
            siprotocol_xml = self.APC_SIP.format(**searchprotocol)
            self._iofile.write(siprotocol_xml)
        # Write ProteinDetectionProtocol XML sub-section:
        self._iofile.write(self.APC_PDP)
        # Write ending of SequenceCollection section:
        self._iofile.write('\n\n    </AnalysisProtocolCollection>\n')
        #
        return self._iofile
    
    def inputs_xml(self, *args, **kwargs):
        # Write opening of Inputs sub-section to mzid file:
        self._iofile.write('\n        <Inputs>\n')
        self._iofile.write(self.I_SF)
        sourcedatabase_xml = self.I_SDB.format(fasta_seqs=self.fasta_info.num_seqs, 
                                               location=self.fasta_info.fullname, 
                                               fasta=self.fasta_info.name)
        self._iofile.write(sourcedatabase_xml)
        spectradata_xml = self.I_SD.format(location=self.spectra_info.fullname, 
                                           raw_file=self.spectra_info.name)
        self._iofile.write(spectradata_xml)        
        # Write ending of Inputs sub-section:
        self._iofile.write('\n\n        </Inputs>\n')
        #
        return self._iofile
    
    @staticmethod
    def _get_scans_string(data_record):
        if data_record.scan_i == data_record.scan_f:
            scans = str(data_record.scan_i)
        else:
            scans = '{0}-{1}'.format(data_record.scan_i, 
                                     data_record.scan_f)
        return scans
        
    def _ad_spectrumresults_xml(self, engine):
        '''
        Generator method of SpectrumIdentificationItem XML sections from
        self.peptides grouped query data, and self.proteins_by_ac ordered query
        data, for a selected search engine
        '''
        db2cv_engine = self.engines[engine]
        spectrum_data = {'engine': engine} #Dict to further format SpectrumIdentificationItem XML section
        # Iterates over spectrum_data grouped by peptide:
        for pid, peptide in self.peptides.items():
            spectrum_data['pid'] = pid
            # Collect common PeptideEvidence_Ref XML sections into
            # spectrum_data['peptideevidencerefs']:
            pe_refs = list()
            pep_prots = [self.proteins_by_ac[ac] for ac in peptide['l_prots_w_decoys'] 
                         if ac in self.proteins_by_ac]
            prots_w_peppos = PepProtFuncs.re_search_pep_in_prots(peptide['peptide'], 
                                                                 pep_prots)
            for prot, pep_poss in prots_w_peppos:
                for match_id in range(len(pep_poss)):
                    # Format the PeptideEvidenceRef XML sub-section:
                    pe_ref = self.AD_SIR_PER.format(pid=pid, 
                                                    ac=prot.ac, 
                                                    match_id=match_id)
                    pe_refs.append(pe_ref)
            spectrum_data['peptideevidencerefs'] = ''.join(pe_refs)

            # Iterates over spectrum_data:
            for data_record in peptide['data_records']:
                # Get the cvParams XML section into spectrum_data['params']:
                prob_value = getattr(data_record, db2cv_engine['field'], None)
                if prob_value:
                    params_data = {'value': prob_value,
                                   'accession': db2cv_engine['accession'],
                                   'name': db2cv_engine['name'],
                                   }
                    # Format the cvParam XML sub-section:
                    spectrum_data['params'] = self.AD_SIR_PARAM.format(**params_data)
                    #
                    spectrum_data['raw_file'] = self.spectra_info.name
                    scans = self._get_scans_string(data_record)
                    spectrum_data['spectrum'] = self.spectra_info.scans2index[scans]
                    spectrum_data['calc_mz'] = peptide['calc_mass'] / data_record.charge
                    spectrum_data['exp_mz'] = data_record.mass / data_record.charge
                    spectrum_data['charge'] = data_record.charge
                    # Format the final SpectrumIdentificationItem XML section:
                    spectrumresult_xml = self.AD_SIR.format(**spectrum_data)
                    yield spectrumresult_xml
    
    def _ad_protdecthyp_siir_xml(self, peptide):
        '''
        Returns peptide-common SpectrumIdentificationItemRef (SIIR) XML sub-
        sections for provided peptide
        '''
        siirs_data = {'pid': peptide['pid'],}
        sii_refs = list()
        for data_record in peptide['data_records']:
            scans = self._get_scans_string(data_record)
            siirs_data['spectrum'] = self.spectra_info.scans2index[scans]
            #
            data_record_engines = list()
            for engine, db2cv_engine in DB2CV_ENGINES.items():
                if getattr(data_record, db2cv_engine['field'], None):
                    data_record_engines.append(engine)
            for engine in data_record_engines:
                siirs_data['engine'] = engine
                sii_ref = self.AD_PDH_SIIR.format(**siirs_data)
                sii_refs.append(sii_ref)
        return ''.join(sii_refs)
        
    def _ad_protdecthyps_xml(self):
        '''
        Generator method of ProteinDetectionHypothesis XML sections from
        self.proteins query data
        '''
        for pdh_id, protein in enumerate(self.proteins, start=1):
            pdh_data = {'ac': protein.ac,  #Dict to further format ProteinDetectionHypothesis XML section and sub-sections
                        'pdh_id': pdh_id,} #
            #
            # Collect PeptideEvidence_Ref XML sections from all peptides
            # matching this protein:
            pe_refs = list()
            covered_aa_pos = set() #For the coverage calculations
            for pid, peptide in self.peptides_by_ac[protein.ac].items():
                pdh_data['pid'] = pid
                pdh_data['spectrumidentificationitemrefs'] = self._ad_protdecthyp_siir_xml(peptide)
                # Put PeptideEvidence_Ref XML sub-sections for the current
                # peptide into pe_refs list:
                pep_prots = [protein]
                prots_w_peppos = PepProtFuncs.re_search_pep_in_prots(peptide['peptide'], 
                                                                     pep_prots)
                for _, pep_poss in prots_w_peppos:
                    for match_id, pep_pos in enumerate(pep_poss):
                        pdh_data['match_id'] = match_id
                        pe_ref = self.AD_PDH_PER.format(**pdh_data)
                        pe_refs.append(pe_ref)
                        # For the coverage calculations:
                        start, end = pep_pos
                        covered_aa_pos.update(  range(start, end+1) )
            pdh_data['peptideevidencerefs'] = ''.join(pe_refs)
            # Coverage calculus:
            pdh_data['coverage'] = len(covered_aa_pos) / len(protein.seq) * 100
            #    
            # Format the final ProteinDetectionHypothesis XML section:
            protdecthyp_xml = self.AD_PDH.format(**pdh_data)
            yield protdecthyp_xml
        
    def analysisdata_xml(self, peptides=None, proteins=None, *args, **kwargs):
        '''
        Write AnalysisData XML section to mzIdentML file, collecting XML code
        for SpectrumIdentificationItem sub-sections (method
        _ad_spectrumresults_xml) for populating each SpectrumIdentificationList
        section corresponding to a search engine results
        '''
        if peptides:
            self.peptides = peptides
        if proteins:
            self.proteins = proteins
        # Write AnalysisData sub-section to mzid file:
        self._iofile.write('\n        <AnalysisData>\n')
        # Write SpectrumIdentificationList data for each search engine results:
        for engine in self.engines:
            self._iofile.write('\n            <SpectrumIdentificationList id="SIL_{0}">\n'.format(engine))
            for spectrum_xml in self._ad_spectrumresults_xml(engine):
                self._iofile.write(spectrum_xml)
            self._iofile.write('\n\n            </SpectrumIdentificationList>\n')
        # Write ProteinDetectionList data:
        self._iofile.write('\n            <ProteinDetectionList id="Integrator_results">\n')
        for group, protdecthyp_xml in enumerate(self._ad_protdecthyps_xml(), 
                                                start=1):
            self._iofile.write('\n                <ProteinAmbiguityGroup id="PAG_{0}">'.format(group))
            self._iofile.write(protdecthyp_xml)
            self._iofile.write('\n                </ProteinAmbiguityGroup>\n')
        self._iofile.write('\n            </ProteinDetectionList>\n')
        #
        self._iofile.write('\n        </AnalysisData>\n')
        #
        return self._iofile
    
    def datacollection_xml(self, *args, **kwargs):
        # Write opening of DataCollection section to mzid file:
        self._iofile.write('\n    <DataCollection>\n')
        
        self.inputs_xml(*args, **kwargs)
        
        self.analysisdata_xml(*args, **kwargs)
        
        # Write ending of DataCollection section:
        self._iofile.write('\n\n    </DataCollection>\n')
        #
        return self._iofile
    
    def write_tail(self, *args, **kwargs):
        self._iofile.write("\n</MzIdentML>")
        return self._iofile
        
    def write_all(self, *args, **kwargs):
        if not self._iofile:
            self.open()
        #
        self.write_head(*args, **kwargs)
        self.analysissoftwarelist_xml(*args, **kwargs)
        self.provider_xml(*args, **kwargs)
        self.sequencecollection_xml(*args, **kwargs)
        self.analysiscollection_xml(*args, **kwargs)
        self.analysisprotocolcollection_xml(*args, **kwargs)
        self.datacollection_xml(*args, **kwargs)
        self.write_tail(*args, **kwargs)
        #
        return self._iofile
    
    def close(self):
        self._iofile.close()
        return self._iofile
