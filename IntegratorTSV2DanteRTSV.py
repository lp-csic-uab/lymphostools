#!/usr/local/bin/python2.7
# encoding: utf-8
"""
IntegratorTSV2DanteRTSV -- Converts some Integrator TSV output files to a suitable DanteR TSV input file

:synopsis:   IntegratorTSV2DanteRTSV is script that converts some Integrator TSV
output files to a suitable DanteR TSV input file.

See the Global variables section to define:
- Configuration constants: ``CLI_PARAMS``, ``DEFAULT_INCLUDE_RE``,
                           ``EXPERIMENTS``, ``GRPER2VAL2HEADER``,
                           ``CONSECUTIVE_ID`` and ``IMPUTE_MV``
- Integrator TSV file constants: ``TMT_COLUMS``, ``ITRAQ_COLUMS``,
                                 ``REPORTER_IONS``,
                                 ``NEW_INTEGRATOR_IMPORTANT_HEADERS``,
                                 ``OLD_INTEGRATOR_IMPORTANT_HEADERS`` and
                                 ``HEADERS_TO_ADD``
- DanteR TSV file constants: ``DANTER_METADATA_HEADERS``
- Execution constants: ``DEBUG`` and ``PROFILE``

It defines the following Classes and Functions:
    :class:`I2DConfiguration`
    :class:`IntegratorTSVLoader`
    :class:`IntegratorExperimentData`
    :class:`DanteRTSVExporter`
    :func:`load_exp_data`
    :func:`process_exp_data`

:created:    2014-01-13

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""
from __future__ import division
from __future__ import print_function

__VERSION__ = '0.99'
__CREATED__ = '2014-01-13'
__UPDATED__ = '2015-10-29'


#===============================================================================
# Imports
#===============================================================================
import sys
import traceback
import os
import csv
import re
import math
import random
from itertools import groupby
from collections import defaultdict, OrderedDict, Iterable

from tools.EasyMUI import Configuration, csv_dict_writer, ProgramCancelled, CLIError

#===============================================================================
# Global variables
#===============================================================================
# Configuration constants:
#    Parameters for the command line interface:
CLI_PARAMS = OrderedDict( (
                 ('verbose', {'arg': ('-v', '--verbose'),
                              'arg_opts': dict(action='count',
                                               help='set verbosity level')
                              } ),
                 ('recursive', {'arg': ('-R', '--recursive'),
                                'arg_opts': dict(action='store_true',
                                                 help='recursively search self.filesandfolders in sub-folders')
                                } ),
#                 ('interactive', {'arg': ('-i', '--interactive'),
#                                  'arg_opts': dict(action='store_true',
#                                                   help='ask for missing parameters')
#                                  } ),
                 ('output_file', {'arg': ('-o', '--output'),
                                  'arg_opts': dict(help='output file as a DanteR TSV input file',
                                                   metavar='output file')
                                  } ),
                 ('include_re', {'arg': ('-inc', '--include'),
                                 'arg_opts': dict(help='only include self.filesandfolders matching this regex pattern from supplied folders. Note: exclude is given preference over include',
                                                  metavar='RE')
                                 } ),
                 ('exclude_re', {'arg': ('-exc', '--exclude'),
                                 'arg_opts': dict(help='exclude self.filesandfolders matching this regex pattern from supplied folders',
                                                  metavar='RE')
                                 } ),
                 ('filesandfolders', {'arg': tuple(),
                                      'arg_opts': dict(help='file(s) or folder(s) to process',
                                                       metavar='file or folder',
                                                       nargs='*')
                                      } ),
                              ) )
#    Configuration defaults:
DEFAULT_INCLUDE_RE = '^.*_extended_report\.xls$'
EXPERIMENTS = OrderedDict()
for exp_id in ('6','7','29','30'):
    EXPERIMENTS[exp_id] = {'qion2time': OrderedDict ( (
                                          ('TMT_126', '0'),   ('TMT_127', '15'),
                                          ('TMT_128', '120'),  ('TMT_129', '0'),
                                          ('TMT_130', '15'), ('TMT_131', '120')
                                                    ) ),
                           'files': list(),
                           'id': exp_id
                           }

GRPER2VAL2HEADER = OrderedDict( (
                                 ('phosphorylated', {'1': 'P', '0': 'NP'}),
                                 ) )
#
CONSECUTIVE_ID = True
IMPUTE_MV = True

# Integrator TSV file constants:
TMT_COLUMNS =   ('TMT_126', 'TMT_127', 'TMT_128', 'TMT_129', 'TMT_130', 'TMT_131')
ITRAQ_COLUMNS = ('ITRAQ_114', 'ITRAQ_115', 'ITRAQ_116', 'ITRAQ_117')
REPORTER_IONS = { 'TMT6plex':   {'headers': TMT_COLUMNS,   'unimod_id': 737},
                  'iTRAQ4plex': {'headers': ITRAQ_COLUMNS, 'unimod_id': 214} }
NEW_INTEGRATOR_IMPORTANT_HEADERS = lambda quant_columns: \
                                    ['file', 'first scan', 'last scan', 'MS',
                                    'consensus unmodified peptide',
                                    'consensus modified peptide', 'consensus z',
                                    'consensus delta_mz', 'consensus proteins',
                                    'consensus first accession',
                                    'consensus first name', 'phosphorylated'] + \
                                    list(quant_columns) +  \
                                    ['ascore revised peptide', 'ascore1', 'ascore2',
                                    'ascore3']
OLD_INTEGRATOR_IMPORTANT_HEADERS = lambda quant_columns: \
                                    ['File', 'First Scan', 'Last scan', 'MS',
                                    'Consensus peptide w/o mods',
                                    'Consensus peptide with mods', 'Consensus z',
                                    'Consensus DeltaMz', 'Consensus proteins',
                                    None, None, 'Phosphorylated'] + \
                                    list(quant_columns) +  \
                                    ['Ascore revised peptide', 'Ascore1', 'Ascore2',
                                    'Ascore3']

# DanteR TSV file constants:
DANTER_METADATA_HEADERS = lambda quant_columns: \
                           ['ID', 'Experiment', 'FINALpeptide'] + \
                           NEW_INTEGRATOR_IMPORTANT_HEADERS(quant_columns)

# Execution constants:
DEBUG = False
PROFILE = False


#===============================================================================
# Class definitions
#===============================================================================
class I2DConfiguration(Configuration):
    """
    Sub-class of :class:`Configuration` to generate some meta-data structures
    needed for other IntegratorTSV2DanteRTSV classes
    """
    _extra_config_file_attrs = ( 'experiments', 'grper2val2header',           #Extra attributes, not in self._config_params_order, to save/load to/from a config file
                                 'consecutive_id', 'impute_mv', 'separator' ) #

    def __init__(self, config_params, config_params_order=None,
                 config_file=None, experiments=None, quant_columns=None,
                 quant_iontype=None, danter_metadata_headers=None,
                 grper2val2header=None, consecutive_id=CONSECUTIVE_ID,
                 max_mv4row=4, impute_mv=IMPUTE_MV, impute_mv_method=None,
                 separator='_', output_file=None, **kwargs):
        """
        :param OrderedDict experiments: keys must be the IDs of the different
        experiments to process. Values must be dictionaries containing a
        'qion2time' sub-dictionary, a 'files' list, and a 'id' string containing
        the experiment ID. Ex.:
            {'qion2time': OrderedDict (( ('TMT_126', '0'),   ('TMT_127', '15'),
                                         ('TMT_128', '30'),  ('TMT_129', '60'),
                                         ('TMT_130', '120'), ('TMT_131', '1440')
                                          )),
            'files': list(),
            'id': 'exp_id'
            }
        :param tuple/list quant_columns: headers of the quantitative columns.
        :param str quant_iontype: quantitative reporter ions used. Must be one
        of the key values of REPORTER_IONS ('TMT6plex' or 'iTRAQ4plex').
        :param list danter_metadata_headers: headers of the DanteR metadata
        columns.
        :param OrderedDict grper2val2header: OrderedDict of dictionaries to
        translate from a value, in a grouper column name, to a header name. Ex.:
            {'Experiment':     {'201': 'Exp201', '202': 'Exp202',
                                '203': 'Exp203', '204': 'Exp204'},
             'phosphorylated': {'1': 'P', '0': 'NP'}, ...
             }
        :param bool consecutive_id: whether to maintain (True) or not (False)
        consecutive row IDs between different files loaded. Defaults to the
        global constant CONSECUTIVE_ID.
        :param bool impute_mv: decides whether to impute new values to replace
        missing values (True) or not (False) when filtering missing values in
        data (:method:`IntegratorExperimentData.filtered_mv`). Defaults to the
        global constant IMPUTE_MV.
        :param string separator: character to use as a separator between group
        headers in new column headers. Defaults to an under-score character (
        "_" ).
        """
        super(I2DConfiguration, self).__init__(config_params,
                                               config_params_order=config_params_order,
                                               config_file=config_file,
                                               **kwargs)
        self._qcolumn2norm_qcolumn = None
        self._exp2time2qheaders = None
        self._exp2qheader2time_qheader = None
        self._exp2time_qheaders = None

        self._last_id = 0

        self._experiments = experiments
        self.quant_columns = quant_columns
        self.quant_iontype = quant_iontype
        self.danter_metadata_headers = danter_metadata_headers
        self.grper2val2header = grper2val2header
        self.consecutive_id = consecutive_id
        self.max_mv4row = max_mv4row
        self.impute_mv = impute_mv
        self.impute_mv_method = impute_mv_method
        self.separator = separator
        self.output_file = output_file

    @staticmethod
    def _swap_odict(old_odict, **sorted_kwargs):
        """
        Swap `old_dict` keys and values; this is:
          {key: value} -> {value: [keys]}

        :param dict old_dict: a dictionary to revert in this way.

        :return: a {value: [keys]} OrderedDict. The order of the reverted dict
        depends on the keyword arguments supplied, that must match those of
        standard :func:`sorted`; that is, `cmp`, `key`, and `reverse`.
        """
        new_dict = OrderedDict()
        for old_key, old_value in sorted(old_odict.items(), **sorted_kwargs):
            new_value = new_dict.get(old_value, list())
            new_value.append(old_key)
            new_dict[old_value] = new_value
        return new_dict

    @property
    def last_id(self):
        if not self.consecutive_id:
            return 0
        return self._last_id
    @last_id.setter
    def last_id(self, last_id):
        self._last_id = last_id

    @property
    def experiments(self):
        return self._experiments
    @experiments.setter
    def experiments(self, experiments):
        self._experiments = experiments
        if experiments:
            # Mark the values to be calculated when they are accessed through
            # their getters:
            self._exp2time2qheaders = None
            self._exp2qheader2time_qheader = None
            self._exp2time_qheaders = None
        else:
            # Set them to void data:
            self._exp2time2qheaders = OrderedDict()
            self._exp2qheader2time_qheader = OrderedDict()
            self._exp2time_qheaders = OrderedDict()

    @property
    def qcolumn2norm_qcolumn(self):
        """
        Lazy and memoized property ``qcolumn2norm_qcolumn``.

        :return OrderedDict : Original quantitative column name in TSV input
        file -> Normalized quantitative column name. Ex.:
            {'TMT_126': 'NORM-TMT_126', 'TMT_127': 'NORM-TMT_127', ...}
        """
        prefix = 'NORM-'
        if self._qcolumn2norm_qcolumn is None and self.quant_columns:
            self._qcolumn2norm_qcolumn = OrderedDict( [(qcol, prefix+qcol) for
                                                       qcol in self.quant_columns] )
        return self._qcolumn2norm_qcolumn

    @property
    def exp2time2qheaders(self):
        """
        Lazy and memoized property ``exp2time2qheaders``.

        :return OrderedDict of OrderedDict : experiment -> time/condition ->
        Original quantitative column names in TSV input file, ordered by
        time/condition. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': { '0': ['TMT_126', 'TMT_127'],
                   '15': ['TMT_128', 'TMT_129'], ... },
             '7': { '0': ['TMT_126', 'TMT_127'],
                   '15': ['TMT_128', 'TMT_129'], ... },
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            {'201': { '0': ['TMT_126'], '15': ['TMT_127'], ... }, ... }
        """
        if self._exp2time2qheaders is None: #Is marked to be (re-)calculated:
            exp2t2qheaders = OrderedDict()
            for exp_id, exp_data in self.experiments.items():
                t2qheaders = self._swap_odict( exp_data['qion2time'],
                                               key=lambda t: (t[1], t[0]) )
                exp2t2qheaders[exp_id] = t2qheaders
            self._exp2time2qheaders = exp2t2qheaders
            self._exp2qheader2time_qheader = None #Mark them to be (re-)calculated later
            self._exp2time_qheaders = None     #
        return self._exp2time2qheaders

    def _exp2time2replicates(self):
        """
        :return dict of dict of list, or None : experiment -> time/condition ->
        replicate header tags. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': { '0': ['R1', 'R2'],
                   '15': ['R1', 'R2'], ... },
             '7': { '0': ['R3', 'R4'],
                   '15': ['R3', 'R4'], ... },
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            None
        """
        exp2time2replicates = dict()
        exp_start_r_n = 0 #Current experiment replicate base number
        for exp_id, time2qheaders in self.exp2time2qheaders.items():
            time2replicates = dict()
            total_exp_r = set()
            for time, qheaders in time2qheaders.items():
                qheaders_len = len(qheaders)
                time2replicates[time] = [ 'R' + str(exp_start_r_n + n)
                                          for n in range(1, qheaders_len+1) ]
                total_exp_r.add(qheaders_len)
            exp2time2replicates[exp_id] = time2replicates
            exp_start_r_n += max(total_exp_r)
        # If each experiment is a replicate by itself, return None:
        if len( exp2time2replicates.keys() ) == total_exp_r: #Same number of experiments and replicates
            exp2time2replicates = None
        #
        return exp2time2replicates

    @property
    def exp2qheader2time_qheader(self):
        """
        Lazy and memoized property ``exp2qheader2time_qheader``.

        :return OrderedDict of OrderedDict : experiment -> Original
        quantitative column name in TSV input file -> New time/condition
        quantitative column name. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': { 'TMT_126': '0_R1_TMT126', 'TMT_127': '0_R2_TMT127', ... },
             '7': { 'TMT_126': '0_R3_TMT126', 'TMT_127': '0_R4_TMT127', ... },
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            {'201': { 'TMT_126': '0_TMT126', 'TMT_127': '15_TMT127', ... }, ... }
        """
        if self._exp2qheader2time_qheader is None: #Is marked to be (re-)calculated:
            exp2qheader2time_qheader = OrderedDict()
            exp2time2replicates = self._exp2time2replicates()
            for exp_id, exp_data in self.experiments.items():
                q_ion2time = exp_data['qion2time']
                qheader2time_qheader = OrderedDict()
                for quant_col in self.quant_columns: #Get the quantitative reporter ions in order.
                    time = q_ion2time[quant_col]
                    qion = quant_col.replace(self.separator, '') #'TMT_126' -> 'TMT126'
                    if exp2time2replicates is not None:
                        replicate = exp2time2replicates[exp_id][time].pop(0)
                        header_tags = (time, replicate, qion) #('0', 'R1', 'TMT126')
                    else:
                        header_tags = (time, qion) #('0', 'TMT126')
                    qheader2time_qheader[quant_col] = self.separator.join(header_tags)
                exp2qheader2time_qheader[exp_id] = qheader2time_qheader
            self._exp2qheader2time_qheader = exp2qheader2time_qheader
        return self._exp2qheader2time_qheader

    @property
    def exp2time_qheaders(self):
        """
        Lazy and memoized property ``exp2time_qheaders``.

        :return OrderedDict of lists : experiment -> All New normalized
        quantitative column names. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': ['0_R1_TMT126', '15_R1_TMT128', '0_R2_TMT127', '15_R2_TMT129', ...],
             '7': ['0_R3_TMT126', '15_R3_TMT128', '0_R4_TMT127', '15_R4_TMT129', ...],
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            {'201': ['0_TMT126', '15_TMT127', ... ], ... }
        """
        if self._exp2time_qheaders is None: #Is marked to be (re-)calculated:
            exp2time_quantheaders = OrderedDict()
            for exp_id, time2qheaders in self.exp2time2qheaders.items():
                qheader2time_qheader = self.exp2qheader2time_qheader[exp_id]
                time_totalqheaders = list()
                for qheaders in zip( *time2qheaders.values() ):
                    time_totalqheaders.extend(qheader2time_qheader[qheader] for
                                              qheader in qheaders)
                exp2time_quantheaders[exp_id] = time_totalqheaders
            self._exp2time_qheaders = exp2time_quantheaders
        return self._exp2time_qheaders

    def ask4experiments(self, default=None): #REFACTORIZE
        """
        Interactively get needed common and individual metadata information for
        the experiments to process.

        :param OrderedDict default: default values for the experiment's
        metadata.

        :return: a OrderedDict with the experiment metadata, also stored as
        ``self.experiments``.
        """
        if not default and self.experiments:
            default = self.experiments
        #
        experiments = OrderedDict() #New experiments metadata dictionary
        title = self.prg_name + ' Configuration'
        #
        # Common experiment metadata:
        # - Number of experiments:
        def_total_exp = str( len(default) )
        msg = 'How many experiment replicates to proccess?'
        total_exp = self.ui.inputbox(msg, title, default=def_total_exp)
        if total_exp is None or total_exp == '0':
            raise ProgramCancelled()
        total_exp = int(total_exp)
        # - Reporter ions used (TMT, iTraq):
        if self.quant_iontype not in REPORTER_IONS.keys(): #`self.quant_iontype` must be a valid REPORTER_IONS key
            msg = 'Select the quantitative reporter ions used'
            qiontype = self.ui.choicebox( msg, title, REPORTER_IONS.keys() )
            if qiontype is None:
                raise ProgramCancelled()
            self.quant_iontype = qiontype
        self.quant_columns = REPORTER_IONS[self.quant_iontype]['headers']
        self.danter_metadata_headers = DANTER_METADATA_HEADERS(self.quant_columns)
        #
        # Individual experiment metadata:
        folder = os.getcwd()
        zero_qion2time = OrderedDict.fromkeys(self.quant_columns, '0')
        last_qion2time = zero_qion2time
        for exp_n in range(1, total_exp+1):
            exp_data = dict() #New individual experiment metadata dictionary
            # - Experiment ID:
            title_rplct = title + ' (Exp. replicate {0})'.format(exp_n)
            try:
                def_exp_id = default.keys()[exp_n-1]
            except IndexError as _:
                def_exp_id = str(exp_n)
            msg = 'What is the ID for {0}th experiment replicate?'.format(exp_n)
            exp_id = self.ui.inputbox(msg, title_rplct, default=def_exp_id)
            if exp_id is None:
                raise ProgramCancelled()
            exp_data['id'] = exp_id
            title_rplct = title + ' (Exp. replicate {0})'.format(exp_id)
            # - Get the experimental condition/time to each quantitative
            #   reporter ion:
            def_qion2time = default.get( exp_id, dict() ).get( 'qion2time',     #Try to get a default value
                                                               last_qion2time ) #
            if set( zero_qion2time.keys() ) != set( def_qion2time.keys() ): #Check if the reporter ions of the defaults are the same as the wanted ones
                def_qion2time = zero_qion2time
            msg = 'What is the experimental condition/time for each {0} reporter-ion?'.format(qiontype)
            qion2time = self.ui.multi_inputbox( msg, title_rplct,
                                                fields2defaults=def_qion2time )
            if qion2time is None:
                raise ProgramCancelled()
            last_qion2time = qion2time.copy()
            exp_data['qion2time'] = qion2time
            # - Get the Integrator TSV data file/s for each experiment:
            folder = self.ui.selectfolder( 'Witch Folder contains the experiment Integrator TSV files to convert?',
                                           title_rplct + '. Select a Folder',
                                           default=folder )
            if folder is None:
                raise ProgramCancelled()
            infiles = self.ui.selectfiles( 'Witch Integrator TSV files from chosen folder do you want to convert?',
                                           title_rplct + '. Select Files',
                                           default='*.xls', folder=folder,
                                           recursive=self.recursive,
                                           include_re=self.include_re,
                                           exclude_re=self.exclude_re )
            if infiles is None:
                raise ProgramCancelled()
            exp_data['files'] = infiles
            # - Add individual experiment's metadata to total experiment's
            #   OrderedDict:
            experiments[exp_id] = exp_data
        #
        self.experiments = experiments
        return self.experiments

    def ask4grper2val2header(self, default=None):
        """
        Interactively get common grouping information for the experiments to
        process: get the headers and values to use for the quantitative data
        classification for DanteR.

        :param OrderedDict default: default values for the experiment's
        data grouping.

        :return OrderedDict of dict : experiment's grouping condition ->
        condition value -> DanteR classification header. Ex.:
            {'Experiment': { '6': '6', '7': '7', ...},
             'phosphorylated', {'1': 'P', '0': 'NP'}, ...}
        This is also stored as ``self.grper2val2header``.
        """
        if not default and self.grper2val2header:
            default = self.grper2val2header
        # Calculate possible grouping conditions (groupers) from metadata headers:
        quant_headers = self.experiments.values()[0]['qion2time'].keys()
        common_headers = set( DANTER_METADATA_HEADERS(quant_headers) )
        common_headers.difference_update(quant_headers)
        common_headers.difference_update( ('', 'Experiment') )
        # Ask user to choose groupers (one or more):
        title = self.prg_name + ' Configuration (Data Classification)'
        msg = 'Select the headers you want to use for data classification'
        grpers = self.ui.multi_choicebox(msg, title, sorted(common_headers))
        if grpers is None:
            raise ProgramCancelled()
        #
        grper2val2header = OrderedDict()
        # Adds 'Experiment' as the first grouper:
        exp_ids = self.experiments.keys()
        grper2val2header['Experiment'] = dict( zip(exp_ids, exp_ids) )
        # Ask user for values and corresponding DanteR classification headers
        # for the other selected groupers:
        for grper in grpers:
            grper_title = '{0}-({1})'.format(title, grper)
            val2header = default.get(grper, None)
            if not val2header:
                msg = 'Enter a comma separated list of the possible values that can have header ' + grper
                vals = self.ui.inputbox(msg, grper_title)
                vals = set( map( str.strip, re.split(', |,', vals) ) )
                vals.discard('')
                val2header = dict( zip(vals, vals) )
            msg = 'For each value of header ' + grper + ' type a classification tag'
            val2header = self.ui.multi_inputbox( msg, grper_title,
                                                 fields2defaults=val2header )
            #
            grper2val2header[grper] = val2header
        #
        self.grper2val2header = grper2val2header
        return self.grper2val2header

    def ask4max_mv4row(self):
        """
        Interactively get the maximum number of missing values for quantitative
        reporter ions to allow for each row of data.
        """
        min_allowed = 0
        max_allowed = len(self.quant_columns)
        max_mv4row = -1
        title = self.prg_name + ' Configuration (Missing Values)'
        msg = 'How many Missing Values to allow for each quantitative peptide ({0}-{1})?'.format(min_allowed, max_allowed)
        while max_mv4row < min_allowed or max_mv4row > max_allowed:
            max_mv4row = self.ui.inputbox(msg, title, self.max_mv4row)
            if max_mv4row is None:
                raise ProgramCancelled()
            try:
                max_mv4row = int(max_mv4row)
            except ValueError as _:
                pass
        self.max_mv4row = max_mv4row
        return max_mv4row

    def ask4impute_mv(self):
        """
        Interactively get how to impute missing values for quantitative
        reporter ions.
        """
        title = self.prg_name + ' Configuration (Missing Values imputation)'
        msg = 'Do you want Missing Values in your data to be replaced with imputed data?'
        self.impute_mv = self.ui.ynbox(msg, title)
        if self.impute_mv:
            # Ask imputation time (Before or After normalization):
            msg = 'When to do the imputation of missing values, Before or After the normalization?'
            self.impute_mv = self.ui.buttonbox(msg, title, ('Before', 'After'))
            # Ask imputation method:
            msg = 'What method do you want to use to do the imputation of missing values?'
            impute_mv_method = self.ui.choicebox( msg, title, ['Fixed Value', 'Minimum Randomized Value', 'Mean of 0.1% Minimum Values'] )
            if impute_mv_method is None:
                raise ProgramCancelled()
            self.impute_mv_method = impute_mv_method
        return self.impute_mv, impute_mv_method

    def ask4output_file(self):
        output_file = self.ui.filesavebox('Name of the file to save the result',
                                          self.prg_name + ' Save as...')
        if output_file is None:
            raise ProgramCancelled()
        if not os.path.splitext(output_file)[1]: #No extension given to file:
            output_file += '.tsv'
        self.output_file = output_file
        return output_file


class IntegratorTSVLoader(object):
    """
    Integrator TSV output file loader class
    """
    _headers_to_add = ('ID', 'Experiment', 'FINALpeptide', 'MV') #Headers and informatio to be added to each loaded row.

    def __init__(self, filen=None, experiment=None, column_filter=None,
                 quant_columns=TMT_COLUMNS, quant_iontype='TMT6plex',
                 autoload=True, id_start=0):
        """
        :param string filen: name of the file to load.
        :param string experiment: experiment ID of the file to load.
        :param iterable/dictionary column_filter: the names of the
        columns to retain. If it is a dictionary it must be of the form:
        ``{ 'old column name': 'new column name', ... }``. Defaults to the
        global NEW_INTEGRATOR_IMPORTANT_HEADERS.
        :param iterable quant_columns: name of the columns with quantitative
        data. Defaults to the global TMT_COLUMNS.
        :param string quant_iontype: name of the quantitative ion reporter
        type used. Defaults to 'TMT6plex'.
        :param bool autoload: if True data is loaded from `filen` as soon as it
        is asigned/changed.
        :param int id_start: the ID to start numbering rows imported from
        an Integretor TSV file. Defaults to 0.
        """
        self._filen = None
        self._file_type = None
        self._colfilter = None
        self._data = list()

        self.experiment = experiment
        self.quant_columns = quant_columns
        self.quant_iontype = quant_iontype
        self.autoload = autoload
        self.id_start = self.last_id = id_start

        self.column_filter = column_filter

        self.filen = filen

    @property
    def data(self):
        """
        Getter of ``self.data`` read-only property.

        :return: a copy of the list of row dictionaries (also copied) contained
        in ``self._data``.
        """
        return [row.copy() for row in self._data]

    @property
    def filen(self):
        return self._filen
    @filen.setter
    def filen(self, filen):
        if filen is not self._filen:
            self._filen = filen
            self._file_type = None
            if filen:
                if self.experiment is None: #TO_DO: Eliminate because only valid for some kind of file names?
                    # Try to get experiment number from file name:
                    exp_id = os.path.split(self.filen)[1].split('_')[0]
                    if exp_id.isdigit():
                        self.experiment = str(int(exp_id)) #Fix '06' to '6'
                if self.autoload:
                    self.load()
            else:
                self._data = list()

    @property
    def column_filter(self):
        return self._colfilter
    @column_filter.setter
    def column_filter(self, column_filter):
        if column_filter and not isinstance(column_filter, dict):
            column_filter = dict( zip(column_filter, column_filter) )
        self._colfilter = column_filter

    def _filter_columns(self, row, colfilter=None):
        """
        Discard columns with unneeded information (those that are not present in
        `colfilter`), and rename them according to `colfilter` dictionary.

        :param dictionary row: data row dictionary to filter/rename columns.
        :param dictionary colfilter: is used to filter those columns that have
        keys in it, and to rename them: ``{old_column_name: new_column_name}`` .
        Defaults to ``self._colfilter``.

        :return: filtered/renamed data row dictionary.
        """
        if colfilter is None:
            colfilter = self._colfilter
        filtered_row = dict()
        for old_coln, new_coln in colfilter.items():
            filtered_row[new_coln] = row.get(old_coln, '')
        return filtered_row

    @staticmethod
    def _make_finalpeptide(row):
        """
        R(737)NS(21)LTGEEGQLAR -> RNsLTGEEGQLAR

        :param dictionary row: data row dictionary.

        :return: a string containing the peptide formated according to
        modifications (phosphorylation).
        """
        if row['ascore revised peptide'].strip('. '):
            mod_pep = row['ascore revised peptide']
        else:
            mod_pep = row['consensus modified peptide']
        final_pep = ''
        aa_mod = re.split('[\\)\\(, ]', mod_pep)
        for each in aa_mod:
            if each.isdigit():
                if each in ('21', '23'):
                    final_pep = final_pep[:-1] + final_pep[-1].lower()
            else:
                final_pep += each
        return final_pep

    @staticmethod
    def _get_file_type_from_io(io_file):
        """
        Basic static method to get the file type information ('NEW' or 'OLD')
        from `io_file` file.

        :param file io_file: a file input/output stream.

        :return: a string containing the file type of `io_file` as 'NEW' or
        'OLD'.
        """
        file_pos = io_file.tell()
        io_file.seek(0, 0)
        line1 = io_file.readline().split('\t')
        io_file.seek(file_pos, 0)
        if line1[0]:
            return 'OLD'
        else:
            return 'NEW'

    def get_file_type(self, io_file=None):
        """
        Get the file type information ('NEW' or 'OLD'). Also sets the default
        ``self.column_filter`` dictionary (if not set at __init__) according to
        this information.

        :param file io_file: optional file input/output stream to get the file
        type information from.

        :return: a string containing the file type of `io_file` or
        ``self.filen`` as 'NEW' or 'OLD'.
        """
        if self._file_type:
            return self._file_type
        # Get the file type from `io_file` or ``self.filen``:
        if io_file:
            self._file_type = self._get_file_type_from_io(io_file)
        elif self.filen:
            with open(self.filen, 'rU') as io_file:
                self._file_type = self._get_file_type_from_io(io_file)
        # Set the default ``self.column_filter`` dictionary (if not set at __init__):
        if self.column_filter is None and self._file_type:
            if self._file_type == 'NEW':
                self.column_filter = NEW_INTEGRATOR_IMPORTANT_HEADERS(self.quant_columns)
            else:
                self.column_filter = dict( zip( OLD_INTEGRATOR_IMPORTANT_HEADERS(self.quant_columns),
                                                NEW_INTEGRATOR_IMPORTANT_HEADERS(self.quant_columns) ) )
        return self._file_type

    @classmethod
    def get_file_headers(cls, io_file, file_type=None):
        """
        A class method to get column headers from the two first lines of an
        Integrator TSV `io_file`.

        :param file io_file: a file input/output stream.
        :param string file_type: optinal pre-calculated file type of `io_file`
        as 'NEW' or 'OLD'.

        :return: a list of column headers from `io_file`
        """
        if not file_type:
            file_type = cls._get_file_type_from_io(io_file)
        #
        if file_type == 'NEW':
            line1 = io_file.readline().split('\t')
            line2 = io_file.readline().split('\t')
            line1.extend( [''] * (len(line2) - len(line1)) )
            raw_headers = map(' '.join, zip(line1, line2))
        elif file_type == 'OLD':
            raw_headers = io_file.readline().split('\t')
        #
        return map(lambda s: s.strip(), raw_headers)

    def _load_wo_processing(self):
        """
        Generator for loading Integrator TSV output file.
        """
        with open(self.filen, 'rU') as io_file:
            self.get_file_type(io_file)
            headers = self.get_file_headers(io_file, self._file_type)
            #
            csvdr = csv.DictReader(io_file, fieldnames=headers, delimiter='\t')
            for row in csvdr:
                yield row

    def load(self, id_start=None):
        """
        Load Integrator TSV output file, and do some Processing of the data.
        Loaded and processed data is stored in ``self._data``.

        :param int id_start: the ID to start numbering rows imported from
        an Integretor TSV file

        :return: ``self.data``. Namely, a copy of loaded and processed data as
        a list of row dictionaries.
        """
        if id_start:
            self.id_start = id_start
        #
        row_id = self.id_start
        for row in self._load_wo_processing():
            # Filter the columns loaded:
            row = self._filter_columns(row)
            # Replace '.' by '0' in 'OLD' Integrator rows:
            if self._file_type == 'OLD':
                for col, value in row.items():
                    if value == '.': row[col] = '0'
            # Convert data to int in 'first scan', 'last scan' and 'MS' columns:
            for col in ('first scan', 'last scan', 'MS'):
                row[col] = int( row[col] )
            # Convert data to float in the quantitative columns:
            for quant_col in self.quant_columns:
                row[quant_col] = float( row[quant_col] )
            # Add new information columns:
            row_id += 1
            if 'ID' in self._headers_to_add:
                row['ID'] = row_id
            if 'Experiment' in self._headers_to_add:
                row['Experiment'] = self.experiment
            if 'FINALpeptide' in self._headers_to_add:
                row['FINALpeptide'] = self._make_finalpeptide(row)
            if 'MV' in self._headers_to_add:
                # Calculate the number of missing values for each row:
                qaunt_vals = [row[quant_col] for quant_col in self.quant_columns]
                row['MV'] = qaunt_vals.count(0.0)
            self._data.append(row)
        self.last_id = row_id
        return self.data


class IntegratorExperimentData(object):
    """
    Class to manipulate and get information from a single quantitative
    experiment Integretor TSV loaded data.
    """

    def __init__(self, data=None, loader=None, headers=None,
                 quant_columns=TMT_COLUMNS, quant_iontype='TMT6plex',
                 qcolumn2norm_qcolumn=None):
        """
        :param list data: list of row dictionaries for working with.
        :param IntegratorTSVLoader loader: a loader instance to use for data
        loading.
        :param iterable headers: name of the data columns.
        :param iterable quant_columns: name of the columns with quantitative
        data. Defaults to the global TMT_COLUMNS.
        :param string quant_iontype: name of the quantitative ion reporter
        type used. Defaults to 'TMT6plex'.
        :param dictionary qcolumn2norm_qcolumn: dictionary to translate from
        Original quantitative column name in TSV input file to Normalized
        quantitative column name. Ex.:
            {'TMT_126': 'NORM-TMT_126', 'TMT_127': 'NORM-TMT_127', ...}
        """
        self._data = None
        self._all_headers = set()
        self._headers = list()
        self._qcolumn2norm_qcolumn = None
        self._loader = None
        self._experiment = None
        self._quantcols_summary = None
        self._rebuild_summaries = False
        self._data_normalized = False

        self.data = data
        self.loader = loader
        self.headers = headers
        self.quant_columns = quant_columns
        self.quant_iontype = quant_iontype
        self.qcolumn2norm_qcolumn = qcolumn2norm_qcolumn

    def _set_data(self, data):
        """
        Base ``self.data`` setter method

        :param list data: list of row dictionaries for working with.
        """
        if data:
            # Set self._data and self._experiment from data:
            self._data = data
            self._experiment = data[0]['Experiment']
            # Set self._all_headers and self.headers:
            all_headers = set()
            for row in self.data:
                all_headers.update(row.keys())
            self._all_headers = all_headers
            self.headers = self.headers #Check if previous defined headers are valid for new data
        else:
            self._data = list()
            self._experiment = None
            self._headers = list()
            self._all_headers = set()
        #
        self._rebuild_summaries = True
        #
        return self

    @property
    def data(self):
        return self._data
    @data.setter
    def data(self, data):
        """
        ``self.data`` setter public interface

        :param list data: list of row dictionaries for working with.

        :warning: This will always reset ``self._data_normalized`` to ``False``
        even if the passed `data` is already normalized.
        """
        self._set_data(data)
        #
        self._data_normalized = False #CAUTION! This step will reset ``self._data_normalized`` to ``False`` even if the passed data is already normalized.

    @property
    def experiment(self):
        return self._experiment

    @property
    def headers(self):
        return self._headers
    @headers.setter
    def headers(self, headers):
        if headers and self._all_headers.issuperset(headers):
            self._headers = list(headers)
        else:
            self._headers = list(self._all_headers)

    @property
    def qcolumn2norm_qcolumn(self):
        """
        Lazy and memoized property ``qcolumn2norm_qcolumn``.

        :return OrderedDict : Original quantitative column name in TSV input
        file -> Normalized quantitative column name. Ex.:
            {'TMT_126': 'NORM-TMT_126', 'TMT_127': 'NORM-TMT_127', ...}
        """
        if self._qcolumn2norm_qcolumn is None:
            self._qcolumn2norm_qcolumn = OrderedDict( [(qcol, 'NORM-'+qcol) for
                                                       qcol in self.quant_columns] )
        return self._qcolumn2norm_qcolumn
    @qcolumn2norm_qcolumn.setter
    def qcolumn2norm_qcolumn(self, qcolumn2norm_qcolumn):
        self._qcolumn2norm_qcolumn = qcolumn2norm_qcolumn

    @property
    def loader(self):
        return self._loader
    @loader.setter
    def loader(self, loader):
        self._loader = loader
        if not self._data and loader:
            # Get the data from the supplied loader object:
            if loader.data:
                self.data = loader.data
            else:
                self.data = loader.load()

    @property
    def quantcols_summary(self):
        """
        Generate a summary, or return a previous generated one if data has not
        changed, with data about each quantitative ion reporter data column in
        ``self.data``.

        :return: summary data as a dictionary of dictionaries. Ex.:
            { 'TMT_126': {'min': 1010.4, 'max': 2330219.8, ... } }
        """
        if not self.data:
            self._quantcols_summary = None
        elif self._rebuild_summaries or not self._quantcols_summary:
            self._quantcols_summary = self._quantcols_summary4data()
            self._rebuild_summaries = False
        return self._quantcols_summary

    @staticmethod
    def _mean(values):
        return sum(values) / len(values)

    def _stdv(self, values):
        values_len = len(values)
        if values_len < 2: #No possible stdv calculation:
            return 0
        mean = self._mean(values)
        variance = sum( [(value - mean)**2 for value in values] ) / (values_len - 1)
        return math.sqrt(variance)

    @staticmethod
    def _median(values):
        working_val = sorted(values)
        #
        n = len(working_val)
        middle = int(n / 2)
        if n % 2 == 0: #Pair
            return (working_val[middle - 1] + working_val[middle]) / 2
        else: #Odd
            return working_val[middle]

    def _quantcols_summary4data(self, data=None, quant_cols=None,
                                count_zeros=False, min_sample_percentage=0.1):
        """
        Generate a summary with data about each quantitative ion reporter data
        column in `data`.

        :param list data: list of row dictionaries for generating the summary.
        Defaults to ``self.data``.
        :param iterable quant_cols: quantitative column names/headers.
        Defaults to ``self.quant_columns``.
        :param bool count_zeros: decides whether to take zero values into
        account or not. Defaults to False.
        :param float min_sample_percentage: lowest percentage of the
        quantitative column values to get for some calculations.

        :return dict of dict : summary data as a dictionary of dictionaries. Ex.:
            { 'TMT_126': {'min': 1010.4, 'max': 2330219.8, ... } }
        """
        if data is None:
            data = self.data
        if quant_cols is None:
            quant_cols = self.quant_columns
        min_len_multiplier = min_sample_percentage / 100
        #
        summary = dict()
        for quant_col in quant_cols:
            # Get sorted quantitative column data:
            if not count_zeros: #Only non-zero values, sorted (min->max):
                col_data = sorted( row[quant_col] for row in data
                                   if row[quant_col] > 0 )
            else: #All the values, sorted (min->max):
                col_data = sorted( row[quant_col] for row in data )
            # Calculate quantitative column summary:
            col_summary = dict()
            col_summary['min'] = col_data[0]  #col_data is already sorted
            col_summary['max'] = col_data[-1] #
            col_summary['mean'] = self._mean(col_data)
            col_summary['stdv'] = self._stdv(col_data)
            col_summary['median'] = self._median(col_data)
            # Calculate some summary for a small sample of the lowest values:
            value_sample_len = int(round( len(col_data) * min_len_multiplier ))
            if value_sample_len < 3:
                value_sample_len = 3 #At least take 3 values from the sample.
            minvalues_sample=col_data[:value_sample_len] #Get a percentage of the lowestquantitative column values.
            col_summary['minvalues_mean'] = self._mean(minvalues_sample)
#             col_summary['minvalues_stdv'] = self._stdv(minvalues_sample)
            #
            summary[quant_col] = col_summary
        return summary

    def __getitem__(self, key):
        if isinstance(key, int): #:return: a row dictionary
            return self.data[key]
        elif key in self._all_headers: #:return: a column as a dictionary
            return { key: [row.get(key, None) for row in self.data] }
        else:
            raise KeyError( str(key) )

    def __setitem__(self, key, values):
        if isinstance(key, int): #Replace a previous row at position = key
            if isinstance(values, dict):
                row = values #with a dictionary
            elif isinstance(values, Iterable):
                row = dict( zip(self.headers, values) ) #with an iterable, transformed to a dictionary
            else:
                row = dict().fromkeys(self.headers, values) #with another kind of object, transformed to a dictionary
            self.data[key] = row #Replace the previous row at position = key with the new one
        else: #Replace or Add a column with header = key
            if isinstance(values, Iterable): #with an iterable
                values = list(values)
                val_size = len(values)
                data_size = len(self.data)
                if val_size < data_size:
                    values = values + [None]*(data_size-val_size)
                elif val_size > data_size:
                    values = values[:data_size]
                for index, row in enumerate(self.data):
                    row[key] = values[index]
            else: #with another kind of object
                for row in self.data:
                    row[key] = values
            self._all_headers.add(key)
        self._rebuild_summaries = True

    def copy(self):
        """
        :return: a copy of itself containing a copy of its actual data
        (``self.data``).
        """
        self_copy = self.__class__()
        self_copy._data = [row.copy() for row in self._data]
        self_copy._rebuild_summaries = self._rebuild_summaries
        self_copy._all_headers = set(self._all_headers)
        self_copy._headers = list(self._headers)
        self_copy._loader = self._loader
        self_copy.quant_columns = self.quant_columns
        self_copy._qcolumn2norm_qcolumn = self._qcolumn2norm_qcolumn
        self_copy._quantcols_summary = self._quantcols_summary
        self_copy._data_normalized = self._data_normalized
        return self_copy

    def filtered_noquant(self):
        """
        Return data from ``self.data`` without no quantitative identifications.

        :return: filtered data as a list of row dictionaries.
        """
        unimod_id = str( REPORTER_IONS[ self.quant_iontype ]['unimod_id'] )
        filtered_data = list()
        for row in self.data:
            if unimod_id in row['consensus modified peptide']:
                    filtered_data.append(row)
        return filtered_data

    def filter_noquant(self):
        """
        Eliminate no quantitative identifications from ``self.data``.

        :return: self.
        """
        self._set_data( self.filtered_noquant() )
        return self

    def filtered_ms3(self):
        """
        Return data from ``self.data`` without redundant MS3 information.

        :return: filtered data as a list of row dictionaries.
        """
        filtered_data = list()
        last_row = {key: 0 for key in self.headers}
        for row in sorted(self.data,
                          key = lambda d: (d['file'], d['last scan'], d['MS'])):
            if not (row['last scan'] == last_row['last scan'] + 1 and
                    row['MS'] == last_row['MS'] + 1):
                    filtered_data.append(row)
            # X-NOTE: In the case we decide to do NOT take into account
            # different identified pepetides from MS2 and corresponding MS3 use
            # the following ALTERNATIVE CODE:
            #if int(row['first scan']) == int(last_row['last scan']) + 1:
            #    if int(row['MS']) == int(last_row['MS']) + 1:
            #        if (last_row['consensus unmodified peptide'] !=
            #            row['consensus unmodified peptide']):
            #            filtered_data.pop(-1)
            #    else:
            #        filtered_data.append(row)
            #else:
            #        filtered_data.append(row)
            last_row = row
        return filtered_data

    def filter_ms3(self):
        """
        Eliminate redundant MS3 information from ``self.data``.

        :return: self.
        """
        self._set_data( self.filtered_ms3() )
        return self

    def splitted_p_nop(self, data=None, phospho_column='phosphorylated',
                       phospho_true_value=1):
        """
        Split data rows in `data` or ``self.data`` in phosporylated and
        not posporylated data.

        :param list data: list of row dictionaries containing the data to
        group/split.
        Defaults to ``self.data``.
        :param str phospho_column: column header indicating the phosphorylated
        condition. Defaults to 'phosphorylated'.
        :param object phospo_true_value: Value of the column if the peptide is
        phosphorylated. Defaults to 1.

        :return tuple of lists: data splitted in phosphorylated and not
        phosphorylated.
        """
        if data is None:
            data = self.data
        #
        phospho = list()
        nophospho = list()
        for row in data:
            if int(row[phospho_column]) == phospho_true_value:
                phospho.append(row)
            else:
                nophospho.append(row)
        #
        return phospho, nophospho

    def mv_summary(self):
        """
        Generate a summary with data about ALL the missing values for
        quantitative ion reporters in ``self.data``.

        :return list of dict : summary data as a list of row dictionaries.
        """
        quant_columns = self.quant_columns

        summary = list()
        # Split ``self.data`` rows in 2 data groups: phospho and nophospho:
        datagroups_names = ('Phosphorylated:', 'Not-Posphorylated:')
        datagroups = self.splitted_p_nop()
        # Calculate the total number of quantitative missing values for each
        # reporter ion in each data group (``mv_row[quant_col]``) and in total
        # (``totals_row[quant_col]``), and for all the reporter ions in each
        # data group (``mv_row['Scans']``) and in total
        # (``totals_row['Scans']``):
        totals_row = defaultdict(int)
        totals_row[''] = 'Totals:'
        for index, datagroup in enumerate(datagroups):
            mv_row = defaultdict(int)
            mv_row[''] = datagroups_names[index]
            mv_row['Scans'] = len(datagroup)
            for row in datagroup:
                if row['MV'] > 0: #Only process rows with quantitative missing values
                    for quant_col in quant_columns:
                        if row[quant_col] == 0: #Missing value
                            mv_row[quant_col] += 1
            summary.append(mv_row)
            totals_row['Scans'] += mv_row['Scans']
            for quant_col in quant_columns:
                totals_row[quant_col] += mv_row[quant_col]
        summary.append(totals_row)
        # Calculate % :
        for mv_row in summary:
            if mv_row['Scans'] > 0:
                for quant_col in quant_columns:
                    mv_row['%'+quant_col] = mv_row[quant_col] / mv_row['Scans'] * 100
            else:
                for quant_col in quant_columns:
                    mv_row['%'+quant_col] = 0

        return summary

    def filtered_mv(self, data=None, max_mv4row=4,
                    impute_mv=False, rplc_mv_func=None):
        """
        Return only those rows from ``self.data`` with less quantitative missing
        values than `max_mv4row`. If `impute_mv` is set to True, missing
        values are replaced wit the results of :func:`min_mv_func`.

        :param list data: list of row dictionaries for filtering missing
        values. Defaults to ``self.data``.
        :param int max_mv4row: maximun number of quantitative missing values
        for each row. Defaults to 4.
        :param bool impute_mv: decides what to do when filtering missing
        values in data:
        True-> impute new values to replace missing values,
        False-> don't do it.
        Defaults to False.
        :param func rplc_mv_func: function that generates random values to
        replace the quantitative missing values. Defaults to
        ``self._rplc_mv_w_min_mv``.

        :return list of dict: filtered data as a list of copied row dictionaries.
        """
        if data is None:
            data = self.data
        #
        filtered_data = [ row.copy() for row in data
                          if row['MV'] <= max_mv4row ]
        if impute_mv:
            filtered_data = self.imputed_mv(filtered_data, rplc_mv_func)
        #
        return filtered_data

    def filter_mv(self, max_mv4row=4, impute_mv=False, rplc_mv_func=None):
        """
        Eliminate rows with more quantitative missing values (mv) than
        `max_mv4row` from ``self.data``. If `impute_mv` is set to True, missing
        values are replaced wit the results of :func:`min_mv_func`.

        :param int max_mv4row: maximun number of quantitative missing values
        for each row. Defaults to 4.
        :param bool impute_mv: decides what to do when filtering
        missing values in ``self.data``:
         True-> impute new values to replace missing values,
         False-> don't do it.
        Defaults to False.
        :param func rplc_mv_func: function that generates random values that
        replace the quantitative missing values. Defaults to
        ``self._rplc_mv_w_minsample_mv``.

        :return: self.
        """
        self._set_data( self.filtered_mv(self.data, max_mv4row,
                                         impute_mv, rplc_mv_func) )
        return self

    @staticmethod
    def _rplc_mv_w_min_mv(summary):
        """
        Default function that generates normal random values arround the minum
        value to replace the quantitative missing values.
        """
        random.seed()
        return random.normalvariate(summary['min'], summary['min'] / 10) # IMPROVE: use the min? use anther sigma better than min/10?

    @staticmethod
    def _rplc_mv_w_fixed_val(summary):
        """
        Function that returns a fixed value to replace the quantitative
        missing values.
        :caution: Only for comparisson with LymPHOS PQuantifier with fixed
        value cut-off and LymPHOS manual DanteR data processing!!
        """
        return 600 #Fixed value.

    @staticmethod
    def _rplc_mv_w_minsample_mv(summary):
        """
        Function that returns the mean value of a sample of the minimun values
        to replace the quantitative missing values.
        """
        return summary['minvalues_mean']

    def imputed_mv(self, data=None, rplc_mv_func=None):
        """
        Replace missig values from rows in ``self.data``, using a function
        (:func:`rplc_mv_func`).

        :param list data: list of row dictionaries for imputting missing
        values. Defaults to ``self.data``.
        :param func rplc_mv_func: function that generates random values to
        replace the quantitative missing values. Defaults to
        ``self._rplc_mv_w_minsample_mv``.

        :return list of dict : list of row data dictionary copies with the
        quantitative missing values replaced (imputed).
        """
        # Default values:
        if data is None:
            data = self.data
        if rplc_mv_func is None:
            rplc_mv_func = self._rplc_mv_w_minsample_mv
        # Do the imputation (replacement) of missing values:
        if not self.isnormalized(): #Act on original quantitative columns:
            quant_cols = self.quant_columns
            col2summary = self.quantcols_summary
        else: #Act on new normalized quantitative columns:
            quant_cols = self.qcolumn2norm_qcolumn.values()
            col2summary = self._quantcols_summary4data(quant_cols=quant_cols)
        #
        imputed_data = list()
        for row in data:
            row_copy = row.copy()
            if row_copy['MV'] > 0: #Only process rows with quantitative missing values
                for col in quant_cols:
                    if row_copy[col] == 0: #A quantitative missing value
                        row_copy[col] = rplc_mv_func( col2summary[col] )
                row_copy['MV'] = 0
            imputed_data.append(row_copy)
        #
        return imputed_data

    def impute_mv(self, rplc_mv_func=None):
        """
        Replace missig values from rows in ``self.data``, using a function
        (:func:`rplc_mv_func`).

        :param func rplc_mv_func: function that generates random values to
        replace the quantitative missing values. Defaults to
        ``self._rplc_mv_w_min_mv``.

        :return: self.
        """
        self._set_data( self.imputed_mv(rplc_mv_func=rplc_mv_func) )
        return self

    def normalized(self):
        """
        Makes a copy of ``self.data`` and returns the copy with normalized
        quantitative data as new columns, after copying and applying Marina's
        normalization procedure (dividing by the median of non-phosphorylated
        peptides for each time/condition).

        :return list of dict : normalized data as a new list of copied row
        dictionaries.
        """
        # Get no-phospho statistics medians ( `nop_medians` ):
        _, nop = self.splitted_p_nop(self.data)
        nop_summary = self._quantcols_summary4data(nop)
        nop_medians = {qcol: nop_summary[qcol]['median']
                       for qcol in self.quant_columns}
        # Quantitative data normalization, stored in new columns:
        qcols_normqcols = self.qcolumn2norm_qcolumn.items()
        norm_data = list()
        for row in self.data:
            row_copy = row.copy()
            # - Quantitative data normalization:
            norm_qdata_cols = {normqcol: row[qcol]/nop_medians[qcol]
                               for qcol, normqcol in qcols_normqcols}
            # - Store normalized data in new columns:
            row_copy.update(norm_qdata_cols)
            norm_data.append(row_copy)
        #
        return norm_data

    def normalize(self):
        """
        Normalize data in ``self.data`` applying Marina's normalization
        procedure (dividing by the median of non-phosphorylated peptides for
        each time/condition).

        :return: self.
        """
        self._set_data( self.normalized() )
        self._data_normalized = True
        return self

    def isnormalized(self):
        return self._data_normalized


class DanteRTSVExporter(object):
    """
    Class to format data to write it as a DanteR TSV input file
    """

    def __init__(self, filen=None, common_columns=DANTER_METADATA_HEADERS,
                 qcolumn2norm_qcolumn=None, exp2time2qheaders=None,
                 exp2qheader2time_qheader=None, exp2time_qheaders=None,
                 grper2val2header=None, separator='_'):
        """
        :param string filen: name of the file to save the data to.
        :param iterable common_columns: the names of the common
        columns in each row to save. Defaults to the global
        DANTER_METADATA_HEADERS.
        :param OrderedDict qcolumn2norm_qcolumn: dictionary to translate from
        Original quantitative column name in TSV input file to Normalized
        quantitative column name. Ex.:
            {'TMT_126': 'NORM-TMT_126', 'TMT_127': 'NORM-TMT_127', ...}
        :param OrderedDict exp2time2qheaders: experiment -> time/condition ->
        Original quantitative column names in TSV input file, ordered by
        time/condition. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': { '0': ['TMT_126', 'TMT_127'],
                   '15': ['TMT_128', 'TMT_129'], ... },
             '7': { '0': ['TMT_126', 'TMT_127'],
                   '15': ['TMT_128', 'TMT_129'], ... },
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            {'201': { '0': ['TMT_126'], '15': ['TMT_127'], ... }, ... }
        :param OrderedDict exp2qheader2time_qheader: dictionary to translate from
        Original quantitative column name in TSV input file to time-formated
        quantitative column name. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': { 'TMT_126': '0_R1_TMT126', 'TMT_127': '0_R2_TMT127', ... },
             '7': { 'TMT_126': '0_R3_TMT126', 'TMT_127': '0_R4_TMT127', ... },
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            {'201': { 'TMT_126': '0_TMT126', 'TMT_127': '15_TMT127', ... }, ... }
        :param OrderedDict exp2time_qheaders: name of the columns with quantitative
        data, for each experiment to be saved. Ex.:
          -Multiple markers per time/condition (as in primary T-cells experiments):
            {'6': ['0_R1_TMT126', '0_R2_TMT127', '15_R1_TMT128', '15_R2_TMT129', ...],
             '7': ['0_R3_TMT126', '0_R4_TMT127', '15_R3_TMT128', '15_R4_TMT129', ...],
             ...}
          -Only one marker per time/condition (as in Jurkat experiments):
            {'201': ['0_TMT126', '15_TMT127', ... ], ... }
        :param OrderedDict grper2val2header: OrderedDict of dictionaries to
        translate from a value, in a grouper column name, to a header name. Ex.:
            { 'Experiment':     {'201': 'Exp201', '202': 'Exp202',
                                 '203': 'Exp203', '204': 'Exp204'},
              'phosphorylated': {'1': 'P', '0': 'NP'},
              ... }
        :param string separator: character to use as a separator between group
        headers in new column headers. Defaults to an under-score character ( "_" ).
        """
        self._filen = None
        self._exp2time_qheaders = None
        self._grper2val2header = None

        self._grper_columns = list()
        self._vals2gheaders = None
        self._current_quant_headers = list()

        self.headers_written = False #Indicates if te file headers have beeen written (X-NOTE: workarround to bug in python 2.7.5 for win32 in file.tell() when file openend for append).

        self.separator = separator #X-NOTE: must be assigned before :property:`vals2gheaders` or :property:`exp2time_qheaders` are requested
        self.filen = filen
        self.common_columns = common_columns
        self.qcolumn2norm_qcolumn = qcolumn2norm_qcolumn
        self.exp2time2qheaders = exp2time2qheaders
        self.exp2qheader2time_qheader = exp2qheader2time_qheader
        self.exp2time_qheaders = exp2time_qheaders
        self.grper2val2header = grper2val2header

    @property
    def filen(self):
        return self._filen
    @filen.setter
    def filen(self, filen):
        if filen is not self._filen:
            if os.path.exists(filen):
                raise IOError('File {0} already exists!'.format(filen))
            else:
                self._filen = filen
                self.headers_written = False #New file, so it still has no headers written.

    @property
    def exp2time_qheaders(self):
        return self._exp2time_qheaders
    @exp2time_qheaders.setter
    def exp2time_qheaders(self, exp2time_qheaders):
        self._exp2time_qheaders = exp2time_qheaders
        if exp2time_qheaders:
            self._vals2gheaders = None #Let it to be calculated when self.vals2gheaders is accesed
        else:
            self._vals2gheaders = OrderedDict()

    @property
    def grper2val2header(self):
        return self._grper2val2header
    @grper2val2header.setter
    def grper2val2header(self, grper2val2header):
        if grper2val2header:
            if grper2val2header.keys()[0] != 'Experiment':
                # If 'Experiment' is not the first grouper column (or there is
                # not an 'Experiment' grouper), place it in the first place:
                exp_ids = self.exp2time_qheaders.keys()
                exp2exp_header = grper2val2header.pop( 'Experiment',
                                                       dict(zip(exp_ids, exp_ids)) )
                new_grper2val2header = OrderedDict()
                new_grper2val2header['Experiment'] = exp2exp_header
                new_grper2val2header.update(grper2val2header)
                grper2val2header = new_grper2val2header
            self._grper_columns = grper2val2header.keys()
            self._vals2gheaders = None #Mark it to be calculated when self.vals2gheaders is accesed
        else: #Void related attributes:
            self._grper_columns = list()
            self._vals2gheaders = OrderedDict()
        self._grper2val2header = grper2val2header

    @property
    def grper_columns(self):
        return self._grper_columns

    @property
    def vals2gheaders(self):
        """
        Lazy and memoized property ``vals2gheaders``.

        :return OrderedDict : tuples of grouper values -> new quantitative
        column names. Ex:
            { ('201', 1, '0_TMT126'): '201_P_0_TMT126',
              ('201', 1, '15_TMT127'): '201_P_15_TMT127',
              ... ,
              ('204', 0, '180_TMT130'): '204_NP_180_TMT130',
              ('204', 0, '360_TMT131'): '204_NP_360_TMT131',
              ... ,
              }
        """
        if self._vals2gheaders is None: #It must be recalculated because of changes in ``self.time_quant_cols`` or in ``self.grper2val2header``
            # Add `quant_cols` as a new grouper (for time quantitative headers)
            # and put it at the end of a copy of ``self.grper2val2header``
            # OrderedDict:
            grper2val2header = self.grper2val2header.copy()
            grper2val2header['quant_cols'] = None
            # Create all the possible header combinations that will araise from
            # ``self.grper2val2header`` and ``self.exp2time_qheaders``:
            sep = self.separator
            val_combs = [tuple()]
            header_combs = [tuple()]
            for grouper, val2header in grper2val2header.items():
                new_val_combs = list()
                new_header_combs = list()
                for old_val_comb, old_header_comb in zip(val_combs, header_combs):
                    if grouper == 'quant_cols':
                        # Addapt quantitative ``val2header`` dictionary to the
                        # values -> header-names acording to the experiment ID
                        # from the current ``old_val_comb`` (values combination
                        # tuple):
                        exp_id = old_val_comb[0]
                        time_qheaders = self.exp2time_qheaders[exp_id]
                        val2header = OrderedDict( zip(time_qheaders, time_qheaders) )
                    for val, header in val2header.items():
                        new_val_combs.append( old_val_comb + (val,) )
                        new_header_combs.append( old_header_comb + (header,) )
                val_combs = new_val_combs
                header_combs = new_header_combs
            header_combs = [sep.join(headers) for headers in header_combs]
            self._vals2gheaders = OrderedDict( zip(val_combs, header_combs) )
        #
        return self._vals2gheaders

    @property
    def headers(self):
        return list(self.common_columns) + self.vals2gheaders.values()

    @staticmethod
    def rename_cols(data, old2new_headers=None, **kwargs):
        """
        In-situ rename column headers in `data` according to a supplied
        translation dictionary (`old2new_headers`).

        :param list data: list of row dictionaries to rename.
        :param dictinoary old2new_headers: translation dictionary containing the old
        name of the column as key and the new name as value:
            {old_column_name: new_column_name}

        :return: `data` as a list of row renamed dictionaries.
        """
        if old2new_headers:
            old2new_headers.update(kwargs)
        elif kwargs:
            old2new_headers = kwargs
        else: #Nothing to rename
            return data
        # In-situ rename column headers:
        for row in data:
            for old_coln, new_coln in old2new_headers.items():
                try:
                    row[new_coln] = row.pop(old_coln)
                except KeyError as _: #Do not rename non-existing cols in row
                    pass
        #
        return data

    def _get_old2new_qheaders(self, grper_values):
        """
        Create new headers from ``self.exp2time_qheaders`` and
        ``self.vals2gheaders``
        """
        exp_id = grper_values[0]
        old2new_headers = OrderedDict()
        for old_header in self.exp2time_qheaders[exp_id]:
            tmp_grper_values = grper_values + (old_header,) #X-NOTE: the order should be the same as the one defined in :method:`_get_all_vals2newheaders`
            new_header = self.vals2gheaders[tmp_grper_values]
            old2new_headers[old_header] = new_header
        return old2new_headers

    def _format_as_quant_time(self, data):
        """
        Duplicate rows and split quantitative data acorrding to
        `self.exp2time2qheaders`, and format quantitative column headers of
        the row dictionaries acorrding to `self.exp2qheader2time_qheader`.

        :param list data: list of row dictionaries containing the data to format.

        :return list : a list of row formated dictionaries.
        """
        exp2qheader2time_qheader = self.exp2qheader2time_qheader
        qcolumn2norm_qcolumn = self.qcolumn2norm_qcolumn
        qheaders = set( qcolumn2norm_qcolumn.keys() )
        qheader_normqheader = qcolumn2norm_qcolumn.items()
        formated_data = list()
        for row in data:
            exp_id = row['Experiment']
            # Duplicate rows and split quantitative data as needed:
            new_rows = list()
            for idx, r_qheaders in enumerate(zip( *self.exp2time2qheaders[exp_id].values() )):
                new_row = row.copy()
                new_row['ID'] += idx/10.0
                for no_r_qheader in qheaders.difference(r_qheaders):
                    new_row[ qcolumn2norm_qcolumn[no_r_qheader] ] = '' #Delete quantitative data not in this replicate
                new_rows.append(new_row)
            # Rename quanitative columns of new rows:
            for new_row in new_rows:
                for qheader, norm_qheader in qheader_normqheader:
                    new_row[exp2qheader2time_qheader[exp_id][qheader]] = new_row.pop(norm_qheader)
            #
            formated_data.extend(new_rows)
        #
        return formated_data

    def _format_grouped_qdata(self, grouped_data):
        """
        In-situ format quantitative column headers of the row dictionaries from
        the different groups of `grouped_data`, acorrding to the values defining
        each group.

        :param dictinoary grouped_data: a dictionary of type {(value_column1,
        value_column2, ...): [rows]} as those returned by :method:`make_groups`

        :return: a list of row formated dictionaries.
        """
        formated_data = list()
        for grper_values, grped_rows in grouped_data.items():
            old2new_headers = self._get_old2new_qheaders(grper_values)
            formated_data.extend( self.rename_cols(grped_rows, old2new_headers) )
            self._current_quant_headers.extend( old2new_headers.values() )
        return formated_data

    def write(self, data):
        """
        Write `data` to file ``self.filen`` after formating it correctly.

        :param list data: list of row dictionaries for formating and writting to
        disc.
        """
        quant_time_data = self._format_as_quant_time(data)
        grped_data = self.make_groups(quant_time_data)
        formated_data = self._format_grouped_qdata(grped_data)

        with open(self.filen, 'ab') as io_file:
            csvdw = csv.DictWriter(io_file, fieldnames=self.headers,
                                   restval='', extrasaction='ignore',
                                   delimiter='\t')
#             if io_file.tell() == 0: #Output file is a new file (has no data) #X-NOTE: This doesn't works in python 2.7.5 for win32, it's alwais 0, even for files with data.
            if not self.headers_written: #X-NOTE: workarround to bug in python 2.7.5 for win32 in file.tell() always returning 0 when file openend for append
                csvdw.writeheader()
                self.headers_written = True
            csvdw.writerows(formated_data)

        return formated_data

    def make_groups(self, data, grper2val2header=None):
        """
        Group the rows in `data` according to the different values of each
        column in `self.grper_columns`.

        :param list data: list of row dictionaries containing the data to group.
        :param OrderedDict grper2val2header: OrderedDict of dictionaries to
        translate from a value, in a grouper column name, to a header name.
        Defaults to ``self.grper2val2header``.

        :return: a dictionary of type {(value_column1, value_column2, ...): [rows]}
        """
        if grper2val2header:
            self.grper2val2header = grper2val2header

        groups = dict()
        grp_func = lambda row: tuple([row[grper_col] for
                                      grper_col in self.grper_columns])
        data = sorted(data, key = grp_func)
        for grper_col_vals, grped_rows_gnrtr in groupby(data, grp_func):
            groups[grper_col_vals] = list(grped_rows_gnrtr)
        #
        return groups


#===============================================================================
# Function definitions
#===============================================================================
def load_exp_data(exp_id, config):
    title = config.prg_name + ' Working, please wait...'
    title_error = config.prg_name + ' Error loading file!'
    #
    exp_data = list()
    for filen in config.experiments[exp_id]['files']:
        if os.path.exists(filen) and os.path.isfile(filen):
            if config.verbose > 0:
                msg = 'Loading experiment {0} file:\n{1}'.format(exp_id, filen)
                config.ui.nonblockingmsgbox(msg, title)
            loader = IntegratorTSVLoader(filen,
                                         experiment=exp_id,
                                         quant_columns=config.quant_columns,
                                         id_start=config.last_id)
            exp_data.extend(loader.data)
            config.last_id = loader.last_id
        else:
            msg = 'Experiment {0} file does not exists!:\n{1}'.format(exp_id,
                                                                      filen)
            config.ui.msgbox(msg, title_error)
    return exp_data


def process_exp_data(exp_id, exp_data, config):
    title = config.prg_name + ' Working, please wait...'
    if config.verbose > 0:
        config.ui.nonblockingmsgbox( 'Processing experiment {0}'.format(exp_id),
                                     title )
    expid_filen_head = '{0}.{1}'.format(config.output_file, exp_id)
    # Create and configure a data handler object, to manipulate the data:
    datahandler = IntegratorExperimentData(exp_data)
    datahandler.quant_iontype = config.quant_iontype
    datahandler.quant_columns = config.quant_columns
    datahandler.qcolumns2norm_qcolumns = config.qcolumn2norm_qcolumn
    rplc_mv_func = {'Fixed Value': datahandler._rplc_mv_w_fixed_val,
                    'Minimum Randomized Value': datahandler._rplc_mv_w_min_mv,
                    'Mean of 0.1% Minimum Values': datahandler._rplc_mv_w_minsample_mv}
    # Eliminate redundant MS3 information from the data:
    datahandler.filter_ms3()
    # Get and save to disc a summary of the missing values found for each
    # reporter ion:
    mv_summary = datahandler.mv_summary()
    mv_summary_filen = expid_filen_head + '.ALL_missing_values_summary.tsv'
    csv_dict_writer( mv_summary_filen, mv_summary,
                     title='Summary with data about ALL the missing values for quantitative ion reporters' )
    # Eliminate data rows with too many missing values:
    datahandler.filter_mv(max_mv4row=config.max_mv4row)
    # Get and save to disc a summary of the missing values found for each
    # reporter ion, but only for peptides without too many missing values:
    mv_summary = datahandler.mv_summary()
    mv_summary_filen = expid_filen_head + '.Filtered_missing_values_summary.tsv'
    csv_dict_writer( mv_summary_filen, mv_summary,
                     title='Summary with data about the missing values for quantitative ion reporters, but only for peptides without too many missing values (<6)' )
    # Get and save to disc a summary of statisdtical values for each reporter
    # ion:
    quantcols_summary = list()
    for quantcol, summary in sorted(datahandler.quantcols_summary.items()):
        summary[''] = quantcol
        quantcols_summary.append(summary)
    quantcols_summary_filen = expid_filen_head + '.quantitative_summary.tsv'
    csv_dict_writer( quantcols_summary_filen, quantcols_summary,
                     title = os.path.basename(quantcols_summary_filen) )
    # Replace missing values by a minimun, depending on the user configuration:
    if config.impute_mv == 'Before':
        datahandler.impute_mv( rplc_mv_func[config.impute_mv_method] )
    # Normalize quantitative values based on the medians of non-phosporylated
    # peptides:
    datahandler.normalize()
    # Replace missing values by a minimun, depending on the user configuration:
    if config.impute_mv == 'After':
        datahandler.impute_mv( rplc_mv_func[config.impute_mv_method] )
    #
    return datahandler.data


def main(*args):
    config = I2DConfiguration(CLI_PARAMS, version=__VERSION__,
                              created=__CREATED__, updated=__UPDATED__)
    if config.include_re:
        config.include_re = (DEFAULT_INCLUDE_RE, config.include_re)
    else:
        config.include_re = (DEFAULT_INCLUDE_RE, )
    if config.exclude_re:
        config.exclude_re = (config.exclude_re, )
    else:
        config.exclude_re = tuple()
    #
    if config.ui.gui and not config.verbose:
        config.verbose = 1

    try:
        wellcome_msg = ['Welcome to {0} program.'.format(config.prg_name),
                        'Version {0} (build {1})'.format(config.version, config.updated),
                        '']
        if config.verbose > 0:
            wellcome_msg.append(' Verbose mode On')
            if config.recursive:
                wellcome_msg.append(' Recursive mode for folders: ON')
#            if config.interactive:
#                wellcome_msg.append(' Interactive mode: ON')

        config.ui.msgbox( '\n'.join(wellcome_msg), config.prg_name )

        if config.include_re and config.exclude_re and config.include_re == config.exclude_re:
            raise CLIError('Include and Exclude patterns are equal! Nothing will be processed.')

        config.ask4experiments(default=EXPERIMENTS)

        config.ask4grper2val2header(default=GRPER2VAL2HEADER)

        config.ask4max_mv4row()
        config.ask4impute_mv()

        config.ask4output_file()

        if not config.experiments or not config.grper2val2header or not config.output_file:
            if config.verbose > 0:
                config.ui.msgbox('Not enought data supplied for processing!!\n',
                                 config.prg_name + ' Error!')
            config.parser.print_usage()
            return 1

        if not config.ui.ynbox():
            return 1

        writer = DanteRTSVExporter(filen=config.output_file,
                                   common_columns=config.danter_metadata_headers,
                                   qcolumn2norm_qcolumn=config.qcolumn2norm_qcolumn,
                                   exp2time2qheaders=config.exp2time2qheaders,
                                   exp2qheader2time_qheader=config.exp2qheader2time_qheader,
                                   exp2time_qheaders=config.exp2time_qheaders,
                                   grper2val2header=config.grper2val2header)
        for exp_id in config.experiments.keys():
            exp_data = load_exp_data(exp_id, config)
            processed_data = process_exp_data(exp_id, exp_data, config)
            writer.write(processed_data)
        #
        config.ui.nonblockingmsgbox().close()
        config.ui.msgbox('Ok. Job finished', config.prg_name + ' Finished!')
        return 0

    except (KeyboardInterrupt, ProgramCancelled):
        ### Handle keyboard interrupt, and program cancellation
        config.ui.msgbox('Program Cancelled by user!',
                         config.prg_name + ' Cancelled!')
        return 1

    except SystemExit:
        ### Handle system exit (for example when -h)
        return 0

    # Un-comment when in production mode:
    except Exception as e:
        if DEBUG:
            raise(e)

        indent = (len(config.prg_name) + 2) * ' '
        err_title = config.prg_name + ': Internal Error'
        err_msg = e.message
        err_trace = traceback.format_exc()
        sys.stderr.write(err_title + '\n')
        sys.stderr.write(indent + err_msg + '\n')
        sys.stderr.write(err_trace)
        config.ui.textbox(msg='ERROR!: '+err_msg, title=err_title,
                          text=err_trace)
        return 2



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    if DEBUG:
        sys.argv.append('-v')
        sys.argv.append('-R')
#        sys.argv.append('--force-cli') #DEBUG cli
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'IntegratorTSV2DanteRTSV_profile.txt'
        cProfile.run('main()', profile_filename)
        with open('IntegratorTSV2DanteRTSV_profile_stats.txt', 'wb') as statsfile:
            p = pstats.Stats(profile_filename, stream=statsfile)
            stats = p.strip_dirs().sort_stats('cumulative')
            stats.print_stats()
        sys.exit(0)
    #
    exit_code = main()
    #
    sys.exit(exit_code)
