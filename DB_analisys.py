#!/usr/bin/env python
# encoding: utf-8
'''
Created on 2013/11/29

@authors: Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
@version: 0.1
'''

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
from __future__ import division

import sys

###########
# WARNING!! This script is DEPRECATED! Use DB_analisys2.py instead!
sys.exit('WARNING!! This script is DEPRECATED! Use DB_analisys2.py instead!')
# WARNING!! This script is DEPRECATED! Use DB_analisys2.py instead!
###########


import os

# LymPHOS Configuration Paths:
if os.name == 'posix':
    LYMPHOS_BASE_PATH = r'/mnt/Dades/Laboratorio Proteomica CSIC-UAB/Programas/LymPHOS Web/src'
elif os.name == 'nt':
    LYMPHOS_BASE_PATH = r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src'
sys.path.append(LYMPHOS_BASE_PATH)
LYMPHOS_SRC_PATH = os.path.join(LYMPHOS_BASE_PATH, 'LymPHOS_v1_5')
sys.path.append(LYMPHOS_SRC_PATH)

# LymPHOS Environment:
import django
if django.get_version() < '1.5':
    from django.core import management
    import LymPHOS_v1_5.settings as LymPHOS_settings
    management.setup_environ(LymPHOS_settings)
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'LymPHOS_v1_5.settings'
    from django.conf import settings as LymPHOS_settings

from LymPHOS_v1_5.lymphos import models

#===============================================================================
# Global variables
#===============================================================================
REGEX_REPLACE = {'B': '[BND]',
                 'Z': '[ZEQ]',
                 'J': '[JIL]'}


#===============================================================================
# Function definitions
#===============================================================================
def hc_psites():
    # 2.- Hihg confidence p-sites:
    HC_psites = 0
    for pep in models.PeptideCache.objects.filter(phospho=True):
        for ascore in pep.ascores:
            if ascore > 19:
                HC_psites += 1
    return HC_psites


def phospho_aa():
    # 3.- pSer, pThr, pTyr:
    psites = {'s': [0, 0],  # [0] -> Total ; [1] -> High Confidence (ascore >19)
              't': [0, 0],  #
              'y': [0, 0]}  #
    for pep in models.PeptideCache.objects.filter(phospho=True):
        ascores = pep.ascores
        for index, psite in enumerate(pep.psites):
            paa = pep.peptide[psite]
            psites[paa][0] += 1
            if ascores[index]>19:
                psites[paa][1] +=1
    return psites


def spectra_with_Ppeptides():
    # Spectra identified as phosphopeptides:
    return models.Data.objects.filter(phospho=True).count()


def quant_spectra_with_Ppeptides():
    # 4.- Quantified from Spectra identified as phosphopeptides:
    return models.Data.objects.filter(phospho=True).exclude(dataquant__isnull=True).count()


def unique_Ppeptides():
    # 5.- Number of non-redundant phosphorilated peptides:
    return models.PeptideCache.objects.filter(phospho=True).count()


def quant_unique_Ppeptides():
    # 5.- Quantified from Number of non-redundant phosphorilated peptides:
    return models.PeptideCache.objects.filter(phospho=True, quantitative=True).count()


def Pproteins():
    # 5.- Number of phosphoproteins:
    return models.Proteins.objects.filter(phospho=True, is_decoy=False).distinct().count()


def quant_Pproteins():
    # 5.- Quantified from Number of phosphoproteins:
    return models.Proteins.objects.filter(phospho=True, is_decoy=False, data__dataquant__isnull=False).distinct().count()


def quant_hc_psites_and_phospho_aa():
    # 6.- Quantified Total p-sites, Hihg confidence p-sites and pSer, pThr, pTyr:
    psites = {'s': [0, 0],  # [0] -> Total ; [1] -> High Confidence (ascore >19)
              't': [0, 0],  #
              'y': [0, 0]}  #
    for pep in models.PeptideCache.objects.filter(phospho=True, quantitative=True):
        ascores = pep.ascores
        for index, psite in enumerate(pep.psites):
            paa = pep.peptide[psite]
            psites[paa][0] += 1
            if ascores[index]>19:
                psites[paa][1] +=1
    return psites


def regex_format(rs_search):
    rl_search = list()
    rkeys = REGEX_REPLACE.keys()
    for each in list(rs_search):
        if each in rkeys:
            each = REGEX_REPLACE[each]
        rl_search.append(each)
    return ''.join(rl_search)


def peptide_regulation_information():
    # 7.- New Up/Down/NotChanging Number of non-redundant phosphorilated peptides, Total p-sites, Hihg confidence p-sites and pSer, pThr, pTyr:
    psites = dict()
    for regulation in range(-1, 3):
        psites[regulation] = {'s': [0, 0],
                              't': [0, 0],
                              'y': [0, 0]}
    for pep in models.PeptideCache.objects.filter(phospho=True, quantitative=True): #Search quantified non-redundant peptides
        ascores = pep.ascores
        #Decide if peptide is Up/Down/NotChanging regulated:
        regulations = set()
        pep_search = regex_format(pep.peptide)
        for supra_exp in models.QuantPepExpCond.objects.filter(unique_pep__regex=pep_search):
            for time, values in supra_exp.time_values().items():
                regulations.add(values[0]['regulation'])
        regulation = None
        dif_regul = len(regulations)
        if dif_regul == 2 and 0 in regulations:
            regulations.remove(0)
        elif dif_regul > 1:
            regulations = set([2])
        regulation = regulations.pop()
        #
        for index, psite in enumerate(pep.psites):
            paa = pep.peptide[psite]
            psites[regulation][paa][0] += 1
            if ascores[index] > 19:
                psites[regulation][paa][1] +=1
    return psites


def protein_regulation_information():
    # 8.- Regulation of phosphoproteins:
    psites = dict()
    for regulation in range(-1, 3):
        psites[regulation] = {'s': [0, 0],
                              't': [0, 0],
                              'y': [0, 0]}
    for pep in models.PeptideCache.objects.filter(phospho=True, quantitative=True): #Search quantified non-redundant phospho-peptides
        ascores = pep.ascores
        #Decide if peptide is Up/Down/NotChanging regulated:
        regulations = set()
        pep_search = regex_format(pep.peptide)
        for supra_exp in models.QuantPepExpCond.objects.filter(unique_pep__regex = pep_search):
            for time, values in supra_exp.time_values().items():
                regulations.add(values[0]['regulation'])
        regulation = None
        dif_regul = len(regulations)
        if dif_regul == 2 and 0 in regulations:
            regulations.remove(0)
        elif dif_regul > 1:
            regulations = set([2])
        regulation = regulations.pop()
        #
        if regulation < 2:
            for prot in pep.proteins:
                pass
            
    return


def main():
    print hc_psites()
    print ''
    
    psites = phospho_aa()
    for i in range(2):
        print '{0} | {1} | {2}'.format(psites['s'][i], psites['t'][i], psites['y'][i])
    print ''
    
    print spectra_with_Ppeptides()
    print unique_Ppeptides()
    print Pproteins()
    print ''
    
    print quant_spectra_with_Ppeptides()
    print quant_unique_Ppeptides()
    print quant_Pproteins()
    print ''
    
    psites = quant_hc_psites_and_phospho_aa()
    for i in range(2):
        print psites['s'][i] + psites['t'][i] + psites['y'][i]
        print '{0} | {1} | {2}'.format(psites['s'][i], psites['t'][i], psites['y'][i])
    print ''
    
    psites = peptide_regulation_information()
    out_lines = list()
    for i in range(2):
        outline = list()
        linesum = 0
        for regulation in [1, -1, 0, 2]: #Up/Down/NotChanging/Variables_in_time
            psite = psites[regulation]
            outline.append('{0} | {1} | {2}'.format(psite['s'][i], psite['t'][i], psite['y'][i]))
            linesum += psite['s'][i] + psite['t'][i] + psite['y'][i]
        outline.append(str(linesum))
        out_lines.append('\t'.join(outline))    
    print '\n'.join(out_lines)
    #PySlices Marker Information -- Begin Output Slice
    #984:121:7    207:33:2    533:79:13    95:5:2    2081
    #793:81:5    167:23:2    389:46:6    81:3:2    1598


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    main()

