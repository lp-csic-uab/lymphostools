# -*- mode: python -*-
a = Analysis(['DanteRDB2TSV.py'],
             pathex=['F:\\LymPHOS Tools'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='DanteRDB2TSV.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True , icon='DanteRDB2TSV.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='DanteRDB2TSV')
