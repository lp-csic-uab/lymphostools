#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
'''
WARNING!! This script is DEPRECATED! Use export_LymPHOSBD2CSVReport.py instead!

Created on 2012/05/04

@authors: Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
'''

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
import sys


###########
# WARNING!! This script is DEPRECATED! Use export_LymPHOSBD2CSVReport.py instead!
sys.exit('WARNING!! This script is DEPRECATED! Use export_LymPHOSBD2CSVReport.py instead!')
# WARNING!! This script is DEPRECATED! Use export_LymPHOSBD2CSVReport.py instead!
###########


import os
if os.name == 'posix':
    LYMPHOS_BASE_PATH = r'/mnt/Dades/Laboratorio Proteomica CSIC-UAB/Programas/LymPHOS Web/src'
elif os.name == 'nt':
    LYMPHOS_BASE_PATH = r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src'
sys.path.append(LYMPHOS_BASE_PATH)
LYMPHOS_SRC_PATH = os.path.join(LYMPHOS_BASE_PATH, 'LymPHOS_v1_5')
sys.path.append(LYMPHOS_SRC_PATH)

from django.core import management
import LymPHOS_v1_5.settings
management.setup_environ(LymPHOS_v1_5.settings)

from LymPHOS_v1_5.lymphos import models
from django.db.models import Count
import csv
import math
import numpy as np
from scipy import stats
from collections import defaultdict
from datetime import datetime


#===============================================================================
# Global variables
#===============================================================================
INFO_HEADER = """DATA EXPORTED FROM LymPHOS DB
Date&Time:; {0}
Original File Name:; {1}
DataBase filter:; {2}

3*SD Filter:; 1: Caution! The peptide SD > 3*(mean of SDs)
; 0: No problem (The peptide SD <= 3*(mean of SDs)
t-test:; 1: change with the confidence indicated by %
; 0: no confidence of change

"""
HEADER = """;C vs 15;;;;;;;;;;;;;;C vs 120;;;;;;;;;;;;;;C vs 240;;;;;;;;;;;;;;Proteins;
SEQ.;Mean Ratio;SD;Fold-change;3*SD Filter;t-test 95%;t-test 99%;#exps;#scans;#Ascore>19 psite-1;#Ascore>19 psite-2;#Ascore>19 psite-3;#scans down;#scans no-change;#scans up;Mean Ratio;SD;Fold-change;3*SD Filter;t-test 95%;t-test 99%;#exps;#scans;#Ascore>19 psite-1;#Ascore>19 psite-2;#Ascore>19 psite-3;#scans down;#scans no-change;#scans up;Mean Ratio;SD;Fold-change;3*SD Filter;t-test 95%;t-test 99%;#exps;#scans;#Ascore>19 psite-1;#Ascore>19 psite-2;#Ascore>19 psite-3;#scans down;#scans no-change;#scans up;#prots.;Prot. ACs
"""
#
OUT_FILENAME = 'LymPHOS_CSV_Export_{0}.xls'
EXTRA_OUT_FILENAME = 'LymPHOS_Export_{0}_{1}.txt'
#
#------------------------------------------------------------------------------ 
# LymPHOS time-supra-experiment statistics for no-phospho-peptides from PQuantifier
# logfile : 
#------------------------------------------------------------------------------ 
#NOP_STATS = { 15: {'mu':  0.011690, 'sigma': 0.107094, 'n': 1492},
#             120: {'mu': -0.008885, 'sigma': 0.106351, 'n': 1492},
#             240: {'mu': -0.037884, 'sigma': 0.306896, 'n':  210},
#             }
#------------------------------------------------------------------------------ 

#------------------------------------------------------------------------------ 
# LymPHOS JM experiment statistics for no-phospho-peptides from PQuantifier
# logfile for Jurkat experiments:
#------------------------------------------------------------------------------ 
NOP_STATS = {201: {'mu': 0.0679, 'sigma': -0.2146, 'n':  823-40},
             202: {'mu': 0.5299, 'sigma':  0.4775, 'n': 1824-90},
             203: {'mu': 0.3451, 'sigma':  0.4609, 'n': 1407-70},
             }
#------------------------------------------------------------------------------ 

#------------------------------------------------------------------------------ 
# LymPHOS experiment statistics for no-phospho-peptides from PQuantifier
# logfile : 
#------------------------------------------------------------------------------ 
#NOP_STATS = {  1: {'mu':  0.0242, 'sigma':  0.4109, 'n':  607-6 },
#               2: {'mu':  0.0215, 'sigma':  0.3453, 'n':  567-4 },
#               5: {'mu':  1.4039, 'sigma':  0.4002, 'n':  178-2 },
#               6: {'mu':  0.2732, 'sigma':  0.1668, 'n': 1656-32},
#               7: {'mu':  0.2914, 'sigma': -0.1483, 'n': 3215-22},
#              29: {'mu':  0.2047, 'sigma': -0.1468, 'n': 3327-66},
#              30: {'mu':  0.2884, 'sigma':  0.3139, 'n': 2879-56},
#              35: {'mu': -0.4271, 'sigma': -0.2647, 'n': 1838-36},
#             }
#------------------------------------------------------------------------------ 

#------------------------------------------------------------------------------ 
# LymPHOS supra-experiment statistics for no-phospho-peptides from PQuantifier
# logfile for experiment 35: 
#------------------------------------------------------------------------------ 
#NOP_STATS = {9: {'mu': -0.4271, 'sigma': -0.2647, 'n': 1838-36}}
#------------------------------------------------------------------------------ 

EXPERIMENTS = [(35,)] #List with tuples of experiment IDs to process in packs. Empty list or None for all.
QUANTITATIVE = True #True: Only quantitative data, False: only non-quantitative data, None: both
PHOSPHO = True #True: Only phosphorilated data, False: only non-phosphorilated data, None: both


#===============================================================================
# Function definitions
#===============================================================================
def tstudent(alpha, df):
    '''
    Return T values from a real t-student distribution, not a table of pre-
    calculated values
    '''
    T = stats.t.interval(1-alpha, df)[1]
    return T


def ttest(mean1, mean2, sd1, sd2, n1, n2):
    '''
    This test also known as Welch's t-test is used only when the two population
    variances are assumed to be different (the two sample sizes may or may not
    be equal) and hence must be estimated separately.
    
    - http://en.wikipedia.org/wiki/Student%27s_t-test#Unequal_sample_sizes.2C_unequal_variance
    - Welch, B. L. (1947), "The generalization of "student's" problem when 
      several different population variances are involved.", Biometrika 34: 28-35
    '''
    mean_dif_variance = (sd1 ** 2) / n1 + (sd2 ** 2) / n2
    mean_dif_sd = math.sqrt(mean_dif_variance)
    t = (mean1 - mean2) / mean_dif_sd
    return abs(t)


def degrees_freedom(mean1, mean2, sd1, sd2, n1, n2):
    '''
    This is the Welch-Satterthwaite equation.
    
    - Satterthwaite, F. E. (1946), "An Approximate Distribution of Estimates of 
      Variance Components.", Biometrics Bulletin 2: 110-114, doi:10.2307/3002019
    '''
    sd1n1 = (sd1 ** 2) / n1
    sd2n2 = (sd2 ** 2) / n2
    df = ((sd1n1 + sd2n2) ** 2) / ((sd1n1 ** 2) / (n1 - 1) + (sd2n2 ** 2) / (n2 - 1))
    return df


def mean_of_sds(peps):
    stdvs = defaultdict(list)
    pepconds = models.QuantPepExpCond.objects.filter(unique_pep__in = peps)
    for pepcond in pepconds:
        time_values = pepcond.time_values()
        for time, (reg_d, reg_v, ratio, stdv) in time_values.items():
            stdvs[time].append(stdv)
    means = dict()
    for time in stdvs.keys():
        means[time] = np.mean(stdvs[time])
    return means


def get_protein_acs(pep_names=None, experiments=None, phospho=True, quant=None):
    filters = dict()
    if pep_names:
        filters['data__peptide__in'] = pep_names
    if experiments:
        filters['data__experiment__id__in'] = experiments
    if phospho is not None:
        filters['phospho'] = phospho
    if quant:
        #filters['data__qdata__gt'] = ''
        filters['dataquant__isnull'] = False
    elif quant is not None:
        #filters['data__qdata'] = ''
        filters['dataquant__isnull'] = True
    
    query = models.Proteins.objects.filter(**filters).distinct()
    return set(query.values_list('ac', flat=True))
    

def get_peptide_names(experiments=None, phospho=True, quant=None, **kwargs):
    filters = kwargs
    if experiments:
        filters['experiment__id__in'] = experiments
    if phospho is not None:
        filters['phospho'] = phospho
    if quant:
        #filters['data__qdata__gt'] = ''
        filters['dataquant__isnull'] = False
    elif quant is not None:
        #filters['data__qdata'] = ''
        filters['dataquant__isnull'] = True
    
    query = models.Data.objects.filter(**filters).distinct()
    return set(query.values_list('peptide', flat=True))
    

def save_iterable(file_name, iterable):
    with open(file_name, 'wb') as iofile:
        iofile.write( '\n'.join(sorted( iterable )) )
    

def write_data(io_file, data, dialect='excel-tab'):
    csvwriter = csv.writer(io_file, dialect)
    quant_times = {15: 'C vs 15 min.', 120: 'C vs 2 h.', 240: 'C vs 4 h.'}
    #
    peps = set( data.values_list('peptide', flat=True) )
    sd_means3 = dict()
    for time, stdv in mean_of_sds(peps).items():
        sd_means3[time] = stdv*3
    #
    for peptide in sorted(peps):
        row = list()
        #Add peptide sequence to the peptide row:
        row.append(peptide)
        #Add time information to the peptide row, grouping all conditions for the peptide:
        pepconds = models.QuantPepExpCond.objects.select_related(depth=1).filter(unique_pep = peptide, dataquant__data__in = data).distinct()
        time_row = defaultdict(list) #Time data sub-row for current peptide for all specified experiments 
        for pepcond in pepconds:
            pepcond_data = data.filter(dataquant__quantpepexpcond = pepcond)
            n_spectra = pepcond_data.count() #Number of quantitative spectra
            #Ascore information and Quantitative values for the current time condition
            ascores_count = [0, 0, 0]
            time_values = defaultdict( lambda: ([],[], []) )
            for data_record in pepcond_data:
                for n_psite, ascore in enumerate(data_record.ascores):
                    if ascore > 19: ascores_count[n_psite] += 1
                record_time_values = data_record.dataquant.time_values()
                for time, (reg_vs, ratio, stdv) in record_time_values.items():
                    time_values[time][0].extend(reg_vs)
                    time_values[time][1].append(ratio)
                    time_values[time][2].append(stdv)
            #Number of quantitative experiments for the current peptide and
            #condition (pepcond)
            n_each_exp = pepcond_data.values('experiment').annotate(Count('experiment')) #Let SQL to group related data records by experiment
            n_exp = n_each_exp.count()
            #Quantitative data and statistics for the current peptide and
            #condition (pepcond): time_values = {pepcond.time_values()}
            for time, (reg_vs, ratios, stdvs) in time_values.items():
                reg_v = [reg_vs.count(-1), reg_vs.count(0), reg_vs.count(1)]
                if n_spectra > 1:
                    ratio = np.mean(ratios)
                    stdv = np.std(ratios)
                    expgroup_id = pepcond.expgroup.id
                    #Student's t-test for mean log2ratio of 2 populations (no
                    #phosphorilated, and current peptide in current condition in
                    #current time) with unequal sample sizes, unequal variance
                    df = degrees_freedom(NOP_STATS[expgroup_id]['mu'],    ratio, 
                                         NOP_STATS[expgroup_id]['sigma'], stdv, 
                                         NOP_STATS[expgroup_id]['n'],     n_spectra)
                    tstat = ttest(NOP_STATS[expgroup_id]['mu'],    ratio, 
                                  NOP_STATS[expgroup_id]['sigma'], stdv, 
                                  NOP_STATS[expgroup_id]['n'],     n_spectra)
                    thyp95 = 0 if tstat < tstudent(0.05, df) else 1
                    thyp99 = 0 if tstat < tstudent(0.01, df) else 1
                else:
                    ratio = ratios[0]
                    stdv = stdvs[0]
                    thyp95 = thyp99 = '-'
                #Alerts (value 1) if the stdv is greater than 3x mean stdv
                sd_filter = 0 if stdv < sd_means3[time] else 1
                #Add the data to condition/time sub-row
                time_row[time].extend( (ratio, stdv, 2**ratio, sd_filter, 
                                        thyp95, thyp99, n_exp, n_spectra) 
                                      )
                time_row[time].extend(ascores_count)
                time_row[time].extend(reg_v)
        #Append condition/time sub-row data (if available) to peptide row
        for time in sorted(quant_times.keys()):
            row.extend( time_row.get(time, ['-']*14) )
        #Append Protein information to the peptide row:
        cached_pep = models.PeptideCache.objects.get(peptide = peptide)
        row.append(cached_pep.n_prots) #Number of matching proteins
        row.extend( sorted([prot[0] for prot in cached_pep.l_prots]) ) #ACs of matchings proteins
        #
        csvwriter.writerow(row)
    #
    return


def export_lymphosdb(filename, filters, extras_filename=None, exp_pack=None):
    #Get the data query:
    data = models.Data.objects.filter(**filters).distinct()
    #Process&Write data:
    print('Exporting LymPHOS DB quantitative data. Please wait....')
    #
    with open(filename, 'wb') as io_file: #Open output file
        #Write the header:
        io_file.write( (INFO_HEADER.format(datetime.now(), filename, filters) + HEADER).replace(';', '\t') )
        #Write the data rows:
        write_data(io_file, data)
    #
    print('See file \"{0}\" for results.'.format(filename))
    #
    if extras_filename:
        #Process&Write Extra data:
        protein_acs = get_protein_acs(experiments=exp_pack)
        save_iterable(extras_filename.format('prots_'), protein_acs)
        save_iterable(extras_filename.format('prots_no_isof'), 
                      {prot.split('-')[0] for prot in protein_acs})
        #
        peptide_names = get_peptide_names(experiments=exp_pack)
        save_iterable(extras_filename.format('peptides'), peptide_names)
        save_iterable(extras_filename.format('peptides_no_mods'), 
                      {pep.upper() for pep in peptide_names})


def main():
    '''
    Main program control
    '''
    time0 = datetime.now()

    #Define filters for searching LymPHOS DB Data table:
    filters = dict()
    if PHOSPHO is not None:
        filters['phospho'] = PHOSPHO
    if QUANTITATIVE:
        #filters['qdata__gt'] = ''
        filters['dataquant__isnull'] = False
    elif QUANTITATIVE is not None:
        #filters['qdata'] = ''
        filters['dataquant__isnull'] = True
    #Export selected/all experiments to file/s:
    if EXPERIMENTS:
        for exp_pack in EXPERIMENTS:
            filters['experiment__id__in'] = exp_pack
            exp_name = 'Exp' + '_'.join(map(str, exp_pack))
            filename = OUT_FILENAME.format(exp_name) 
            extras_filename = EXTRA_OUT_FILENAME.format(exp_name)
            export_lymphosdb(filename, filters, extras_filename, exp_pack)
    else:
        exp_name = 'ExpALL'
        filename = OUT_FILENAME.format(exp_name)
        extras_filename = EXTRA_OUT_FILENAME.format(exp_name)
        export_lymphosdb(filename, filters, extras_filename)
    
    dtime = datetime.now() - time0
    print('\nData exported in {0} seconds'.format(dtime.seconds))
    


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    main()
    #        
    print('\nEnd of program execution.')
