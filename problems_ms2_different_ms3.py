#!/usr/bin/env python
# -*- coding: utf8 -*-
#
"""
problems_ms2_different_ms3
:synopsis: Check problems related to MS2 identifications different from corresponding MS3 ones in Integrator TSV output files.

:created:    2014-01-16

:author:     Óscar_Gallardo (ogallard@gmail_com) at LP_CSIC_UAB (lp_csic@uab_cat)
:copyright:  2014 LP_CSIC_UAB. Some rights reserved.
:license:    GPLv3

:contact:    lp_csic@uab_cat

:version:    0.1
:updated:    2014-01-16
"""

#===============================================================================
# Imports
#===============================================================================
import IntegratorTSV2DanteRTSV as i2d
import json
import csv
from datetime import datetime
from collections import OrderedDict


#===============================================================================
# Global variables
#===============================================================================
__version__ = 0.1
__created__ = '2014-01-16'
__updated__ = '2014-01-16'

# Test Files:
#FILEN = '/home/oga/Baixades/Jurkat/Wild type - Jurkat E6.1/201/201_eos_Ab_0_15_30_60_120_1440_Fr00_extended_report.xls'
#JSONFILEN = '/home/oga/tmp/201_ops_Ab_0_15_30_60_120_1440_Fr00.db'
FILEN = '/home/oga/tmp/Lymphos_GLOBAL_TMT_Julio2013.csv'
JSONFILEN = None

# Output Files and File Templates:
JSONBADDATA_FILEN = '/home/oga/json_bad_data_test.db'
MGFBADDATA_FILEN = '/home/oga/mgf_bad_data_test.mgf'
NOT_IN_JSON_F_TMPLT = '/home/oga/ms{0}_not_in_jsondata.csv'


#===============================================================================
# Function definitions
#===============================================================================
def main():
    data = i2d.IntegratorTSVLoader(FILEN).data
    baddata = list()
    last_row = data[-1].copy()
    with open('/home/oga/ms3pepdiferrentfromms2.csv','wb') as io_file:
        csvdw = csv.DictWriter(io_file, fieldnames=sorted(last_row.keys()), delimiter='\t')
        csvdw.writeheader()
        for row in sorted(data, key = lambda d: (   d['file']   , d['last scan'], d['MS'])):
            if (int(row['last scan']) == int(last_row['last scan']) + 1 and 
                int(row['MS']) == int(last_row['MS']) + 1):
                if last_row['consensus unmodified peptide']   !=   row['consensus unmodified peptide']:
                    csvdw.writerow(last_row)
                    csvdw.writerow(row)
                    baddata.append(last_row)
                    baddata.append(row)
            last_row = row                
    if not JSONFILEN:
        return
    
    with open(JSONFILEN, 'Urb') as io_file:
        jsondata = json.load(io_file)
    
    jsonbaddata = OrderedDict()
    not_in_jsondata = {'2': [], '3': []}
    for index in range(0, len(baddata), 2):
        rows = baddata[index:index+2]
        errors = False
        for row in rows:
            jkey = '{0}.{1}.{2}'.format( row['file'], row['first scan'], 
                                         row['last scan'] )
            if not jsondata.get(jkey, None):
                errors = True
                break
        #
        if not errors:
            for row in rows:
                jkey = '{0}.{1}.{2}'.format( row['file'], row['first scan'], 
                                             row['last scan'] )
                jsonbaddata.update( {jkey: jsondata[jkey]} )
        else:
            for row in rows:
                not_in_jsondata[row['MS']].append(row)
    
    with open(JSONBADDATA_FILEN, 'wb') as io_file:
        json.dump(jsonbaddata, io_file, indent=True)
    
    with open(MGFBADDATA_FILEN, 'wb') as io_file:
        # Information header:
        io_file.write('# Generated from Integrator JSON file {0} on {1}\n'
                      '#  http://www.lymphos.org\n\n'\
                      '# (cc) BY-NC-SA '\
                      '( http://creativecommons.org/licenses/by-nc-sa/3.0/'\
                      ' )\n\n'\
                      'MASS=Monoisotopic\n'.format(JSONFILEN, 
                                                   datetime.now().ctime())
                      )
        for jkey, jvalue in jsonbaddata.items():
            # Spectrum begining and meta-data header:
            charge = int(jvalue['consensus_z'])
            if charge < 0:
                charge = str(abs(charge)) + '-'
            else:
                charge = str(charge) + '+'
            _, scan_i, scan_f = jkey.split('.')
            if scan_i == scan_f:
                scans = str(scan_i)
            else:
                scans = '{0}-{1}'.format(scan_i, scan_f)
            io_file.write('BEGIN IONS\n'\
                          'TITLE=Spectrum MS{0} Id {1} scans: {4}\n'\
                          'PEPMASS={2:.3f}\n'\
                          'CHARGE={3}\n'\
                          'SCANS={4}\n'.format( 
                             jvalue['ms'], jkey, 
                             float(jvalue['spectral_info']['parent_mass']),
                             charge, scans)
                          )
            # Spectrum mz/intensity ion pairs:
            mzs = jvalue['spectral_info']['mass_array'].split('|')
            intensities = jvalue['spectral_info']['intensity_array'].split('|')
            for mz, intensity in zip(mzs, intensities):
                io_file.write('{0} {1}\n'.format(mz, intensity))
            # Spectrum ending:
            io_file.write('END IONS\n\n')
    
    for ms, ms_baddata in not_in_jsondata.items():
        with open(NOT_IN_JSON_F_TMPLT.format(ms),'wb') as io_file:
            csvdw = csv.DictWriter(io_file, fieldnames=sorted(last_row.keys()), 
                                   delimiter='\t')
            csvdw.writeheader()
            for row in ms_baddata:
                csvdw.writerow(row)
    return


#===============================================================================
# Main program control
#===============================================================================
if __name__ == '__main__':
    main()  