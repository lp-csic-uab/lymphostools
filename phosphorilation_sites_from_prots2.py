#!/usr/local/bin/python2.7
# encoding: utf-8
"""
:synopsis: Calculates the number of different phospho-site sequences from the
proteins and their associated phospho-peptides in a LymPHOS filtered DataBase.
It can also write a CSV file containing those phospho-site sequences along with
their maximum Ascore, and the phospho-peptides supporting it.

It defines :func:`get_Psite_seqs_from_Ppeptides`, :func:`regex_format` 
and :func:`get_phosphosites_from_prots`

:created:    2013/11/18

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB. All rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.2
"""

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
from __future__ import division

import sys
import os

# LymPHOS Configuration Paths:
if os.name == 'posix':
    LYMPHOS_BASE_PATH = r'/mnt/Dades/Laboratorio Proteomica CSIC-UAB/Programas/LymPHOS Web/src'
elif os.name == 'nt':
    LYMPHOS_BASE_PATH = r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src'
sys.path.append(LYMPHOS_BASE_PATH)
LYMPHOS_SRC_PATH = os.path.join(LYMPHOS_BASE_PATH, 'LymPHOS_v1_5')
sys.path.append(LYMPHOS_SRC_PATH)

# LymPHOS Environment:
import django
if django.get_version() < '1.5':
    from django.core import management
    import LymPHOS_v1_5.settings as LymPHOS_settings
    management.setup_environ(LymPHOS_settings)
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'LymPHOS_v1_5.settings'
    from django.conf import settings as LymPHOS_settings

from LymPHOS_v1_5.lymphos import models
from collections import defaultdict, namedtuple

import re


#===============================================================================
# Class definitions
#===============================================================================
PSiteData = namedtuple('PSiteData', ['proteins', 'peptides'])


ProteinPSiteData = namedtuple('ProteinPSiteData', ['protein', 'ac', 'position', 
                                                   'aa'])


PeptidePSiteData = namedtuple('PeptidePSiteData', ['peptide', 'seq', 'ascore'])


#===============================================================================
# Function definitions
#===============================================================================
def get_Psite_seqs_from_Ppeptides(peptides, protein, prot_seq=None, extend=7, 
                                  phospho_sites=None):
    """
    Extend by ``extend`` amino acids the C- and N-terms of each phospho-site
    found in the ``protein.seq`` from the peptide sequence in ``peptides``
    iterable

    Returns a ``phospho_sites`` defaultdict:: 
      {phospo_site_seq: ( set(ProteinPSiteData), set(PeptidePSiteData) ), ...}
    Also allows direct modification of a supplied ``phospho_sites`` defaultdict
    (acts like a ByRef parameter)::
      defaultdict( lambda: PSiteData( set(), set() ) )

    Highly Modified from :func:``enlarge_sequences`` in :mod:``tools.phoscripts.kinase_motifs``
    """
    if not prot_seq:
        prot_seq = protein.seq
    extreme = '-' * extend
    prot_seq = extreme + prot_seq + extreme
    
    if not isinstance(phospho_sites, defaultdict): #Allows direct modification of a suplied phospho_sites defaultdict
        phospho_sites = defaultdict( lambda: PSiteData( set(), set() ) )

    for pep in peptides:
        pep_seq = pep.peptide
        reges_sequence = regex_format(pep_seq)
        found = False
        for match in re.finditer(reges_sequence, prot_seq):
            found = True
            for pep_psite, pep_ascore in zip(pep.psites, pep.ascores):
                start = match.start() + pep_psite
                tail = prot_seq[ start-extend : start ]
                head = prot_seq[ start+1 : start+1+extend ]
                sec = tail + pep_seq[pep_psite] + head
                phospho_sites[sec].proteins.add( ProteinPSiteData(protein, 
                                                                  protein.ac, 
                                                                  start, 
                                                                  prot_seq[pep_psite]) )
                phospho_sites[sec].peptides.add( PeptidePSiteData(pep, pep_seq, 
                                                                  pep_ascore) )
        #
        if not found:
            print 'not found {0}'.format(pep_seq)
            get_Psite_seqs_from_Ppeptides([pep], protein, pep_seq.upper(), 
                                          extend, phospho_sites)

    return phospho_sites


def regex_format(pep_seq, regex_replace=None):
    """
    Formating of ``pep_seq`` string according to a ``regex_replace`` dictionary 
    ( {aa: regex} ).
    Ex.: AAABAAXAAI -> AAA[BND]AA.AA[IJ]
    """
    if not regex_replace:
        regex_replace = {'B': '[BND]',
                         'Z': '[ZEQ]',
                         'J': '[JIL]',
                         'X': '[.]',
                         'I': '[IJ]',
                         'L': '[LJ]',
                         'Other': '{0}',
#                         'Global': '{0}', #Return the resulting regex, without using a lookahead assertion to accelerate searches
                         'Global': '(?={0})', #Add a lookahead assertion (allows overlaping matches, but those matches are void: X-NOTE: http://stackoverflow.com/a/11430936) and return the regex
                         }
    regex_pep_seq = list()
    rkeys = regex_replace.keys()
    pep_seq = pep_seq.upper()
    for aa in list(pep_seq):
        if aa in rkeys:
            aa = regex_replace[aa]
        else:
            aa = regex_replace['Other'].format(aa)
        regex_pep_seq.append(aa)
    return regex_replace['Global'].format(''.join(regex_pep_seq))


def get_phosphosites_from_prots(prots, outfile=None, sides=7):
    """
    Build and return a ``phospho_sites`` dictionary of phospho-site sequences
    and phospho- site data found in proteins.
    
    If ``outfile`` is supplied, the ``phospho_sites`` dictionary data is saved
    to a CSV file containing phospho-site sequences along with their maximum
    Ascore, and the phospho-peptides supporting it.
    """
    phospho_sites = defaultdict( lambda: PSiteData( set(), set() ) )
    for prot in prots:
        get_Psite_seqs_from_Ppeptides(prot.phosphopeptides, prot, extend=sides, 
                                      phospho_sites=phospho_sites) #X-NOTE: call for in-situ modification of phospho_sites dictionary
    
    if outfile:
        # Write phospho_sites data to a CSV file:
        data = []
        for psite_seq, psite_data in sorted(phospho_sites.iteritems()):
            peptide_seqs = set()
            psite_ascores = list()
            for psite_pep in psite_data.peptides:
                peptide_seqs.add(psite_pep.seq)
                psite_ascores.append(psite_pep.ascore)
            line = '{0}\t{1}\t{2}'.format(psite_seq, 
                                          max(psite_ascores), 
                                          '\t'.join(peptide_seqs))
            data.append(line)
        text = '\n'.join(data)
        with open(outfile, 'w') as io_file:
            io_file.write('p-site seq\tMax. Ascore\tPeptides\n') #Header row
            io_file.write(text) #Data rows
    
    return phospho_sites


def main():
    test_prots = models.Proteins.objects.filter(is_decoy=False, phospho=True)
    print( len(get_phosphosites_from_prots(test_prots, 
                                           outfile='/home/oga/psites.test.2.txt')) 
          )


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    main()
