#!/usr/local/bin/python2.7
# encoding: utf-8
"""
add_tmt_labeling_to_sequest_search -- Adds TMT UniMod Code (737) to MS3 peptides in SequestGui output files

:synopsis: add_tmt_labeling_to_sequest_search is script that adds TMT UniMod Code (737) to MS3 peptides in SequestGui output files

:created:    2013-11-18

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.2
:updated:    2014-01-13
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import print_function

import sys
import os
import shutil
import csv
import re
import glob
from importlib import import_module

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

# Check for launching the easygui interface or the console one:
if os.name == 'posix' and not os.environ.get('DISPLAY', None):
    GUI = False
else:
    try:
        import easygui
        GUI = True
    except ImportError:
        GUI = False


#===============================================================================
# Global variables
#===============================================================================
__version__ = 0.2
__date__ = '2013-11-08'
__updated__ = '2014-01-13'

DEBUG = True
TESTRUN = False
PROFILE = False


#===============================================================================
# Class definitions
#===============================================================================
class CLIError(Exception):
    """Generic exception to raise and log different fatal errors."""
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = 'E: %s' % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg


class UIDialogs(object):
    """
    Abstract class for User Interface Dialogs
    """
    def gui_toolkit_suport(self, guitoolkit_name):
        """
        Test if the system has GUI support for the given GUI Toolkit name
        (guitoolkit_name).
        """
        if os.name == 'posix' and not os.environ.get('DISPLAY', None):
            self.gui_module = None
        else:
            try:
                self.gui_module = import_module(guitoolkit_name)
            except ImportError:
                self.gui_module = None
        return self.gui_module
    
    def print(self, *args, **kwargs):
        return self.msgbox( *args, **kwargs)
    
    def msgbox(self, text, title=None, *args, **kwargs):
        raise NotImplemented

    def inputbox(self, text='', title=None, default=None, *args, **kwargs):
        raise NotImplemented

    def selectfiles(self, msg='', title=None, default=None, folder=None, 
                    recursive=False, followlinks=False, include_re=None, 
                    exclude_re=None):
        raise NotImplemented

    def selectfolder(self):
        raise NotImplemented


class CLIDialogs(UIDialogs):
    """
    Command Line Interface Dialogs
    """
    print = globals()['__builtins__'].print

    def gui_toolkit_suport(self, guitoolkit_name):
        self.gui_module = None
        return self.gui_module
    
    @staticmethod
    def msgbox(text, title=None, *args, **kwargs):
        if title:
            text = '{0} - {1}'.format(title, text)
        
        print( '{0}\n'.format(text) )
        
        return
    
    @staticmethod
    def inputbox(text='', title=None, default=None, *args, **kwargs):
        if title:
            text = '{0} - {1}'.format(title, text)
        if default:
            text = '{0} [{1}]'.format(text, default)
            
        return raw_input( '{0} :'.format(text) )
    
    @staticmethod
    def selectfiles(msg='', title=None, default=None, folder=None, 
                    recursive=False, followlinks=False, include_re=None, 
                    exclude_re=None):
        if not folder:
            folder = os.getcwd()
        file_choices = get_list_files((folder,), recursive, followlinks)
        file_choices = filter_strings_by_re(file_choices, include_re, exclude_re)
        print('Files in {0}'.format(folder))
        print('\n  '.join(file_choices))
        selected_files = cli_input(msg, title, file_choices)
        # selected_files treatment:
        selected_files = selected_files.strip().split(' ')
        selected_files = [os.path.join(folder, filen) for filen in selected_files]
        selected_files = get_list_files(selected_files, recursive, followlinks)
        selected_files = filter_strings_by_re(selected_files, include_re, exclude_re)
        return selected_files
        

class EasyGUIDialogs(UIDialogs):
    """
    Graphic User Interface Dialogs
    """
    def __init__(self):
        self.cli_module = CLIDialogs
        self.gui_toolkit_suport('easygui')
        
        if self.gui_module:
            self.turn_on_gui()
        else:
            self.turn_off_gui()
    
    def _selectfiles(self, msg='', title=None, default=None, folder=None, #REFACTORIZE
                     recursive=False, followlinks=False, include_re=None, 
                     exclude_re=None):
        if not folder:
            folder = os.getcwd()
        file_choices = get_list_files((folder,), recursive, followlinks)
        file_choices = filter_strings_by_re(file_choices, include_re, exclude_re)
        selected_files = self.gui_module.multchoicebox(msg, title, file_choices)
        return selected_files
    
    def _print(self, value, sep=' ', end='\n', file=None, *args):
        args = (value,) + args
        msg = sep.join(args) + end
        title = 'Information'
        if file and file is not sys.stdout:
            self.cli_module.print(*args, sep=sep, end=end, file=file)
        return self.gui_module.msgbox(msg, title)
    
    def gui(self, turn_on):
        if turn_on and self.gui_module:
            self.msgbox = self.gui_module.msgbox
            self.inputbox = self.gui_module.enterbox
            self.selectfolder = self.gui_module.diropenbox
            self.selectfiles = self._selectfiles
            self.print = self._print
        else:
            self.msgbox = self.cli_module.msgbox
            self.inputbox = self.cli_module.inputbox
            self.selectfolder = self.cli_module.inputbox
            self.selectfiles = self.cli_module.selectfiles
            self.print = self.cli_module.print
        return self
    
    def turn_off_gui(self):
        return self.gui(False)
    
    def turn_on_gui(self):
        return self.gui(True)
    
    def gui_and_cli_print(self, *args, **kwargs):
        self.cli_module.print(*args, **kwargs)
        return self._print(*args, **kwargs)

    
#===============================================================================
# Function definitions
#===============================================================================
def add_tmt_to_aa(peptide, pos):
    if pos == len(peptide) - 1:
        peptide = peptide + '(737)'
    elif peptide[pos+1] == '(':
        mods = peptide[pos+2:peptide.find(')',pos)].split(',')
        if '737' not in mods:
            peptide = peptide[:pos+1] + '(737,' + peptide[pos+2:]
    else:
        peptide = peptide[:pos+1] + '(737)' + peptide[pos+1:]
    #
    return peptide

def add_tmt_to_file(infilen, outfilen=None):
    if not outfilen or outfilen == infilen:
        outfilen = infilen + '.tmp'
        tmp_file = True
    else:
        tmp_file = False
    #
    with open(infilen, 'rb') as i_file:
        with open(outfilen, 'wb') as o_file:
            csv_infile = csv.DictReader(i_file, delimiter='\t')
            realfieldnames = csv_infile.fieldnames
            csv_outfile = csv.DictWriter(o_file, realfieldnames + ['FIX'],      #X-NOTE: Changed to produce the same bad-formed output as SequestGui (with an empty extra column)
                                         restval='', extrasaction='ignore',     #
                                         delimiter='\t')
            csv_outfile.writer.writerow(realfieldnames)                         #X-NOTE: Changed to produce the same bad-formed output as SequestGui (with an empty extra column, but not for the headers)
            for psm in csv_infile:
                psm.pop(None, '')                                               #X-NOTE: Remove the empty extra column without header in SequestGui TSV file (bad-formed) 
                if int(psm['ms_level']) == 3:
                    pep = add_tmt_to_aa(psm['peptide'], 0)
                    k_pos = pep.find('K', 1)
                    while k_pos > 0:
                        pep = add_tmt_to_aa(pep, k_pos)
                        k_pos = pep.find('K', k_pos+1)
                    psm['peptide'] = pep
                csv_outfile.writerow(psm)
    #
    if tmp_file:
        shutil.copy2(infilen, infilen + '.OLD.bak')
        shutil.copy2(outfilen, infilen)
        os.remove(outfilen)
    return


def cli_output(text, title=None, *args, **kwargs):
    if title:
        text = '{0} - {1}'.format(title, text)
    
    print( '{0}\n'.format(text) )
    
    return


def cli_input(text='', title=None, default=None, *args, **kwargs):
    if title:
        text = '{0} - {1}'.format(title, text)
    if default:
        text = '{0} [{1}]'.format(text, default)
        
    return raw_input( '{0} :'.format(text) )


def gui_multiselect_files(msg='', title=None, default=None, folder=None, #REFACTORIZE
                          recursive=False, followlinks=False, 
                          include_re=None, exclude_re=None):
    if not folder:
        folder = os.getcwd()
    file_choices = get_list_files((folder,), recursive, followlinks)
    file_choices = filter_strings_by_re(file_choices, include_re, exclude_re)
    selected_files = easygui.multchoicebox(msg, title, file_choices)
    return selected_files


def cli_multiselect_files(msg='', title=None, default=None, folder=None, #REFACTORIZE
                          recursive=False, followlinks=False, 
                          include_re=None, exclude_re=None):
    if not folder:
        folder = os.getcwd()
    file_choices = get_list_files((folder,), recursive, followlinks)
    file_choices = filter_strings_by_re(file_choices, include_re, exclude_re)
    print('Files in {0}'.format(folder))
    print('\n  '.join(file_choices))
    selected_files = cli_input(msg, title, file_choices)
    # selected_files treatment:
    selected_files = selected_files.strip().split(' ')
    selected_files = [os.path.join(folder, filen) for filen in selected_files]
    selected_files = get_list_files(selected_files, recursive, followlinks)
    selected_files = filter_strings_by_re(selected_files, include_re, exclude_re)
    return selected_files


def gui_print(value, sep=' ', end='\n', file=None, *args):
    args = (value,) + args
    msg = sep.join(args) + end
    title = 'Information'
    if file and file is not sys.stdout:
        old_print(*args, sep=sep, end=end, file=file)
    return easygui.msgbox(msg, title)


def gui_and_cli_print(*args, **kwargs):
    old_print(*args, **kwargs)
    return gui_print(*args, **kwargs)


def get_folder_files(folder, recursive=False, followlinks=False):
    """
    Get all the file names inside a folder.
    
    If recursive = True, files in subfolders will be also gathered.
    If followlinks = False, files or folders symlinks will be filtered out.
    """
    folder_files = list()
    if not recursive:
        # Add only files with path to folder_files, taking into account
        # followlinks:
        for filen in os.listdir(folder):
            filen = os.path.join(folder, filen)
            if not os.path.isdir(filen) and (followlinks or 
                                             not os.path.islink(filen)):
                folder_files.append(filen)
    else:
        for rootfolder, subfolders, files in os.walk(folder):
            if not followlinks:
                # In-situ elimination of symlinks from subfolders, to avoid
                # os.walk to traverse them:
                for subfolder in reversed(subfolders):
                    if os.path.islink(subfolder):
                        subfolders.remove(subfolder)
            # Add files with path to folder_files, taking into account
            # followlinks:
            for filen in files:
                filen = os.path.join(rootfolder, filen)
                if followlinks or not os.path.islink(filen):
                    folder_files.append(filen)
    return folder_files


def get_list_files(filesandfolders, recursive=False, followlinks=False):
    """
    Returns a list of file names from a list of globs, files and folders
    (filesandfolders), gathering the files inside each of the folders.
    
    If recursive = True, files all in subfolders will be also gathered. 
    If followlinks = False, files or folders symlinks will be filtered out.
    """
    files = list()
    for fileorfolder in filesandfolders:
        if not os.path.exists(fileorfolder): #Globs and non existing files/folders
            if '*' in fileorfolder or '?' in fileorfolder or '[' in fileorfolder: #Globs
                glob_files = glob.glob(fileorfolder)
                glob_files = get_list_files(glob_files, recursive, followlinks)
                files.extend(glob_files)
            else: #Filter non existing files/folders
                continue
        if not followlinks and os.path.islink(fileorfolder): #Filter links
            continue
        if os.path.isdir(fileorfolder): #Folder
            folder_files = get_folder_files(fileorfolder, recursive, followlinks)
            files.extend(folder_files)
        else: #File
            files.append(fileorfolder)
    return files


def filter_strings_by_re(strings, include_re=None, exclude_re=None):
    """
    Filter a list of strings according to regex in excluded regex list
    (exclude_re) and/or included regex list (include_re)
    """
    filtered_strings = strings[:]
    # Discard strings matching a regex in excluded regex list:
    if exclude_re:
        for ex_re in exclude_re:
            filtered_strings = [each for each in filtered_strings 
                                if not re.search(ex_re, each)]
    # Discar strings not matching a regex in included regex list:
    if include_re:
        for in_re in include_re:
            filtered_strings = [each for each in filtered_strings 
                                if re.search(in_re, each)]        
    #
    return filtered_strings


def main(*argv):
    """Command line options."""

    if argv:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = 'v%s' % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split('\n')[1]
    program_license = '''%s

  Created by Óscar Gallardo on %s.
  Copyright 2013 LP-CSIC/UAB. All rights reserved.

  Licensed under the GPLv3
  https://www.gnu.org/licenses/gpl-3.0.html

  Distributed on an 'AS IS' basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set verbosity level [default: %(default)s]')
    parser.add_argument('-R', '--recursive', dest='recursive', action='store_true', help='recursively search filesandfolders in sub-folders' )
    parser.add_argument('-i', '--interactive', dest='interactive', action='store_true', help='ask for missing parameters' )
    parser.add_argument('-inc', '--include', dest='include_re', help='only include filesandfolders matching this regex pattern from supplied folders. Note: exclude is given preference over include. [default: %(default)s]', metavar='RE' )
    parser.add_argument('-exc', '--exclude', dest='exclude_re', help='exclude filesandfolders matching this regex pattern from supplied folders. [default: %(default)s]', metavar='RE' )
    parser.add_argument('-V', '--version', action='version', version=program_version_message)
    parser.add_argument('--force-cli', dest='force_cli', action='store_true', help='force command line interface (no GUI)' )
    parser.add_argument(dest='filesandfolders', help='file(s) or folder(s) to process [default: %(default)s]', metavar='file or folder', nargs='*')
    
    try:
        # Process arguments
        args = parser.parse_args()
        
        recursive = args.recursive
        interactive = args.interactive
        verbose = args.verbose
        include_re = args.include_re
        exclude_re = args.exclude_re
        force_cli = args.force_cli
        filesandfolders = args.filesandfolders
        
        if include_re:
            include_re = ('^.*sequest_ms3.*\.xls$', include_re)
        else:
            include_re = ('^.*sequest_ms3.*\.xls$', )
        if exclude_re:
            exclude_re = (exclude_re, )
        else:
            exclude_re = tuple()
        
        if verbose > 0:
            cfg_msg = ['Verbose mode On']
            if recursive:
                cfg_msg.append('Recursive mode for folders On')
            if interactive:
                cfg_msg.append('Interactive mode On')
                
            print( '\n'.join(cfg_msg) )
        
        if include_re and exclude_re and include_re == exclude_re:
            raise CLIError('include and exclude pattern are equal! Nothing will be processed.')
        
        if not filesandfolders and (GUI or interactive):
            chosenfolder = selectfolder('Witch Folder contains the files to modify?', 
                                        'Select a Folder', 
                                        default=os.getcwd())
            files = selectfiles('Witch Files do you want to modify from chosen folder?', 
                                 'Select Files', 
                                 default='*.xls', folder=chosenfolder, 
                                 recursive=recursive, 
                                 include_re=include_re, 
                                 exclude_re=exclude_re)
        else:
            files = get_list_files(filesandfolders, recursive)
            files = filter_strings_by_re(files, include_re, exclude_re)
        
        if not files: #No files from filesandfolders or interactively selected -> give usage help:
            if verbose > 0: print('No files or folders supplied for processing!!\n')
            sys.argv.append('-h')
            parser.parse_args()
        
        for infile in files:
            if os.path.exists(infile) and os.path.isfile(infile):
                if verbose > 0:
                    old_print('Processing file:\n  {0}'.format(infile))
                add_tmt_to_file(infile)
            else:
                print('File {0} does not exists!'.format(infile))
        #
        print('Job finished')
        return 0
    except (KeyboardInterrupt, SystemExit):
        ### Handle keyboard interrupt, and system exit (for example when -h)
        return 0
#    except Exception as e:
#        if DEBUG or TESTRUN:
#            raise(e)
#        indent = len(program_name) * ' '
#        sys.stderr.write(program_name + ': ' + repr(e) + '\n')
#        sys.stderr.write(indent + '  for help use --help')
#        return 2


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    if DEBUG:
        sys.argv.append('-v')
        sys.argv.append('-R')
        sys.argv.append('-i')
        sys.argv.append('--force-cli')
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'add_tmt_labeling_to_sequest_search_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open('profile_stats.txt', 'wb')
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    #
    if GUI:
        msgbox = easygui.msgbox
        inputbox = easygui.enterbox
        selectfiles = gui_multiselect_files
        selectfolder = easygui.diropenbox
        old_print = print
        print = __builtins__.print = gui_and_cli_print
    else:
        msgbox = cli_output
        selectfiles = cli_multiselect_files
        inputbox = selectfolder = cli_input
    #
    exit_code = main()
    #
    if GUI:
        print = __builtins__.print = old_print
    #
    sys.exit(exit_code)