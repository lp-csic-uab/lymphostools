# -*- mode: python -*-
a = Analysis(['add_tmt_labeling_to_sequest_search.py'],
             pathex=['F:\\LymPHOS Tools'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='add_tmt_labeling_to_sequest_search.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True , icon='add_tmt_labeling_to_sequest_search.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='add_tmt_labeling_to_sequest_search')
