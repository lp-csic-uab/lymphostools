#!/usr/bin/env python
# -*- coding: utf8 -*-
#
"""
:synopsis: recreate_lymphosdb_allpep.py is a script that Recreates a LymPHOS
DataBase (the one defined in DJango environment configuration) according to a
supplied SQL schema and the corresponding data files (see Global variables
section). This version imports **ALL** the peptides in the input data files
(both phosphorilated and not phosphorilated).

:created:    2013/12/09

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB. All rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.3
:updated:    2014-10-16
"""

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
import sys
import os

# LymPHOS Configuration Paths:
if os.name == 'posix':
    LYMPHOS_BASE_PATH = r'/mnt/Dades/Laboratorio Proteomica CSIC-UAB/Programas/LymPHOS Web/src'
elif os.name == 'nt':
    LYMPHOS_BASE_PATH = r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src'
sys.path.append(LYMPHOS_BASE_PATH)
LYMPHOS_SRC_PATH = os.path.join(LYMPHOS_BASE_PATH, 'LymPHOS_v1_5')
sys.path.append(LYMPHOS_SRC_PATH)

# LymPHOS Environment:
import django
if django.get_version() < '1.5':
    from django.core import management
    import LymPHOS_v1_5.settings as LymPHOS_settings
    management.setup_environ(LymPHOS_settings)
else: #Changes from django version 1.5 :
    os.environ['DJANGO_SETTINGS_MODULE'] = 'LymPHOS_v1_5.settings'
    from django.conf import settings as LymPHOS_settings
    if django.get_version() >= '1.7': #Changes from django version 1.7 :
        from django.core.wsgi import get_wsgi_application
        application = get_wsgi_application()


from django.db import connection, transaction
connection.use_debug_cursor = False
from lymphos import logic, filters
import datetime
from collections import defaultdict


#===============================================================================
# Global variables
#===============================================================================
# Data files path (PATH) and SQL files (SQL_CREATEDB_FILE, SQL_INIT_FILE) to
# make a new database:
SQL_CREATEDB_FILE = os.path.join(LYMPHOS_SRC_PATH, LymPHOS_settings.DB_NAME + '_django.sql').replace('\\','/')
SQL_INIT_FILE = os.path.join(LYMPHOS_SRC_PATH, LymPHOS_settings.DB_NAME + '_init.sql').replace('\\','/')
#  - For LymPHOS: 
if LymPHOS_settings.USE_PROFILE == 'NORMAL':
    PATH = os.path.join(LYMPHOS_BASE_PATH, '..', 'DataFiles/Imported into LymPHOS DB/').replace('\\', '/')
#  - For LymPHOS JM:
elif LymPHOS_settings.USE_PROFILE == 'JM':
    PATH = os.path.join(LYMPHOS_BASE_PATH, '..', 'DataFiles/Imported into LymPHOS JM DB/').replace('\\', '/')

# Valid Extensions, in the right order, for the data files to import:
#       Currently supported Extensions:
#       - Conditions JSON file must have a .DB extension.
#       - JSON data files from Integrator must be in .ZIP archives, or have a .IDB 
#         extension.
#       - PQuantifier JSON quantification file must have a .JSON extension.
VALID_EXT = ('DB', 'ZIP', 'IDB', 'JSON')

# IMPORTERS dictionary structure:
#     IMPORTERS = {'EXTENSION': (ConverterClass, {'init_parameter': value})}
IMPORTERS = {'DB':  (filters.JsonCondConverter, {}),
             'ZIP': (filters.JsonIntegratorConverter, {'only_phospho': False, 
                                                       'with_decoys': True}),
             'IDB': (filters.JsonIntegratorConverter, {'only_phospho': False, 
                                                       'with_decoys': True}),
             'JSON': (filters.JsonQuantConverter, {}),
             }

# Decides if only import data or also recreate the database:
RECREATE_DB = True #False: only import data. True: recreate database and import data. 
if not RECREATE_DB:
    VALID_EXT = VALID_EXT[1:] #Usually don't import conditions data if we will not recreate the database


#===============================================================================
# Function definitions
#===============================================================================
def sql2database(sql_filename, connection=connection):
    """
    Sends a SQL script (``sql_filename``) to the active database connection.
    """
    
    cursor = connection.cursor()
    with open(sql_filename, 'rU') as io_file:
        sql_instruction = list() #List of lines from a multi-line SQL instruction
        for row in io_file:
            row = row.strip() #Strip leading and trailing spaces, '\n', ...
            if row and not row.startswith('--'): #Avoid SQL comments
                sql_instruction.append(row)
                if ';' in row: #End of SQL instruction
                    #Build the instruction in one line and execute it:
                    cursor.execute( ' '.join(sql_instruction) )
                    sql_instruction = list() #Prepare the list for a new multi-line SQL instruction
    transaction.commit_unless_managed()
    #
    return


def create_database(sql_create_db, sql_initial_data=None):
    """
    Empty and rebuild the DataBase from a SQL script (``sql_create_db``) and
    fill in initial data from another SQL script (``sql_initial_data``).
    """
    sql2database(sql_create_db)
    if sql_initial_data:
        sql2database(sql_initial_data)
    #
    return
    
@transaction.commit_on_success
def import_file(importer, imp_file, imp_zipped, log_key):
    """
    Import the supplied ``imp_file`` using the supplied ``importer`` object
    """
    
    log_msg = ''
    date = datetime.datetime.now()
    logs = defaultdict(list)
    importer.zipped = imp_zipped
    
    #Converter execution (load data & import data):
    importer.process_file(imp_file)
    #Errors check:
    imp_log_keys = importer.logger.keys()
    imp_error_keys = [each for each in imp_log_keys if 'Error' in each]
    if not imp_error_keys: #No importer logs 
        log_msg = '{0:%Y-%m-%d %H:%M} : '\
                  '{1} Imported into LymPHOS DataBase'.format(date, 
                                                              imp_file)
    else: #Some importer logs in file reading
        log_msg = '{0:%Y-%m-%d %H:%M} : '\
                  'Errors Reading uploaded file {1}'.format(date, 
                                                            imp_file)
        #Update the page self._errors with the importer logs
        logs.update(importer.logger())
    logs[log_key].append(log_msg)
    #If any not imported data is available, Log it to disk:         #DEBUG
    if 'UnsavedData' in imp_log_keys:
        n_datarecords = 0
        for index, dict_data in enumerate(importer.logger('UnsavedData')):
            n_datarecords += len(dict_data)
            log_fname = '{0}_{1}.log'.format(imp_file, index)
            with open(log_fname, 'w') as log_file:
                filters.json.dump(dict_data, log_file, indent=4)
        log_msg = '{0} Data record(s) not imported (see {1}_*.log '\
                  'for details)'.format(n_datarecords, imp_file)
        logs[log_key].append(log_msg)
        imp_log_keys.remove('UnsavedData')
    for il_key in imp_log_keys:
        log_msg = ', '.join( importer.logger(il_key) ) 
        logs[log_key].append(log_msg)
    del importer.logger.log
    #
    return logs


def rebuild_cache():
    """
    Rebuild the ``PeptideCache`` table
    """
    
    date = datetime.datetime.now()
    
    new_cache = logic.MySQLCache()
    new_cache.peptide_cache()
    log_msg = '{0:%Y-%m-%d %H:%M} : Changes applied. '\
              'PeptideCache Table Rebuilded'.format(date)
    return log_msg


def process_files(files):
    """ 
    Process & Import the ``files`` specified.
    These files must follow the next extension rules:
        - Conditions JSON file must have a .DB extension.
        - JSON data files from Integrator must be in .ZIP archives, or have a .IDB 
          extension.
        - PQuantifier JSON quantification file must have a .JSON extension.
    """
    # Filter the list of input files accordingly with the extensions allowed,
    # and convert it to a dictionary whose keys are the file extensions allowed,
    # so we can access files in the order they appear in VALID_EXT:
    ext_files = defaultdict(list)
    for filename in files:
        ext = filename.rpartition('.')[2].upper()
        if ext in VALID_EXT:
            ext_files[ext].append(filename)
    # Load & Import each input file, in the order they appear in VALID_EXT,
    # using the right importer filter:
    for ext in VALID_EXT:
        zipped = True if ext == 'ZIP' else False
        for filename in ext_files.get(ext, tuple()):
            print('Processing file:{1}  {0} ...{1}'.format(filename, os.linesep))
            ConverterClass, init_parameters = IMPORTERS[ext]
            importer = ConverterClass(**init_parameters) #Re-new importer
            #
            logs = import_file(importer, filename, zipped, ext)
            #
            print('  Logs: {0}.{1}'.format(logs, os.linesep))
            del importer #Discard previous importer to free memory
    #
    return



#===============================================================================
# Main program control
#===============================================================================
if __name__ == '__main__':
    # Process&Import the files specified in sys.argv or all files in PATH. These
    # files must follow the next extension rules:
    #     - Conditions JSON file must have a .DB extension.
    #     - JSON data files from Integrator must be in .ZIP archives, or have a .IDB extension.
    #     - PQuantifier JSON quantification file must have a .JSON extension.
    
    # Get files to import:
    files = None
    if len(sys.argv) > 1:
        files = sys.argv[1:]
    else:
        files = [PATH + each for each in os.listdir(PATH)]
    #
    if files:
        # Empty and rebuild the DataBase from a SQL script:
        if RECREATE_DB:
            print('Creating database from {0}{1}'.format(SQL_CREATEDB_FILE, os.linesep))
            create_database(SQL_CREATEDB_FILE, SQL_INIT_FILE)
        #
        # Import & Process the data files:
        process_files(files)
        #
        # Rebuild DataBase cache table:
        print('Rebuilding PeptideCache database table...{0}'.format(os.linesep))
        log_msg = rebuild_cache()
        print('  Logs: {0}{1}'.format(log_msg, os.linesep))
        #
        print('End of program processing.{0}'.format(os.linesep))
    else:
        print('Sorry, no files to import. Aborting... {0}'.format(os.linesep))
