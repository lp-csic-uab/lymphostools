#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
:synopsis: up_bigxml2smalls.py is a script that splits big FASTA and XML files
downloaded from UniProt.org into individual protein files, so they can be 
opened and processed by :class:`LymPHOS_v1_5.lymphos.filters.UniProtXmlConverter` 
(from project LymPHOS 2) and :class:`proteomics.filters.UniProtRepositoryFetcher` 
(from project LymPHOS-UB-AC / CrossTalk).

:created:    2012/05/02

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '1.0'
__UPDATED__ = '2015-04-18'

#===============================================================================
# Imports
#===============================================================================
import os
import xml.etree.ElementTree
from multiprocessing import Process, cpu_count, freeze_support


#===============================================================================
# Global variables
#===============================================================================
IN_PATH = '/home/oga/Laboratori de Proteomica CSIC-UAB/Bases de Dades/UniProtKB/Human reference proteome - UP000005640 rev.02-2016 - 2016-04-18/'.replace('\\','/')
IN_XML_FILEN = 'Homo sapiens (Human) reference proteome - rev.02-2016 - 2016-04-18.xml'
IN_FASTA_FILEN = 'Homo sapiens (Human) reference proteome - rev.02-2016 - 2016-04-18.fasta'


#===============================================================================
# Function definitions
#===============================================================================
def split_uniprot_xml(input_path, input_filen, output_path = None):
    """
    Split a UniProt XML file (input_path, input_filen) in small XML files, each
    containing only one protein, and using as name the AC of that protein.
    """
    if not output_path:
        output_path = os.path.join(input_path, 'splitted')
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    full_input_filen = os.path.join(input_path, input_filen)
    #
    nprots = 0
    #
    print( 'Start processing of file: {0}'.format(full_input_filen) )
    with open(full_input_filen, 'r') as up_io:
        xml_head = up_io.readline()
        up_head = up_io.readline()
        up_tail = '</uniprot>\n'
        xml_ns = '{http://uniprot.org/uniprot}'
        up_io.seek(0)
        xparser = xml.etree.ElementTree.iterparse(up_io, events = ('start', 'end')) #Create a XML parser that stops at each tag start and end events
        event, xml_root = xparser.next() #Get a pointer to the xml root element ('{http://uniprot.org/uniprot}uniprot')
        # Listen to XML parsing events and check the parsed XML element for a
        # full protein entry:
        for event, xml_elem in xparser:
            if event == 'end' and xml_elem.tag == xml_ns + 'entry': #Finished parsing until the end of a protein entry
                out_filen = xml_elem.find(xml_ns + 'accession').text + '.xml'
                # Write the new protein entry to output file as a complete
                # UniProt XML file:
                with open( os.path.join(output_path, out_filen), 'w' ) as out_io:
                    out_io.write(xml_head)
                    out_io.write(up_head)
                    xml.etree.ElementTree.ElementTree(element=xml_elem).write(out_io)
                    out_io.write(up_tail)
                nprots += 1
                xml_root.clear() #Clear the root element to this current point to avoid memory increase
                print( ' Protein splitted and saved to {0}'.format(out_filen) )
    print( '{0} total proteins splitted and saved from {1}'.format(nprots, 
                                                                   input_filen) )
    #
    return nprots


def split_fasta(input_path, input_filen, output_path = None):
    """
    Split a FASTA file (input_path, input_filen) in small FASTA files, each containing
    only one protein, and using as name the AC of that protein.
    """
    if not output_path:
        output_path = os.path.join(input_path, 'splitted')
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    full_input_filen = os.path.join(input_path, input_filen)
    #
    file_slice = list()
    out_filen = ''
    nprots = 0
    #
    print( 'Start processing of file: {0}'.format(full_input_filen) )
    with open(full_input_filen, 'rU') as fasta_io:
        for row in fasta_io:
            row = row.strip()
            if row[0] == '>': #row contains a FASTA header (start of a protein)
                # Save previous protein, if some:
                if len(file_slice) > 1:
                    with open( os.path.join(output_path, out_filen), 'wb' ) as out_io:
                        out_io.write('\n'.join(file_slice))
                    nprots += 1
                    print( ' Protein splitted and saved to {0}'.format(out_filen) )
                # Start reading new protein (empty file slice and get new output
                # file name):
                file_slice = list()
                if 'decoy' in row:
                    out_filen = row[1:] + '.fasta'
                else:
                    out_filen = row.split('|')[1] + '.fasta'
            # Always put the new row in the current file slice:
            file_slice.append(row)
        if len(file_slice) > 1: #Get the last protein from FASTA file:
            with open( os.path.join(output_path, out_filen), 'w' ) as out_io:
                out_io.write('\n'.join(file_slice))
            nprots += 1
    print( '{0} total proteins splitted and saved from {1}'.format(nprots, 
                                                                   input_filen) )
    #
    return nprots


#===============================================================================
# Main program control
#===============================================================================
if __name__ == '__main__':
    freeze_support()
    #
    if cpu_count() > 1: #Take advantage of multi-core CPUs
        process1 = Process(target = split_uniprot_xml, args = (IN_PATH, IN_XML_FILEN))
        process1.start()
        process2 = Process(target = split_fasta, args = (IN_PATH, IN_FASTA_FILEN))
        process2.start()
        process1.join()
        process2.join()
    else:
        split_uniprot_xml(IN_PATH, IN_XML_FILEN)
        split_fasta(IN_PATH, IN_FASTA_FILEN)
    #
    print('Finished')
