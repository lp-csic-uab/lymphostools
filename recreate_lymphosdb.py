#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
'''
recreate_lymphosdb -- Recreates a LymPHOS DataBase (the one defined in DJango environment configuration) according to a supplied SQL schema and the corresponding data files (see Global variables section). This version imports ONLY the phosphorilated peptides in data files.

WARNING!! This script is DEPRECATED! Use recreate_lymphosdb_allpep.py instead!

recreate_lymphosdb_allpep.py is script that Recreates a LymPHOS DataBase (the
one defined in DJango environment configuration) according to a supplied SQL
schema and the corresponding data files (see Global variables section).
This version imports ONLY the phosphorilated peptides in data files.

It defines create_database(), process_files(), import_file() and rebuild_cache()

Created on 2012/05/04

@author:     �scar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
@copyright:  2013 LP-CSIC/UAB. All rights reserved.
@license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

@contact:    lp.csic@uab.cat

@version:    0.2
@deffield    updated: 2012-12-09
'''

#===============================================================================
# Imports and DJango enviroment configuration
#===============================================================================
import sys


###########
# WARNING!! This script is DEPRECATED! Use recreate_lymphosdb_allpep.py instead!
sys.exit('WARNING!! This script is DEPRECATED! Use recreate_lymphosdb_allpep.py instead!')
# WARNING!! This script is DEPRECATED! Use recreate_lymphosdb_allpep.py instead!
###########


sys.path.append(r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src')
sys.path.append(r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src\LymPHOS_v1_5')
from django.core import management
import LymPHOS_v1_5.settings
management.setup_environ(LymPHOS_v1_5.settings)
from django.db import connection, transaction
connection.use_debug_cursor = False
from lymphos import models, logic, filters
import datetime
import os
from collections import defaultdict


#===============================================================================
# Global variables
#===============================================================================
PATH = 'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\DataFiles\Imported into LymPHOS DB'.replace('\\', '/') + '/'
SQL_FILE = 'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src\LymPHOS_v1_5\lymphos2.2.sql'.replace('\\','/')


#===============================================================================
# Function definitions
#===============================================================================
def create_database(sql_filename):
    '''
    Empty and rebuild the DataBase from a SQL script:
    '''
    
    cursor = connection.cursor()
    with open(sql_filename, 'r') as io_file:
        sql_instruction = list()
        for row in io_file:
            row = row.strip() #Strip leading and trailing spaces, '\n', ...
            if row and not row.startswith('--'): #Avoid SQL comments
                sql_instruction.append(row)
                if ';' in row: #End of SQL instruction
                    #Build the instruction in one line and execute it:
                    cursor.execute(' '.join(sql_instruction))
                    sql_instruction = list()
    transaction.commit_unless_managed()
    #
    return


@transaction.commit_on_success
def import_file(importer, imp_file, imp_zipped, log_key):
    '''
    Import the supplied imp_file using the supplied importer object
    '''
    
    log_msg = ''
    date = datetime.datetime.now()
    logs = defaultdict(list)
    importer.zipped = imp_zipped
    
    #Converter execution (load data & import data):
    importer.process_file(imp_file)
    #Errors check:
    imp_log_keys = importer.logger.keys()
    imp_error_keys = [each for each in imp_log_keys if 'Error' in each]
    if not imp_error_keys: #No importer logs 
        log_msg = '{0:%Y-%m-%d %H:%M} : '\
                  '{1} Imported into LymPHOS DataBase'.format(date, 
                                                              imp_file)
    else: #Some importer logs in file reading
        log_msg = '{0:%Y-%m-%d %H:%M} : '\
                  'Errors Reading uploaded file {1}'.format(date, 
                                                            imp_file)
        #Update the page self._errors with the importer logs
        logs.update(importer.logger())
    logs[log_key].append(log_msg)
    #If any not imported data is available, Log it to disk:         #DEBUG
    if 'UnsavedData' in imp_log_keys:
        n_datarecords = 0
        for index, dict_data in enumerate(importer.logger('UnsavedData')):
            n_datarecords += len(dict_data)
            log_fname = '{0}_{1}.log'.format(imp_file, index)
            with open(log_fname, 'w') as log_file:
                filters.json.dump(dict_data, log_file, indent=4)
        log_msg = '{0} Data record(s) not imported (see {1}_*.log '\
                  'for details)'.format(n_datarecords, imp_file)
        logs[log_key].append(log_msg)
        imp_log_keys.remove('UnsavedData')
    for il_key in imp_log_keys:
        log_msg = ', '.join( self.importer.logger(il_key) ) 
        logs[log_key].append(log_msg)
    del importer.logger.log
    #
    return logs


def rebuild_cache():
    '''
    Rebuild the PeptideCache table
    '''
    
    date = datetime.datetime.now()
    
    new_cache = logic.MySQLCache()
    new_cache.peptide_cache()
    log_msg = '{0:%Y-%m-%d %H:%M} : Changes applied. '\
              'PeptideCache Table Rebuilded'.format(date)
    return log_msg


def process_files(files):
    ''' 
    Process&Import the files specified.
    These files must follow the next extension rules:
        - Conditions JSON file must have a .DB extension.
        - JSON data files from Integrator must be in .ZIP files, or have a .IDB extension.
        - PQuantifier JSON quantification file must have a .JSON extension.
    '''
    
    valid_ext = ('DB', 'ZIP', 'IDB', 'JSON')
    importers = {'DB': filters.JsonCondConverter,
                 'ZIP': filters.JsonIntegratorConverter,
                 'IDB': filters.JsonIntegratorConverter,
                 'JSON': filters.JsonQuantConverter,
                 }
    
    #Filter the list of input files accordingly with the extensions allowed,
    #and convert it to a dictionary whose keys are the file extensions allowed,
    #so we can access files in the order they appear in valid_ext:
    ext_files = defaultdict(list)
    for filename in files:
        ext = filename.rpartition('.')[2].upper()
        if ext in valid_ext:
            ext_files[ext].append(filename)
    #Load&Import each input file, in the order they appear in valid_ext, using
    #the right importer filter:
    for ext in valid_ext:
        zipped = True if ext == 'ZIP' else False
        for filename in ext_files.get(ext, tuple()):
            print('Processing file %s ...%s'%(filename, os.linesep))
            importer = importers[ext]() #Re-new importer 
            logs = import_file(importer, filename, zipped, ext)
            print('%s.%s'%(logs, os.linesep))
            del importer #Discard previous importer to liberate memory
    #
    return



#===============================================================================
# Main program control
#===============================================================================
if __name__ == '__main__':
    #Process&Import the files specified in sys.argv or all files in PATH. These
    #files must follow the next extension rules:
    #    - Conditions JSON file must have a .DB extension.
    #    - JSON data files from Integrator must be in .ZIP files, or have a .IDB extension.
    #    - PQuantifier JSON quantification file must have a .JSON extension.
    
    #Get files to import:
    files = None
    if len(sys.argv) > 1:
        if sys.argv[1].upper() == 'TEST':
            print('Entering TEST mode.%s'%os.linesep)
            files = ['test/file.ext']
        else:
            files = sys.argv[1:]
    else:
        files = [PATH+each for each in os.listdir(PATH)]
    #
    if files:
        #Empty and rebuild the DataBase from a SQL script:
        print('Creating database from:\n %s%s'%(SQL_FILE, os.linesep))
        create_database(SQL_FILE)
        #Import&Process the data files:
        process_files(files)
        #Rebuild DataBase cache table:
        log_msg = rebuild_cache()
        print('%s.%s'%(log_msg, os.linesep))
        #
        print('End of program processing.%s'%os.linesep)
    else:
        print('Sorry, no files to import. Aborting... %s'.format(os.linesep))
