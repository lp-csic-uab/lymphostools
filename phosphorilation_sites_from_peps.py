#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Created on 2013/11/15

@authors: Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
@version: 0.1
'''

#===============================================================================
# Imports and DJango environment configuration
#===============================================================================
from __future__ import division

import sys
import os

# LymPHOS Configuration Paths:
if os.name == 'posix':
    LYMPHOS_BASE_PATH = r'/mnt/Dades/Laboratorio Proteomica CSIC-UAB/Programas/LymPHOS Web/src'
elif os.name == 'nt':
    LYMPHOS_BASE_PATH = r'D:\Oscar\Mis documentos\Laboratorio Proteomica CSIC-UAB\Programas\LymPHOS Web\src'
sys.path.append(LYMPHOS_BASE_PATH)
LYMPHOS_SRC_PATH = os.path.join(LYMPHOS_BASE_PATH, 'LymPHOS_v1_5')
sys.path.append(LYMPHOS_SRC_PATH)

# LymPHOS Environment:
import django
if django.get_version() < '1.5':
    from django.core import management
    import LymPHOS_v1_5.settings as LymPHOS_settings
    management.setup_environ(LymPHOS_settings)
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'LymPHOS_v1_5.settings'
    from django.conf import settings as LymPHOS_settings

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum, Count

from LymPHOS_v1_5.lymphos import models

from collections import defaultdict, namedtuple
import re
import random

#===============================================================================
# Class definitions
#===============================================================================
PepData = namedtuple('PepData', ['peptide', 'match_start'])


#===============================================================================
# Function definitions
#===============================================================================
def regex_format(pep_seq):
    '''
    Formating of pep_seq string according to a regex_replace dictionary ( {aa: regex} )
    '''
    regex_replace = {'B': '[ BND]',
                     'Z': '[ ZEQ]',
                     'J': '[ JIL]',
                     'X': '[ .]',
                     'I': '[ IJ]',
                     'L': '[ LJ]',
                     'Other' : '[ %s]'}
    regex_pep_seq = list()
    rkeys = regex_replace.keys()
    pep_seq = pep_seq.upper()
    for each in list(pep_seq):
        if each in rkeys:
            each = regex_replace[each]
        else:
            each = regex_replace['Other']%each
        regex_pep_seq.append(each)
#    return '(?=%s)'%''.join(regex_pep_seq) #Add a lookahead assertion (allows overlaping matches, but those matches are void: X-NOTE: http://stackoverflow.com/a/11430936) and return the regex
    return ''.join(regex_pep_seq) #Don't use lookahead assertion to accelerate searches


def do_contigs(seq_pepdatas, min_overlap=3):
    contigs = defaultdict(set)
    seqs4search = sorted(seq_pepdatas.keys(), key=len)
    matched_seqs = set()
    while seqs4search:
        seq2search = seqs4search.pop(0)
        seq2search_len = len(seq2search)
        seq_pattern = regex_format(seq2search)
        spaces_len = seq2search_len - min_overlap
        spaces = ' ' * spaces_len
        for seq4search in seqs4search:
            seq_target = spaces + seq4search + spaces
            for match in re.finditer(seq_pattern, seq_target):
                matched_seqs.add(seq2search)
                matched_seqs.add(seq4search)
                contig_seq = (seq_target[:match.start()] + seq2search + 
                              seq_target[match.start() + seq2search_len:])
                contig_len = len(contig_seq)
                contig_seq = contig_seq.lstrip()
                removed_spaces = contig_len - len(contig_seq)
                remain_spaces = spaces_len - removed_spaces
                real_match_start = match.start() - removed_spaces
                contig_seq = contig_seq.rstrip()
                for iter_seq, iter_match_start in ((seq2search, real_match_start), 
                                                   (seq4search, remain_spaces)):
                    for pepdata in seq_pepdatas[iter_seq]:
                        new_pepdata = PepData(pepdata.peptide, 
                                              pepdata.match_start + 
                                              iter_match_start)
                        contigs[contig_seq].add(new_pepdata)
    if matched_seqs:
        contigs = do_contigs(contigs) #Continue aligning without the non-matching sequences (1/3 of total sequences for LymPHOS Normal)
        for seq_wo_match in matched_seqs.symmetric_difference(seq_pepdatas.keys()):
            contigs[seq_wo_match] = seq_pepdatas[seq_wo_match]
        return contigs
    else:
        return seq_pepdatas


def random_peps(number=100):
    random.seed()
    aa = [chr(asc) for asc in range( ord('A'), ord('Z')+1 )]
    rnd_peps = list()
    for x in range(number):
        pep = list()
        for y in range(random.randint(5,20)):
            pep.append(random.choice(aa))
        rnd_peps.append(''.join(pep))
    return rnd_peps


def get_peptides():
#    peptides = random_peps()
#    peptides = ['NMKBWKDPLDMQDNROUDKW', 'AAANMKBWKDPLDMQ', 'DMQDNROUDKWAA', #X-NOTE: 'DMQDNROUDKWAAA' -> Not ending
#                'DMQDNROUDKWEE', 'WEEJEJEJEJEHFE',
#                'HFEGMQRSXCZQ', 'FEGMQRS']
    peptides = models.PeptideCache.objects.filter(phospho=True)
    peps = dict()
    for pep in peptides:
        peps[pep.peptide] = [ PepData(pep, 0) ]
    return peps


def main():
    
    seq_pepdatas = get_peptides()
    contig_pepdatas = do_contigs(seq_pepdatas)
    
    print(contig_pepdatas)


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    main()
