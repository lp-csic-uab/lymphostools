#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
exportLymPHOSDB2CSVReport -- Exports a LymPHOS2 DataBase (the one defined in DJango environment configuration) as a CSV file. If quantitative data is exported some assessment parameters (T Student tests, Fold-Change, 3*SDofMeans, ...) are calculated, and graphical histograms are saved as PNG files.

:synopsis: `exportLymPHOSDB2CSVReport.py` is a script that exports a LymPHOS2
DataBase (the one defined in DJango environment configuration) as a CSV file. If
quantitative data is exported some assessment parameters (T Student tests, Fold-
change, 3*SDofMeans, ...) are calculated, and graphical histograms are saved as
PNG files.
See file ``CSVReport_config.py`` to define Inputs (``EXPERIMENTS`` to analyze,
``QUANTITATIVE``, ``PHOSPHO``) and Outputs (``OUT_FILENAME``,
``EXTRA_OUT_FILENAME``, ``GRAPH_OUT_FILENAME``).

It defines :func:`export_lymphosdb`

:created:    2013/10/08

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013-2017 LP-CSIC/UAB. All rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '1.7'
__UPDATED__ = '2017-07-05'


#===============================================================================
# Imports
#===============================================================================
from tools_commons import DB_SubSet, GeneralFuncs, MathFuncs
from CSVReport_exporter_class import CSVReportGenerator, GraphReportGenerator

from collections import defaultdict
from datetime import datetime


#===============================================================================
# Global variables
#===============================================================================

import CSVReport_config as config


#===============================================================================
# Function definitions
#===============================================================================
def _get_time_nop_norm_from_lymphos(data_records):
    time_nop_norm = dict()
    time_nop_values = defaultdict(list)
    for data_record in data_records.filter(phospho=False):
        quanttimes = data_record.experiment.quanttimes
        for time, quant in zip(quanttimes, data_record.qdata_l):
            time_nop_values[time].append(quant)
    for time, values in time_nop_values.items():
        time_nop_norm[time] = MathFuncs.calc_normal(values)
    return time_nop_norm


def export_lymphosdb(filename, sub_db, extras_filename=None,
                     graph_filename=None, exp_pack=None):
    """
    Exports a filtered LymPHOS DataBase (`sub_db`) to a CSV file
    (`filename`). If `extras_filename` is specified, extra information
    (protein list, peptide list) is also exported as plain text files. And if
    `graph_filename`, graphical histograms are also written to PNG files.

    :param string filename: file name template for the output CSV file.
    :param sub_db: a filtered LymPHOS database. :type sub_db: an instance of
    :class:`tools_common.DB_SubSet`.
    :param string extras_filename: file name template to generate the output
    filenames for the extra information plain text files.
    :param string graph_filename: file name template to generate the output
    filenames for the graph information PNG files.
    """
    # Get the data query:
    data_records = sub_db.get_data()
    proteins = sub_db.get_proteins()
    quant_times = sub_db.get_quant_times()
    # Process & Write Data:
    print( 'Exporting LymPHOS DB main data to \"{0}\".\n Please wait....\n'.format(filename) )
    #
    if config.QUANTITATIVE is not False:
#        nop_db = DB_SubSet(exp_pack, phospho=False, quant=True)
#        dataquant_records = nop_db.get_dataquants()
#        nop_stats = _get_time_nop_norm_from_lymphos(dataquant_records)
#        nop_stats = MathFuncs.mix_normals(config.NOP_STATS, exp_pack) #X-NOTE: mixing data from different supra-experiments was crazy!
        nop_stats = { type_id: MathFuncs.mix_normals(exp2nop_stats, exp_pack)
                      for type_id, exp2nop_stats in config.NOP_STATS.items() }
    else:
        nop_stats = None
    csv_report = CSVReportGenerator(filename, data_records, proteins, nop_stats,
                                    config.QUANTITATIVE, quant_times, sub_db)
    csv_report.write_all()
    csv_report.close()
    #
    print(' See file \"{0}\" for results.\n'.format(filename))
    #
    # Process & Write Extra Data:
    if extras_filename:
        print('Exporting LymPHOS DB extra data. Please wait....\n')
        #
        protein_acs = csv_report.proteins_by_ac.keys()
        peptide_seqs = { pep[0] for pep in csv_report.peptides.keys() }
        sufixes_and_data = {'prots': sorted(protein_acs),
                            'prots_no_isof': sorted({prot.split('-')[0]
                                                     for prot in protein_acs}),
                            'peptides': sorted(peptide_seqs),
                            'peptides_no_mods': sorted(pep.upper()
                                                       for pep in peptide_seqs),
                            }
        filenames = list()
        for sufix, data in sufixes_and_data.items():
            filename = extras_filename.format(sufix)
            filenames.append(filename)
            GeneralFuncs.save_iterable(filename,data)
        #
        print(' See files \n  {0}\n for results.\n'.format( '\n  '.join(filenames)) )
    #
    # Process & Write time specific ratios Histograms:
    if config.QUANTITATIVE and graph_filename:
        print('Exporting LymPHOS DB time specific ratios histograms.\n Please wait....\n')
        # Mix all no-phosporilated statistics (mixing data from different supra-experiments is crazy! FIXME!):
        all_exp2nop_stats = dict()
        for exp2nop_stats in config.NOP_STATS.values():
            all_exp2nop_stats.update(exp2nop_stats)
        nop_stats = MathFuncs.mix_normals(all_exp2nop_stats, exp_pack)
        # Write ratio histograms of the experiments in each time (mixing data from different supra-experiments is crazy! FIXME!):
        graph_report = GraphReportGenerator(graph_filename, data_records,
                                            nop_stats, sub_db)
        graph_report.write_ratio_histograms()
        #
        print(' See files of type \"{0}\"for results.\n'.format(graph_filename))


def main():
    """
    Main program control
    """
    #Export selected or all experiments to file/s:
    if config.EXPERIMENTS:
        for exp_pack in config.EXPERIMENTS:
            sub_db = DB_SubSet(exp_pack, config.PHOSPHO, config.QUANTITATIVE)
            exp_fname = 'Exp' + '_'.join( map(str, sorted(exp_pack)) )
            filename = config.OUT_FILENAME.format(experiments=exp_fname)
            extras_filename = config.EXTRA_OUT_FILENAME.format('{0}',
                                                        experiments=exp_fname)
            export_lymphosdb(filename, sub_db, extras_filename,
                             config.GRAPH_OUT_FILENAME, exp_pack)
    else:
        sub_db = DB_SubSet(None, config.PHOSPHO, config.QUANTITATIVE)
        exp_fname = 'ExpALL'
        filename = config.OUT_FILENAME.format(experiments=exp_fname)
        extras_filename = config.EXTRA_OUT_FILENAME.format('{0}',
                                                           experiments=exp_fname)
        export_lymphosdb(filename, sub_db, extras_filename, config.GRAPH_OUT_FILENAME)



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    time0 = datetime.now()
    #
    main()
    #
    dtime = datetime.now() - time0
    print('\nData exported in {0} seconds.'.format(dtime.seconds))
    #
    print('\nEnd of program execution.\n')
