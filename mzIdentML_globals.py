#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
'''
Created on 2013/09/19

@authors: Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
'''

#===============================================================================
# Global variables
#===============================================================================
DB2CV_ENGINES = {'OMSSA':    {'software_name': 'OMSSA',
                              'field': 'omssa',
                              'software_accession': 'MS:1001475',
                              'software_version': '2.1.4', 
                              'accession': 'MS:1001329',
                              'name': 'OMSSA:pvalue'},
                 'Phenyx':   {'software_name': 'Phenyx',
                              'field': 'phenyx',
                              'software_accession': 'MS:1001209',
                              'software_version': '2.6', 
                              'accession': 'MS:1001395',
                              'name': 'Phenyx:Pepzscore'},
                 'SEQUEST':  {'software_name': 'Sequest',
                              'field': 'xcorr',
                              'software_accession': 'MS:1001208',
                              'software_version': '28', 
                              'accession': 'MS:1001155',
                              'name': 'Sequest:xcorr'},
                 'EasyProt': {'software_name': 'Phenyx', #X-NOTE: EasyProt simulates being Phenyx
                              'field': 'easyprot',
                              'software_accession': 'MS:1001209',
                              'software_version': '2.2', 
                              'accession': 'MS:1001395',
                              'name': 'Phenyx:Pepzscore'},
                 'PEAKS':    {'software_name': 'PEAKS Studio',
                              'field': 'peaks',
                              'software_accession': 'MS:1001946',
                              'software_version': '6',
                              'accession': 'MS:1001950',
                              'name': 'PEAKS:peptideScore'},
                 }
CV_INTEGRATOR = {'engine': 'Integrator',
                 'software_name': 'Integrator',
                 'unit_accession': 'MS:1001456',
                 'unit_name': 'analysis software',
                 'software_version': '3.4'}

FASTA_FULLFILEN = '/home/oga/Laboratori de Proteomica CSIC-UAB/S-HPP/Infrastructures/FastaDB/uniprot_sprot_human_target_decoy.fasta'

PROVIDER = {'role_accession': 'MS:1001271',
            'role_name': 'researcher',
            'first_name': 'Óscar',
            'last_name': 'Gallardo',
            'person_email': 'ogallardo@proteored.org',
            'organization': 'Laboratori de Proteòmica CSIC/UAB',
            'organization_email': 'lp.csic@uab.cat'} 

EXP_SEARCHPROTOCOLS = {'EXP_COMMON': {'ENG_COMMON': {'enzyme': 'Trypsin',
                                                     'enzyme_acc': 'MS:1001251',
                                                     'missed_cleavages': 1,
                                                     'fragment_tolerance': 0.8,
                                                     'parent_tolerance': 1.5,
                                                     'search_unimods': (4, 21, 23, 35)},
                                      },
                       (35,):        {'ENG_COMMON': {'enzyme': 'Trypsin',
                                                     'enzyme_acc': 'MS:1001251',
                                                     'missed_cleavages': 1,
                                                     'fragment_tolerance': 0.8,
                                                     'parent_tolerance': 1.5,
                                                     'search_unimods': (4, 21, 23, 35)},
                                      },
                       }


