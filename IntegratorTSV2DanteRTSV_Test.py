#!/usr/local/bin/python2.7
# encoding: utf-8
"""
Unit Testing for IntegratorTSV2DanteRTSV

:synopsis:   IntegratorTSV2DanteRTSV is script that converts some Integrator TSV
output files to a suitable DanteR TSV input file.

It defines the following classes and functions:
    :class:`Test`

:created:    2014-01-21

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.95
:updated:    2014-07-25
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import division
from __future__ import print_function

from pprint import pprint as print
from collections import OrderedDict
import random
import csv
import os

import unittest

import IntegratorTSV2DanteRTSV as i2d


#===============================================================================
# Global variables
#===============================================================================
REF_OUT_FILE_BN = '203_manual_processed_results.tsv'
FILE_BN = '203_integrator_tsv_output.xls'
REF_OUT_FILE_FQN = os.path.join( os.path.dirname(__file__), 'test_data', 
                                 REF_OUT_FILE_BN )
FILE_FQN = os.path.join( os.path.dirname(__file__), 'test_data', FILE_BN )
FILEN_EXP_ID = FILE_BN.partition('_')[0] #FILEN_EXP_ID
EXPERIMENTS = ( dict(), dict() )
for expid in (FILEN_EXP_ID,):
    EXPERIMENTS[0][expid] = {'qion2time': {'TMT_126':   '0', 'TMT_127':   '15', 
                                           'TMT_128':  '30', 'TMT_129':   '60', 
                                           'TMT_130': '120', 'TMT_131': '1440'},
                             'files': [ FILE_FQN ],
                             'id': expid
                             }
    EXPERIMENTS[1][expid] = {'qion2time': {'TMT_126':   '0', 'TMT_127':   '0', 
                                           'TMT_128':  '15', 'TMT_129':  '15', 
                                           'TMT_130':  '60', 'TMT_131':  '60'},
                             'files': [ FILE_FQN ],
                             'id': expid
                             }
GRPER2VAL2HEADER = OrderedDict( ( 
                                 ('Experiment', {FILEN_EXP_ID: 'Exp{0}'.format(FILEN_EXP_ID)}), 
                                 ('phosphorylated', {'1': 'P', '0': 'NP'}), 
                                 ) )
TIMES2QUANTHEADERS = (
                {'0':  ('TMT_126',), '15':  ('TMT_127',), '30':  ('TMT_128',), 
                 '60': ('TMT_129',), '120': ('TMT_130',), '1440': ('TMT_131',)}, 
                {'0':  ('TMT_126', 'TMT_127'), '15': ('TMT_128', 'TMT_129'), 
                 '60': ('TMT_130', 'TMT_131')},                                               
                      )
TIMES2TIME_QHEADERS = (
                {'0':  '0_TMT126',  '15':  '15_TMT127',  '30':   '30_TMT128', 
                 '60': '60_TMT129', '120': '120_TMT130', '1440': '1440_TMT131'}, 
                {'0':  '0_TMT126_TMT127', '15':  '15_TMT128_TMT129', 
                 '60': '60_TMT130_TMT131'},                                            
                       )
DUPLICATES = (1, 2)


#===============================================================================
# Class definitions
#===============================================================================
class I2DConfigurationTest(unittest.TestCase):
    def test_exp2time2qheaders(self):
        for exp in EXPERIMENTS:
            config = i2d.I2DConfiguration(i2d.CLI_PARAMS, experiments=exp)
            self.assertItemsEqual( exp.keys(), config.exp2time2qheaders.keys() )
            for exp_id, time2qheaders in config.exp2time2qheaders.items():
                times = set( exp[exp_id]['qion2time'].values() )
                self.assertItemsEqual( time2qheaders.keys(), times )
                qheaders = list()
                for vals in time2qheaders.values():
                    qheaders.extend(vals)
                self.assertItemsEqual( qheaders, 
                                       exp[exp_id]['qion2time'].keys() )
    
    def test_exp2time2time_qheader(self):
        for exp in EXPERIMENTS:
            config = i2d.I2DConfiguration(i2d.CLI_PARAMS, experiments=exp)
            self.assertItemsEqual( exp.keys(), 
                                   config.exp2time2time_qheader.keys() )
            for exp_id, time2time_qheader in config.exp2time2time_qheader.items():
                times = set( exp[exp_id]['qion2time'].values() )
                self.assertItemsEqual( time2time_qheader.keys(), times )
                for time, time_qheader in time2time_qheader.items():
                    self.assertTrue( time_qheader.startswith(str(time)) )
    
    def test_exp2time_qheaders(self):
        for exp in EXPERIMENTS:
            config = i2d.I2DConfiguration(i2d.CLI_PARAMS, experiments=exp)
            self.assertItemsEqual( exp.keys(), config.exp2time_qheaders.keys() )
            for exp_id, time_qheaders in config.exp2time_qheaders.items():
                time_qheaders2 = config.exp2time2time_qheader[exp_id].values()
                self.assertItemsEqual( time_qheaders, time_qheaders2 )
                for index, time in enumerate( config.exp2time2time_qheader[exp_id].keys() ):
                    self.assertTrue( time_qheaders[index].startswith(str(time)) )


class IntegratorTSVLoaderTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.filen = FILE_FQN
    
    def test_make_finalpeptide(self):
        mock_rows = ( {'ascore revised peptide': '.', 
                       'consensus modified peptide': 'R(737)NS(21)LTGEEGQLAR',
                       'RESULT': 'RNsLTGEEGQLAR'}, 
                      {'ascore revised peptide': 'R(737)NS(21, 23)LTGEEGQLAR',
                       'RESULT': 'RNsLTGEEGQLAR'}, 
                      {'ascore revised peptide': 'RNS(23)LTGEEGQLAR(737)',
                       'RESULT': 'RNsLTGEEGQLAR'}, 
                      {'ascore revised peptide': ' .', 
                       'consensus modified peptide': 'R(737)NS(4)LTGEEGQLAR',
                       'RESULT': 'RNSLTGEEGQLAR'}, 
                      {'ascore revised peptide': 'RNS(21,23)LT(23)GEEGQLAR',
                       'consensus modified peptide': 'RNS(21)LTGEEGQLAR',
                       'RESULT': 'RNsLtGEEGQLAR'} )
        integrator_loaders = ( i2d.IntegratorTSVLoader, 
                               i2d.IntegratorTSVLoader() )
        
        for integrator_loader in integrator_loaders:
            for mock_row in mock_rows:
                self.assertEqual(integrator_loader._make_finalpeptide(mock_row), 
                                 mock_row['RESULT'])


class IntegratorExperimentDataTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.loader = i2d.IntegratorTSVLoader(FILE_FQN)
        cls.data = cls.loader.data

    def setUp(self):
        self.datahandler = i2d.IntegratorExperimentData(self.data)

    def tearDown(self):
        pass

    def test_quantcols_summary(self):
        quantcols_summary = self.datahandler.quantcols_summary
        print(quantcols_summary)
        self.assertIsInstance(quantcols_summary, dict)
        self.assertItemsEqual(self.datahandler.quant_columns, 
                              quantcols_summary.keys())
        for _, values in quantcols_summary.items():
            self.assertIsInstance(values, dict)
            self.assertItemsEqual( ('min', 'max', 'mean', 'stdv', 'median'), 
                                   values.keys() )
            for sub_value in values.values():
                self.assertGreater(sub_value, 0)
    
    def test_copy(self):
        datahandeler_copy = self.datahandler.copy()
        self.assertItemsEqual(datahandeler_copy.data, self.datahandler.data)
        self.assertIsNot(datahandeler_copy, self.datahandler)
        self.assertIsNot(datahandeler_copy.data, self.datahandler.data)
    
    def test_default_min_mv_func(self):
        quantcols_summary = self.datahandler.quantcols_summary
        for quant_col in self.datahandler.quant_columns:
            col_summary = quantcols_summary[quant_col]
            self.assertGreater(self.datahandler._default_min_mv_func(col_summary), 0)
    
    def test_imputed_mv(self):
        mock_row = {key: 0.0 for key in self.datahandler.quant_columns}
        mock_row['MV'] = 6
        mock_data = [ mock_row ]
        imputed_data = self.datahandler.imputed_mv(mock_data)
        print(imputed_data)
        for row in imputed_data:
            self.assertEqual(row['MV'], 0)
            for quant_col in self.datahandler.quant_columns:
                self.assertGreater(row[quant_col], 0)
    
    def test_normalized(self):
        test_params = zip(TIMES2QUANTHEADERS, TIMES2TIME_QHEADERS, DUPLICATES)
        #
        for time2quantheaders, time2time_qheader, duplicate in test_params:
            self.datahandler.time2quantheaders = time2quantheaders
            self.datahandler.time2time_qheader = time2time_qheader
            norm_data = self.datahandler.normalized()
            nonorm_data = self.datahandler.data
            # Test copy of data during normalization:
            self.assertIsNot(norm_data, nonorm_data)
            self.assertIsNot(norm_data[0], nonorm_data[0])
            # Test total number of rows:
            self.assertEqual(len(norm_data), len(nonorm_data) * duplicate)
            # Test normalized data row headers:
            for _ in range(100):
                norm_row = random.choice(norm_data)
                nonorm_row = random.choice(nonorm_data)
                self.assertEqual( len(norm_row), 
                                  len(nonorm_row) + len(time2time_qheader) )
                for time_qheader in time2time_qheader.values():
                    self.assertIn(time_qheader, norm_row.keys())
            # Test that normalized duplicated rows have the same quantitative
            # values (except for their IDs):
            if duplicate == 2:
                for index in range(0, len(norm_data), 2):
                    norm_row1 = norm_data[index]
                    norm_row2 = norm_data[index+1]
                    self.assertEqual(norm_row1['ID'], norm_row2['ID'] - 0.5)
                    for quant_col in self.datahandler.quant_columns:
                        self.assertEqual(norm_row1[quant_col], 
                                         norm_row2[quant_col])
            print(norm_data[:2])


class DanteRTSVExporterTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.filen = FILE_FQN
        cls.loader = i2d.IntegratorTSVLoader(FILE_FQN)
        cls.datahandler = i2d.IntegratorExperimentData(cls.loader.data)
        cls.outfilen = FILE_FQN + '.toDanteR.tsv'
        cls.test_times2quantheaders = TIMES2QUANTHEADERS
        cls.test_times2time_qheaders = TIMES2TIME_QHEADERS
    
    def setUp(self):
        if os.path.exists(self.outfilen):
            os.remove(self.outfilen)
        self.writer = i2d.DanteRTSVExporter(common_columns=i2d.DANTER_METADATA_HEADERS(i2d.TMT_COLUMNS), 
                                            grper2val2header=GRPER2VAL2HEADER)
    
    def tearDown(self):
        pass

    def test_init(self):
        self.assertIsInstance(self.writer, i2d.DanteRTSVExporter)
        self.assertRaises( IOError, lambda filen: setattr(self.writer, 'filen', 
                                                          filen), FILE_FQN )
        self.assertIsNone( setattr(self.writer, 'filen', self.outfilen) )
    
    def test_vals2gheaders(self):
        for time2time_qheader in self.test_times2time_qheaders:
            self.writer.exp2time_qheaders = {FILEN_EXP_ID: time2time_qheader.values()}
            len_combine = ( [len(self.writer.exp2time_qheaders[FILEN_EXP_ID])] + 
                            [len(value) for value in 
                             self.writer.grper2val2header.values()] 
                           )
            self.assertEqual( len(self.writer.vals2gheaders), 
                              reduce(int.__mul__, len_combine) )
            print(self.writer.vals2gheaders)
    
    def test_rename_cols(self):
        mock_data = ( {'a': 1, 
                       'b': 2,
                       'c': 3}, 
                      {'a': 11, 
                       'c': 33,
                       'd': 44}, 
                      {'a': 111, 
                       'b': 222,
                       'c': 333} )
        testing_old2new = ( {'a': 'a_new'}, 
                            {'a': 'a_new', 'b': 'b_new'}, 
                            {'a': 'a_new', 'c': 'c_new'}, 
                            {'d': 'd_new'} )
        #
        for old2new in testing_old2new:
            data = [row.copy() for row in mock_data]
            self.writer.rename_cols(data, old2new)
            for oldrow, newrow in zip(mock_data, data):
                self.assertEqual(len(oldrow), len(newrow))
                for old, new in old2new.items():
                    self.assertEqual( oldrow.get(old, None), 
                                      newrow.get(new, None) )
                    self.assertNotIn(old, newrow.keys())
                for no_renamed in set(oldrow.keys()).difference(old2new.keys()):
                    self.assertEqual(oldrow[no_renamed], newrow[no_renamed])
    
    def test_format_quant_data(self):
        params = zip( self.test_times2quantheaders, 
                      self.test_times2time_qheaders )
        for time2quantheaders, time2time_qheader in params:
            self.datahandler.time2quantheaders = time2quantheaders
            self.datahandler.time2time_qheader = time2time_qheader
            norm_data = self.datahandler.normalized()
            self.writer = i2d.DanteRTSVExporter(common_columns=i2d.DANTER_METADATA_HEADERS(i2d.TMT_COLUMNS), 
                                                grper2val2header=GRPER2VAL2HEADER)
            self.writer.exp2time_qheaders = {FILEN_EXP_ID: time2time_qheader.values()}
            #
            grped_data = self.writer.make_groups(norm_data)
            for grped_rows in grped_data.values():
                for quant_header in self.writer.exp2time_qheaders[FILEN_EXP_ID]:
                    self.assertIn(quant_header, grped_rows[0])
            formated_data = self.writer._format_quant_data(grped_data)
            for grped_rows in grped_data.values():
                for quant_header in self.writer.exp2time_qheaders[FILEN_EXP_ID]:
                    self.assertNotIn(quant_header, grped_rows[0])
            self.assertEqual( len(formated_data), len(norm_data) )
            self.assertEqual( len(self.writer._current_quant_headers), 
                              len(time2quantheaders.keys()) * 
                              len(self.writer.grper2val2header['phosphorylated']) 
                             )
            for current_quant_header in self.writer._current_quant_headers:
                self.assertIn( current_quant_header, 
                               self.writer.vals2gheaders.values() )
            for grped_rows in grped_data.values():
                print( grped_rows[0] )
        
    def test_write(self):
        params = zip( self.test_times2quantheaders, 
                      self.test_times2time_qheaders )
        for test, (time2quantheaders, time2time_qheader) in enumerate(params):
            self.datahandler.time2quantheaders = time2quantheaders
            self.datahandler.time2time_qheader = time2time_qheader
            norm_data = self.datahandler.normalized()
            self.writer = i2d.DanteRTSVExporter(common_columns=i2d.DANTER_METADATA_HEADERS(i2d.TMT_COLUMNS), 
                                                grper2val2header=GRPER2VAL2HEADER)
            self.writer.exp2time_qheaders = {FILEN_EXP_ID: time2time_qheader.values()}
            #
            outfilen = '{0}.test_{1}'.format(self.outfilen, test)
            if os.path.exists(outfilen):
                os.remove(outfilen)
            self.writer.filen = outfilen
            self.writer.write(norm_data)
            #
            with open(self.writer.filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                self.assertItemsEqual(csvdr.fieldnames, self.writer.headers)


class Integrator2DanteRWorkFlowTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.config = i2d.I2DConfiguration(i2d.CLI_PARAMS, 
                                          experiments=EXPERIMENTS[0], 
                                          quant_columns=i2d.TMT_COLUMNS, 
                                          quant_iontype='TMT6plex')
        cls.config.output_file = FILE_FQN + '.toDanteR.tsv'
        # Load reference data:
        cls.ref_output_file = REF_OUT_FILE_FQN
        cls.ref_data = list()
        with open(cls.ref_output_file, 'rb') as io_file:
            csvdr = csv.DictReader(io_file, delimiter='\t')
            for row in csvdr:
                for header, value in row.items():
                    if header.startswith('203') and value:
                        row[header] = float(value)
                    elif header in ('first scan', 'last scan', 'MS'):
                        row[header] = int(value)
                cls.ref_data.append(row)
        # Sort reference data for future comparisons:
        cls.ref_data.sort(key = lambda d: (d['file'], d['last scan'], d['MS']))
    
    def setUp(self):
        if os.path.exists(self.config.output_file):
            os.remove(self.config.output_file)
    
    def tearDown(self):
        pass

    def test_full_workflow(self):
        dantewriter = i2d.DanteRTSVExporter(filen=self.config.output_file, 
                                            common_columns=i2d.DANTER_METADATA_HEADERS(i2d.TMT_COLUMNS), 
                                            exp2time_qheaders=self.config.exp2time_qheaders,
                                            grper2val2header=GRPER2VAL2HEADER)
        for exp_id in self.config.experiments.keys():
            exp_data = i2d.load_exp_data(exp_id, self.config)
            processed_data = i2d.process_exp_data(exp_id, exp_data, self.config)
            
            for newcol in self.config.exp2time2time_qheader[exp_id].values():
                rnd_row = random.choice(processed_data)
                self.assertIn(newcol, rnd_row.keys())
            processed_data.sort(key = lambda d: (d['file'], d['last scan'], d['MS']))
            self.assertEqual( len(processed_data), len(self.ref_data) )
            hhead = {'0': 'NP', '1': 'P'}
            for ref_row, row in random.sample( zip(self.ref_data, processed_data), 1000 ):
                self.assertEqual( ref_row['last scan'], row['last scan'] )
                self.assertEqual( ref_row['MS'], row['MS'] )
                for time, newcol in self.config.exp2time2time_qheader[exp_id].items():
                    ref_col = '_'.join( (exp_id, 
                                         hhead[ref_row['phosphorylated']], 
                                         time) )
                    if ref_row[ref_col]:
                        self.assertEqual( round(ref_row[ref_col], 4), 
                                          round(row[newcol], 4) )
            
            for mv_summary_type in ('ALL', 'Filtered'):
                mv_summary_file = '{0}.{1}.{2}_missing_values_summary.tsv'.format(self.config.output_file, exp_id, mv_summary_type)
                self.assertTrue( os.path.exists(mv_summary_file) )
            q_summary_file = '{0}.{1}.quantitative_summary.tsv'.format(self.config.output_file, exp_id)
            self.assertTrue( os.path.exists(q_summary_file) )

            dantewriter.write(processed_data)
            self.assertTrue( os.path.exists(self.config.output_file) )


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testIntedratorDataHandling_quantcols_summary']
    unittest.main()
